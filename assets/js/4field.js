import * as user from './user.js'
import * as subcontractor from './subcontractor.js'
import * as mask_generator from './mask_generator.js'
import * as backlog_activities from './backlog_activities.js'

/*Alerts Functions*/
export function append_sucess_alert(text){
    $('.alerts .card-body').html('')
    $('.alerts .card-body').append("<div class='alert alert-success alert-dismissible fade show' role='alert'><p>" + text + "</p><button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>")
    $(window).scrollTop(0);
}

export function append_danger_alert(text){
    $('.alerts .card-body').html('')
    $('.alerts .card-body').append("<div class='alert alert-danger alert-dismissible fade show' role='alert'><p>" + text + "</p><button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>")
    $(window).scrollTop(0);
}

export function append_warning_alert(text){
    $('.alerts .card-body').html('')
    $('.alerts .card-body').append("<div class='alert alert-warning alert-dismissible fade show' role='alert'><p>" + text + "</p><button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>")
    $(window).scrollTop(0);
}

$(document).ready(function(){
    // Habilitando acesso ao editor de conta
    user.get_user_profile_page()

    /*Sidebar Functions*/
    function get_side_bar(){
        $.ajax({
            url: "../new_4field/php/get_side_bar.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('#sidebar').html(data)
                user.get_user_manage_page()
                user.get_recolect_all_users_page()
                subcontractor.get_subcontractor_add_page()
                subcontractor.get_all_subcontractor_page()
                subcontractor.get_subcontractor_report_page()
                mask_generator.get_mask_generator_page()
                backlog_activities.get_backlog_activities_page()
            }
        })
    }
    get_side_bar()

})