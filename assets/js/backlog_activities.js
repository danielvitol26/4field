import {append_sucess_alert, append_danger_alert, append_warning_alert} from './4field.js'

export function get_backlog_activities_page(){
    //$('.get_backlog_activities_page').click(function(){
        $.ajax({
            url: "../new_4field/php/backlog_activities/get_backlog_activities_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                create_bar_consolidated_daily()
                create_bar_priority_daily()
            }
        })
    //})
}

async function create_bar_consolidated_daily(){
    const ctx = document.getElementById('consolidated-daily');

    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                'Até 4h',
                '4h-6h',
                '6h-8h',
                '8h-24h',
                '24h-48h',
                '48h-72h',
                '72h-96h',
                '>96h',
            ],
            datasets: [{
                label: 'Consolidado',
                data: [65, 59, 80, 81, 56, 55, 40, 90],
                backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 205, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(201, 203, 207, 0.2)',
                'rgba(201, 203, 207, 0.2)',
                ],
                borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)',
                'rgb(201, 203, 207)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    grid: {
                        display: false
                    }
                },
                x: {
                    grid: {
                        display: false,
                    }
                },
            }
        }
    })
}

async function create_bar_priority_daily(){
    const ctx = document.getElementById('priority-daily');

    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Alta', 'Média', 'Baixa'],
            datasets: [{
                label: 'Prioridade',
                data: [65, 59, 80],
                backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 205, 86, 0.2)',
                ],
                borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            indexAxis: 'y',
            scales: {
                y: {
                    beginAtZero: true,
                    grid: {
                        display: false
                    }
                },
                x: {
                    grid: {
                        display: false,
                    }
                },
            },
            // plugins: {
            //     title: {
            //         display: true,
            //         text: 'Custom Chart Title'
            //     },
            //     subtitle: {
            //         display: true,
            //         text: 'Custom Chart Subtitle'
            //     }
            // }
        }
    })
}