$(document).ready(function(){
    let activities_from_api = []
    let activities_ids_from_api = []
    let activities_to_update = []
    let final_activities_to_update = []
    let activities_to_add = []

    let start_date = get_today_date()
    let end_date = get_future_date(30)
    let non_scheduled = true

    async function check_activities_date_and_add_to_historic(date) {
        console.log('Checando ativididades de dias anteriores')
        let data = {
            check_activities_date_and_add_to_historic: true,
            date: date,
        }

        const response = await fetch(
            '../../php/backlog_activities_collector/check_activities_date_and_add_to_historic.php',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            }
        )
        const checked_activities = await response.json()
        return checked_activities
    }

    async function get_all_activities_from_api(start_date, end_date, non_scheduled) {
        console.log('Coletando atividades na API')
        let data = {
            get_all_activities_from_api: true,
            start_date: start_date,
            end_date: end_date,
            non_scheduled: non_scheduled,
        }

        const response = await fetch(
            '../../php/backlog_activities_collector/get_all_activities_from_api.php',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            }
        )
        const all_activities_from_api = await response.json()
        return all_activities_from_api
    }

    async function get_activities_from_db_with_activities_ids_from_api(activities_ids) {
        if(activities_ids.length > 0){
            let data = {
                activities_ids: activities_ids,
            }

            const response = await fetch(
                '../../php/backlog_activities_collector/get_activities_from_db_with_activities_ids_from_api.php',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data),
                }
            )
            const activities_from_db = await response.json()
            return activities_from_db
        }else{
            return false
        }
    }

    async function get_activities_to_add_from_activities_to_update(activities_to_update){
        if(activities_to_update.length == 0){
            console.log('Adicionando todos os registros que vieram da API (banco zerado)')
            if(activities_from_api.length > 0){
                activities_to_add = activities_from_api
                return true
            }else{
                return false
            }
        }else if(activities_to_update.length > 0){
            console.log('Confrontando registros do banco com os que vieram da API')

            const get_column_from_array = (array, column_number) => array.map(row => row[column_number])
            let activity_id_column = get_column_from_array(activities_to_update, 4).map(function(item) {
                return parseInt(item)
            })

            /*Add => Pega registros da API que não foram encontrados no banco
              e adiciona ao array 'activities_to_add'*/
            let add_activity_id_array = activities_ids_from_api.filter(a => !activity_id_column.includes(a));
            add_activity_id_array = add_activity_id_array.map(function(item) {
                return String(item)
            })

            add_activity_id_array.forEach(activity_id => {
                let found_activity = activities_from_api.filter( activity => activity.activityId === parseInt(activity_id))
                if(found_activity.length > 0){
                    found_activity = found_activity[0]
                    activities_to_add.push(found_activity)
                }
            })

            /*Update => Pega registros que vieram do banco e compara
              com a API para ver os que realmente precisam de alteração
              e adiciona ao array 'final_activities_to_update'*/

            activities_to_update.forEach(activity_to_update => {
                let date = new Date()
                let current_id = activity_to_update[0]
                let current_XA_PI_CREATE_DATE = activity_to_update[1]
                let current_date = activity_to_update[2]
                let current_A_TIME_OF_BOOKING = activity_to_update[3]
                let current_activityId = parseInt(activity_to_update[4])
                let current_apptNumber = activity_to_update[5]
                let current_XA_PI_EVENT = activity_to_update[6]
                let current_activityType = activity_to_update[7]
                let current_status = activity_to_update[8]
                let current_resourceInternalId = parseInt(activity_to_update[9])
                let current_resourceId = activity_to_update[10]
                let current_XA_ORIGIN_BUCKET = activity_to_update[11]
                let current_regional = activity_to_update[12]
                let current_contract = activity_to_update[13]
                let current_company = activity_to_update[14]
                let current_stateProvince = activity_to_update[15]
                let current_city = activity_to_update[16]
                let current_XA_EXECUTOR_USER = activity_to_update[17]
                let current_startTime = activity_to_update[18]
                let current_endTime = activity_to_update[19]
                let current_XA_PI_ALARM_TYPE = activity_to_update[20]
                let current_XA_PI_FAIL_TYPE = activity_to_update[21]
                let current_XA_PI_CM = activity_to_update[22]
                let current_XA_PI_END_ID = activity_to_update[23]
                let current_XA_PI_NETWORK_ELEMENT = activity_to_update[24]
                let current_XA_PI_NE_TYPE = activity_to_update[25]
                let current_XA_PI_NETWORK = activity_to_update[26]
                let current_XA_PI_NOTDONE_REASON = activity_to_update[27]
                let current_XA_PI_OP = activity_to_update[28]
                let current_XA_PI_OPENING_NOTE = activity_to_update[29]
                let current_XA_PI_PRIORITY = activity_to_update[30]
                let current_XA_PI_RESPONSABLE = activity_to_update[31]
                let current_XA_PI_SUB_AREA = activity_to_update[32]
                let current_XA_PI_SUSPEND_REASON = activity_to_update[33]
                let current_XA_PI_TRAM_REASON = activity_to_update[34]
                let current_XA_PI_TRAM_SUS = activity_to_update[35]

                /*
                id
                XA_PI_CREATE_DATE
                date
                A_TIME_OF_BOOKING
                activityId
                apptNumber
                XA_PI_EVENT
                activityType
                status
                resourceInternalId
                resourceId
                XA_ORIGIN_BUCKET
                regional
                contract
                company
                stateProvince
                city
                XA_EXECUTOR_USER
                startTime
                endTime
                XA_PI_ALARM_TYPE
                XA_PI_FAIL_TYPE
                XA_PI_CM
                XA_PI_END_ID
                XA_PI_NETWORK_ELEMENT
                XA_PI_NE_TYPE
                XA_PI_NETWORK
                XA_PI_NOTDONE_REASON
                XA_PI_OP
                XA_PI_OPENING_NOTE
                XA_PI_PRIORITY
                XA_PI_RESPONSABLE
                XA_PI_SUB_AREA
                XA_PI_SUSPEND_REASON
                XA_PI_TRAM_REASON
                XA_PI_TRAM_SUS
                */

                let activity_from_api = activities_from_api.filter( activity => activity.activityId === parseInt(current_activityId))
                if(activity_from_api.length > 0){
                    activity_from_api = activity_from_api[0]

                    //Corrigindo datas para terem o mesmo formanto quando diferentes
                    let has_change_in_current_A_TIME_OF_BOOKING = false
                    let has_change_in_api_A_TIME_OF_BOOKING = false

                    if(current_A_TIME_OF_BOOKING.length == 19 && activity_from_api.A_TIME_OF_BOOKING.length == 16){
                        current_A_TIME_OF_BOOKING = current_A_TIME_OF_BOOKING.slice(0, -3)
                        has_change_in_current_A_TIME_OF_BOOKING = true
                    }else if(current_A_TIME_OF_BOOKING.length == 16 && activity_from_api.A_TIME_OF_BOOKING.length == 19){
                        activity_from_api.A_TIME_OF_BOOKING = activity_from_api.A_TIME_OF_BOOKING.slice(0, -3)
                        has_change_in_api_A_TIME_OF_BOOKING = true
                    }

                    //Iniciando confrontamento para encontrar diferenças
                    let count_diferences = 0
                    if(typeof activity_from_api.XA_PI_CREATE_DATE !== "undefined" && current_XA_PI_CREATE_DATE != activity_from_api.XA_PI_CREATE_DATE){
                        console.log('XA_PI_CREATE_DATE')
                        console.log(current_XA_PI_CREATE_DATE + ' != ' + activity_from_api.XA_PI_CREATE_DATE)
                        count_diferences++
                    }else if(typeof activity_from_api.date !== "undefined" && current_date != activity_from_api.date){
                        console.log('date')
                        console.log(current_date + ' != ' + activity_from_api.date)
                        count_diferences++
                    }else if(typeof activity_from_api.A_TIME_OF_BOOKING !== "undefined" && current_A_TIME_OF_BOOKING != activity_from_api.A_TIME_OF_BOOKING){
                        console.log('A_TIME_OF_BOOKING')
                        console.log(current_A_TIME_OF_BOOKING + ' != ' + activity_from_api.A_TIME_OF_BOOKING)
                        count_diferences++
                    }else if(typeof activity_from_api.activityId !== "undefined" && current_activityId != activity_from_api.activityId){
                        console.log('activityId')
                        console.log(current_activityId + ' != ' + activity_from_api.activityId)
                        count_diferences++
                    }else if(typeof activity_from_api.apptNumber !== "undefined" && current_apptNumber != activity_from_api.apptNumber){
                        console.log('apptNumber')
                        console.log(current_apptNumber + ' != ' + activity_from_api.apptNumber)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_EVENT !== "undefined" && current_XA_PI_EVENT != activity_from_api.XA_PI_EVENT){
                        console.log('XA_PI_EVENT')
                        console.log(current_XA_PI_EVENT + ' != ' + activity_from_api.XA_PI_EVENT)
                        count_diferences++
                    }else if(typeof activity_from_api.activityType !== "undefined" && current_activityType != activity_from_api.activityType){
                        console.log('activityType')
                        console.log(current_activityType + ' != ' + activity_from_api.activityType)
                        count_diferences++
                    }else if(typeof activity_from_api.status !== "undefined" && current_status != activity_from_api.status){
                        console.log('status')
                        console.log(current_status + ' != ' + activity_from_api.status)
                        count_diferences++
                    }else if(typeof activity_from_api.resourceInternalId !== "undefined" && current_resourceInternalId != activity_from_api.resourceInternalId){
                        console.log('resourceInternalId')
                        console.log(current_resourceInternalId + ' != ' + activity_from_api.resourceInternalId)
                        count_diferences++
                    }else if(typeof activity_from_api.resourceId !== "undefined" && current_resourceId != activity_from_api.resourceId){
                        console.log('resourceId')
                        console.log(current_resourceId + ' != ' + activity_from_api.resourceId)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_ORIGIN_BUCKET !== "undefined" && current_XA_ORIGIN_BUCKET != activity_from_api.XA_ORIGIN_BUCKET){
                        console.log('XA_ORIGIN_BUCKET')
                        console.log(current_XA_ORIGIN_BUCKET + ' != ' + activity_from_api.XA_ORIGIN_BUCKET)
                        count_diferences++
                    }else if(typeof activity_from_api.regional !== "undefined" && current_regional != activity_from_api.regional){
                        console.log('regional')
                        console.log(current_regional + ' != ' + activity_from_api.regional)
                        count_diferences++
                    }else if(typeof activity_from_api.contract !== "undefined" && current_contract != activity_from_api.contract){
                        console.log('contract')
                        console.log(current_contract + ' != ' + activity_from_api.contract)
                        count_diferences++
                    }else if(typeof activity_from_api.company !== "undefined" && current_company != activity_from_api.company){
                        console.log('company')
                        console.log(current_company + ' != ' + activity_from_api.company)
                        count_diferences++
                    }else if(typeof activity_from_api.stateProvince !== "undefined" && current_stateProvince != activity_from_api.stateProvince){
                        console.log('stateProvince')
                        console.log(current_stateProvince + ' != ' + activity_from_api.stateProvince)
                        count_diferences++
                    }else if(typeof activity_from_api.city !== "undefined" && current_city != activity_from_api.city){
                        console.log('city')
                        console.log(current_city + ' != ' + activity_from_api.city)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_EXECUTOR_USER !== "undefined" && current_XA_EXECUTOR_USER != activity_from_api.XA_EXECUTOR_USER){
                        console.log('XA_EXECUTOR_USER')
                        console.log(current_XA_EXECUTOR_USER + ' != ' + activity_from_api.XA_EXECUTOR_USER)
                        count_diferences++
                    }else if(typeof activity_from_api.startTime !== "undefined" && current_startTime != activity_from_api.startTime){
                        console.log('startTime')
                        console.log(current_startTime + ' != ' + activity_from_api.startTime)
                        count_diferences++
                    }else if(typeof activity_from_api.endTime !== "undefined" && current_endTime != activity_from_api.endTime){
                        console.log('endTime')
                        console.log(current_endTime + ' != ' + activity_from_api.endTime)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_ALARM_TYPE !== "undefined" && current_XA_PI_ALARM_TYPE != activity_from_api.XA_PI_ALARM_TYPE){
                        console.log('XA_PI_ALARM_TYPE')
                        console.log(current_XA_PI_ALARM_TYPE + ' != ' + activity_from_api.XA_PI_ALARM_TYPE)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_FAIL_TYPE !== "undefined" && current_XA_PI_FAIL_TYPE != activity_from_api.XA_PI_FAIL_TYPE){
                        console.log('XA_PI_FAIL_TYPE')
                        console.log(current_XA_PI_FAIL_TYPE + ' != ' + activity_from_api.XA_PI_FAIL_TYPE)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_CM !== "undefined" && current_XA_PI_CM != activity_from_api.XA_PI_CM){
                        console.log('XA_PI_CM')
                        console.log(current_XA_PI_CM + ' != ' + activity_from_api.XA_PI_CM)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_END_ID !== "undefined" && current_XA_PI_END_ID != activity_from_api.XA_PI_END_ID){
                        console.log('XA_PI_END_ID')
                        console.log(current_XA_PI_END_ID + ' != ' + activity_from_api.XA_PI_END_ID)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_NETWORK_ELEMENT !== "undefined" && current_XA_PI_NETWORK_ELEMENT != activity_from_api.XA_PI_NETWORK_ELEMENT){
                        console.log('XA_PI_NETWORK_ELEMENT')
                        console.log(current_XA_PI_NETWORK_ELEMENT + ' != ' + activity_from_api.XA_PI_NETWORK_ELEMENT)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_NE_TYPE !== "undefined" && current_XA_PI_NE_TYPE != activity_from_api.XA_PI_NE_TYPE){
                        console.log('XA_PI_NE_TYPE')
                        console.log(current_XA_PI_NE_TYPE + ' != ' + activity_from_api.XA_PI_NE_TYPE)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_NETWORK !== "undefined" && current_XA_PI_NETWORK != activity_from_api.XA_PI_NETWORK){
                        console.log('XA_PI_NETWORK')
                        console.log(current_XA_PI_NETWORK + ' != ' + activity_from_api.XA_PI_NETWORK)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_NOTDONE_REASON !== "undefined" && current_XA_PI_NOTDONE_REASON != activity_from_api.XA_PI_NOTDONE_REASON){
                        console.log('XA_PI_NOTDONE_REASON')
                        console.log(current_XA_PI_NOTDONE_REASON + ' != ' + activity_from_api.XA_PI_NOTDONE_REASON)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_OP !== "undefined" && current_XA_PI_OP != activity_from_api.XA_PI_OP){
                        console.log('XA_PI_OP')
                        console.log(current_XA_PI_OP + ' != ' + activity_from_api.XA_PI_OP)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_OPENING_NOTE !== "undefined" && current_XA_PI_OPENING_NOTE != activity_from_api.XA_PI_OPENING_NOTE){
                        console.log('XA_PI_OPENING_NOTE')
                        console.log(current_XA_PI_OPENING_NOTE + ' != ' + activity_from_api.XA_PI_OPENING_NOTE)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_PRIORITY !== "undefined" && current_XA_PI_PRIORITY != activity_from_api.XA_PI_PRIORITY){
                        console.log('XA_PI_PRIORITY')
                        console.log(current_XA_PI_PRIORITY + ' != ' + activity_from_api.XA_PI_PRIORITY)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_RESPONSABLE !== "undefined" && current_XA_PI_RESPONSABLE != activity_from_api.XA_PI_RESPONSABLE){
                        console.log('XA_PI_RESPONSABLE')
                        console.log(current_XA_PI_RESPONSABLE + ' != ' + activity_from_api.XA_PI_RESPONSABLE)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_SUB_AREA !== "undefined" && current_XA_PI_SUB_AREA != activity_from_api.XA_PI_SUB_AREA){
                        console.log('XA_PI_SUB_AREA')
                        console.log(current_XA_PI_SUB_AREA + ' != ' + activity_from_api.XA_PI_SUB_AREA)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_SUSPEND_REASON !== "undefined" && current_XA_PI_SUSPEND_REASON != activity_from_api.XA_PI_SUSPEND_REASON){
                        console.log('XA_PI_SUSPEND_REASON')
                        console.log(current_XA_PI_SUSPEND_REASON + ' != ' + activity_from_api.XA_PI_SUSPEND_REASON)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_TRAM_REASON !== "undefined" && current_XA_PI_TRAM_REASON != activity_from_api.XA_PI_TRAM_REASON){
                        console.log('XA_PI_TRAM_REASON')
                        console.log(current_XA_PI_TRAM_REASON + ' != ' + activity_from_api.XA_PI_TRAM_REASON)
                        count_diferences++
                    }else if(typeof activity_from_api.XA_PI_TRAM_SUS !== "undefined" && current_XA_PI_TRAM_SUS != activity_from_api.XA_PI_TRAM_SUS){
                        console.log('XA_PI_TRAM_SUS')
                        console.log(current_XA_PI_TRAM_SUS + ' != ' + activity_from_api.XA_PI_TRAM_SUS)
                        count_diferences++
                    }

                    //Retornando datas alteradas para o formato inicial
                    if(has_change_in_current_A_TIME_OF_BOOKING == true){
                        current_A_TIME_OF_BOOKING = current_A_TIME_OF_BOOKING + ':00'
                    }else if(has_change_in_api_A_TIME_OF_BOOKING == true){
                        activity_from_api.A_TIME_OF_BOOKING = activity_from_api.A_TIME_OF_BOOKING + ':00'
                    }

                    if(count_diferences > 0){
                        activity_from_api.id = current_id
                        final_activities_to_update.push(activity_from_api)
                    }
                }
            })
            return true
        }else{
            return false
        }
    }

    async function add_activities_to_add(activities, collect_date) {
        if(activities.length > 0){
            let data = {
                activities_to_add: activities,
                collect_date: collect_date,
            }

            const response = await fetch(
                '../../php/backlog_activities_collector/add_activities_to_add.php',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data),
                }
            )
            const add_status = await response.json()
            return add_status
        }else{
            return false
        }
    }

    async function update_activities_to_update(activities, collect_date) {
        if(activities.length > 0){
            let data = {
                activities_to_update: activities,
                collect_date: collect_date,
            }

            const response = await fetch(
                '../../php/backlog_activities_collector/update_activities_to_update.php',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data),
                }
            )
            const update_status = await response.json()
            return update_status
        }else{
            return false
        }
    }

    function get_today_date(){
        let today_date = new Date()
        return today_date.toISOString().split('T')[0]
    }

    function get_future_date(days_quantity){
        var future_date = new Date()
        future_date.setDate(future_date.getDate() + days_quantity)
        return future_date.toISOString().split('T')[0]
    }

    function get_past_date(days_quantity){
        var past_date = new Date()
        past_date.setDate(past_date.getDate() - days_quantity)
        return past_date.toISOString().split('T')[0]
    }

    check_activities_date_and_add_to_historic(start_date).then(checked_activities => {
        checked_activities = checked_activities['checked_activities']
        if(checked_activities == true){
            console.log("Atividades de backlog antigas adicionadas ao histórico")
        }else if(checked_activities == false){
            console.log("Erro ao adicionar atividades ao histótico de backlog")
        }else if(checked_activities == 'without_registers_to_histotic'){
            console.log("Sem atividades de backlog antigas para adicionar ao histórico")
        }

        get_all_activities_from_api(start_date, end_date, non_scheduled).then(all_activities_from_api => {
            activities_from_api = all_activities_from_api['all_activities_from_api']

            if(activities_from_api.length == 0){
                console.log('Nenhum registro encontrado na API')
            }else{
                console.log('Registros coletados na API: ' + activities_from_api.length)
                activities_from_api.forEach(element => {
                    if(typeof element.activityId !== "undefined"){
                        activities_ids_from_api.push(element.activityId)
                    }
                })

                get_activities_from_db_with_activities_ids_from_api(activities_ids_from_api).then(activities_from_db => {
                    if(activities_from_db != false){
                        activities_to_update = activities_from_db.activities_to_update

                        get_activities_to_add_from_activities_to_update(activities_to_update).then(activities_confrontation_status => {
                            if(activities_confrontation_status == true){
                                console.log('Executando as alterações de update e add')
                                add_activities_to_add(activities_to_add, start_date).then(add_status => {
                                    if(add_status == false){
                                        console.log('Sem registros para adicionar')
                                    }else if(add_status['add_status'] == true){
                                        console.log('Registros adicionados com sucesso:' + activities_to_add.length)
                                    }else{
                                        console.log('Erro ao adicionar registros')
                                    }
                                })

                                update_activities_to_update(final_activities_to_update, start_date).then(update_status => {
                                    if(update_status == false){
                                        console.log('Sem registros para atualizar')
                                    }else if(update_status['update_status'] == true){
                                        console.log('Registros atualizados com sucesso:' + final_activities_to_update.length)
                                    }else{
                                        console.log('Erro ao atualizar registros')
                                    }
                                    window.close() //Comentar esse comando quando for fazer manutenção
                                })
                            }else{
                                console.log('Erro ao confrontar os dados')
                            }
                        })
                    }
                })
            }
        })
    })
})