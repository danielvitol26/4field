import {append_sucess_alert, append_danger_alert, append_warning_alert} from './4field.js'

export function get_mask_generator_page(){
    //$('.get_mask_generator_page').click(function(){
        $.ajax({
            url: "../new_4field/php/mask_generator/get_mask_generator_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                select_mask_and_change_fields()
            }
        })
    //})
}

let title_mask
let fields_array

function select_mask_and_change_fields(){
    $('#select_mask').change(function(){
        // Antes de exibir os campos, devemos ocultar todos os fields
        $('div.btn_gerar_mascara').addClass('none')
        $('div.generated_mask').addClass('none')
        let second_step_children = $('.second_step').children()
        second_step_children.each(function () {
            if($(this).hasClass('none') == false){
                $(this).addClass('none')
            }
        })

        // Após ocultar todos os fields, devemos exibir somente os campos da máscara selecionada
        title_mask = $(this).find(':selected').data('title')
        let fields = $(this).find(':selected').data('fields')
        fields_array = fields.split(',')
        fields_array.forEach(field => {
            $('.' + field).removeClass('none')
        })
        $('div.btn_gerar_mascara').removeClass('none')

        create_mask_with_fields()
    })
}

function create_mask_with_fields(){
    $('button#btn_gerar_mascara').click(function(){
        validate_filters_for_mask()

        $('div.generated_mask').removeClass('none')
    })
}

function validate_filters_for_mask(){
    let count_blank_fields = 0
    fields_array.every(field => {
        let field_value = $('input[name=' + field + ']').val()

        if(typeof field_value === 'undefined'){
            field_value = $('select[name=' + field + ']').val()
        }

        if(field_value == ''){
            count_blank_fields++
            $('p.return_message_mask').text('Preencha o campo "'+ field +'" para continuar.')
            return false
        }
        return true;
    })

    if(count_blank_fields == 0){
        $('div.generated_mask_fields').html('')
        $('div.generated_mask_fields').append('<p class="title">*FIELD D&T ('+ title_mask.toUpperCase() +')*</p>')
        fields_array.forEach(field => {
            let field_value = $('input[name=' + field + ']').val()
            let field_title = $('input[name=' + field + ']').data('title')

            if(typeof field_value === 'undefined'){
                field_value = $('select[name=' + field + ']').val()
                field_title = $('select[name=' + field + ']').data('title')
            }

            $('div.generated_mask_fields').append('<p class="text">*'+ field_title +':* '+ field_value +'</p>')
            $('.' + field).removeClass('none')
        })

        $('div.generated_mask_fields').append('<p class="text mask_end">dt_3669410</p>')
        $('div.generated_mask_fields').append('<p class="text">portal_4field</p>')
    }
}

// export function get_all_subcontractor_page(){
//     $('.get_all_subcontractor_page').click(function(){
//         $.ajax({
//             url: "../new_4field/php/subcontractor/get_all_subcontractor_page.php",
//             type: "POST",
//             data: {},
//             dataType: 'html',
//             contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//             success: function (data) {
//                 $('.main-content').html(data)
//                 reprovacao()
//                 aprovacao()
//             }
//         })
//     })
// }

// export function get_subcontractor_report_page(){
//     $('.get_subcontractor_report_page').click(function(){
//         $.ajax({
//             url: "../new_4field/php/subcontractor/get_subcontractor_report_page.php",
//             type: "POST",
//             data: {},
//             dataType: 'html',
//             contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//             success: function (data) {
//                 $('.main-content').html(data)
//             }
//         })
//     })
// }

// function get_parceira(){
//     $('#parceira').change(function(){
//         let parceira = $(this).val()

//         if( parceira != "" ) {
//             $.ajax({
//                 url: "../new_4field/php/subcontractor/get_subcontractor_with_regional.php",
//                 type: "GET",
//                 data: {
//                     parceira: parceira,
//                 },
//                 dataType: 'json',
//                 contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//                 success: function (data) {
//                     //console.log(data);
//                     var options = '<option value="">--Selecione--</option>';
//                     for (var i = 0; i < data.length; i++) {
//                         options += '<option value="' + data[i].regional + '">' + data[i].regional + '</option>';
//                     }
//                     $('#regional').html(options).show();
//                     $('.carregando').hide();
//                 }
//             });
//         }

//     });

// }

// function set_subcontratada(){
//     $('.submit').click(function () {
//         let parceira = $('#parceira').val()
//         let regional = $('#regional').val()
//         let mes_subcontratacao = $('#mes_subcontratacao').val()
//         let servico = $('#servico').val()
//         let empresa = $('#empresa').val()
//         let cnpj = $('#cnpj').val()

//         if(parceira == ''){
//             append_warning_alert('Selecione uma "Parceira" para continuar.')
//         }else if(regional == ''){
//             append_warning_alert('Selecione uma "Regional" para continuar.')
//         }else if(mes_subcontratacao == ''){
//             append_warning_alert('Selecione uma "Mês subcontratacao" para continuar.')
//         }else if(servico == ''){
//             append_warning_alert('Selecione uma "Serviço" para continuar.')
//         }else if(empresa == ''){
//             append_warning_alert('Insira uma "Empresa" para continuar.')
//         }else if(cnpj == ''){
//             append_warning_alert('Insira uma "CNPJ" para continuar.')
//         }else if(!_cnpj(cnpj)){
//             append_warning_alert("CNPJ inválido, corrija para continuar.")
//         }else{
//             let dados = $('#form_sub').serialize();
//             $.ajax({
//                 url: "../new_4field/php/subcontractor/add_subcontractor.php",
//                 type: "POST",
//                 data: dados,
//                 dataType: 'json',
//                 contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//                 success: function (data) {
//                     if(data['success'] == true){
//                         append_sucess_alert("Cadastro realizado com sucesso! Prazo de 5 dias úteis para análise.")
//                         $(".alert-success").delay(6000).slideUp(200, function() {
//                             $(this).alert('close');
//                         });
//                     }else{
//                         append_danger_alert("Erro ao realizar cadastro, tente novamente mais tarde.") 
//                     }
//                 }
//             });
//         }
//     });
// }

// function mask_cnpj(){
//     $("#cnpj").mask("00.000.000/0000-00");
// }

// function _cnpj(cnpj) {

//     cnpj = cnpj?.replace(/[^\d]+/g, '');

//     if (cnpj == '') return false;

//     if (cnpj?.length != 14)
//         return false;


//     if (cnpj == "00000000000000" ||
//         cnpj == "11111111111111" ||
//         cnpj == "22222222222222" ||
//         cnpj == "33333333333333" ||
//         cnpj == "44444444444444" ||
//         cnpj == "55555555555555" ||
//         cnpj == "66666666666666" ||
//         cnpj == "77777777777777" ||
//         cnpj == "88888888888888" ||
//         cnpj == "99999999999999")
//         return false;


//     let tamanho = cnpj.length - 2
//     let numeros = cnpj.substring(0, tamanho);
//     let digitos = cnpj.substring(tamanho);
//     let soma = 0;
//     let pos = tamanho - 7;
//     for (let i = tamanho; i >= 1; i--) {
//         soma += numeros.charAt(tamanho - i) * pos--;
//         if (pos < 2)
//             pos = 9;
//     }
//     let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
//     if (resultado != digitos.charAt(0)) return false;
//     tamanho = tamanho + 1;
//     numeros = cnpj.substring(0, tamanho);
//     soma = 0;
//     pos = tamanho - 7;
//     for (let i = tamanho; i >= 1; i--) {
//         soma += numeros.charAt(tamanho - i) * pos--;
//         if (pos < 2)
//             pos = 9;
//     }
//     resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
//     if (resultado != digitos.charAt(1))
//         return false;

//     return true;

// }

// function reprovacao(){
//     $('.reprov').click( function(){
//         $('.ocultar_motivo').css('display', 'block');
//         let motivo = $('#motivo_reprovacao').val();
//         let id = $(this).attr("data-id");

//         if(motivo == ""){
//             $("#motivo_reprovacao").focus();

//         }else{
//             $.ajax({
//                 url: "../new_4field/php/subcontractor/validate_subcontractor.php",
//                 type: "POST",
//                 data: {motivo: motivo, acao: 'reprovar', id: id},
//                 dataType: 'json',
//                 contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//                 success: function (data) {
//                     if(data['success'] == true){
//                         append_sucess_alert("Reprovação realizada com sucesso!")
//                         $(".alert-success").delay(2000).slideUp(200, function() {
//                             $(this).alert('close');
//                         });
//                     }else{
//                         append_danger_alert("Erro ao realizar reprovação, tente novamente mais tarde.") 
//                     }
//                 }
//             })
//         }
//     });
// }

// function aprovacao(){
//     $('.aprov').click( function(){
//         let id = $(this).attr("data-id");

//         $.ajax({
//             url: "../new_4field/php/subcontractor/validate_subcontractor.php",
//             type: "POST",
//             data: {acao: 'aprovar', id: id},
//             dataType: 'json',
//             contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//             success: function (data) {
//                 if(data['success'] == true){
//                     append_sucess_alert("Aprovação realizada com sucesso!")
//                     $(".alert-success").delay(2000).slideUp(200, function() {
//                         $(this).alert('close');
//                     });
//                 }else{
//                     append_danger_alert("Erro ao realizar aprovação, tente novamente mais tarde.") 
//                 }
//             }
//         })

//     });
// }

