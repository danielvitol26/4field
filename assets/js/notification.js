/*
Orientações para criar uma notificação:

- Devemos importar o arquivo "notifications.js" dentro do arquivo em que queremos criar uma notificação:

import * as notification from './notification.js'

- Após, devemos chamar a função add_new_notification(titulo, subtitulo, tipo, usuario, url_destino):

let titulo = 'Exemplo de título'
let subtitulo = 'Exempro de subtitulo a ser exibido'
let tipo = 'danger'
let usuario = id_user
let url_destino = ''

notification.add_new_notification(titulo, subtitulo, tipo, usuario, url_destino)

- Para o campo tipo, temos as seguintes opções:

warning (amarelo)
danger (vermelho)
success (verde)
primary (azul)

- Temos a opção de passar a url de destino, porém não está habilitada pois precisamos criar as rotas primeiro.
*/

export function load_user_notification(){
    let usuario = $('#user_id_to_notification').val()

    let data = {
        usuario: usuario,
    }

    $.ajax({
        url: "../new_4field/php/notification/load_user_notification.php",
        type: "POST",
        data: data,
        dataType: 'html',
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        success: function (data) {
            $('div.notification_content').html(data)
            change_notifications_status(usuario)
        }
    })
}
load_user_notification()

export function reload_user_notification_every_one_minute(){
    const reload_user_notification = window.setInterval(function(){
        load_user_notification()
    }, 30000) //Atualizar para 60000 após conclusão
}
reload_user_notification_every_one_minute()

let change_to_visualized = true
export function change_user_notification_to_visualized(){
    var ul_notifications = document.querySelector("ul.notifications");
    $(document).on("click", function(e) {
        if(change_to_visualized == true){
            change_to_visualized = false
            var check_if_clicked_out_of_ul_notifications = !ul_notifications.contains(e.target)
            if(check_if_clicked_out_of_ul_notifications){
                $('span.notification_icon').remove()
                $('li.unviewed-notification').removeClass('unviewed-notification')
            }
        }
    })
}

export function change_notifications_status(usuario){
    $('.show_notifications').click(function(){
        let data = {
            usuario: usuario,
        }

        $.ajax({
            url: "../new_4field/php/notification/change_notification_status.php",
            type: "POST",
            data: data,
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                change_to_visualized = true
                change_user_notification_to_visualized()
            }
        })
    })
}

export function add_new_notification(titulo, subtitulo, tipo, usuario, url_destino){
    let data = {
        titulo: titulo,
        subtitulo: subtitulo,
        tipo: tipo,
        usuario: usuario,
        url_destino: url_destino,
    }

    $.ajax({
        url: "../new_4field/php/notification/add_new_notification.php",
        type: "POST",
        data: data,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        success: function (data) {
            //console.log(data)
        }
    })
}
