$(document).ready(function(){
    let resources_from_api = []
    let resource_internal_ids_from_api = []
    let resources_to_update = []
    let final_resources_to_update = []
    let resources_to_add = []

    async function get_all_resources_from_api() {
        console.log('Coletando registros na API')
        let data = {
            get_all_resources_from_api: true,
        }

        const response = await fetch(
            '../../php/resources_collector/get_all_resources_from_api.php',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            }
        )
        const all_resources_from_api = await response.json()
        return all_resources_from_api
    }

    async function get_resources_from_db_with_resource_internal_id_from_api(resource_internal_ids) {
        if(resource_internal_ids.length > 0){
            let data = {
                resource_internal_ids: resource_internal_ids,
            }

            const response = await fetch(
                '../../php/resources_collector/get_resources_from_db_with_resource_internal_id_from_api.php',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data),
                }
            )
            const resources_from_db = await response.json()
            return resources_from_db
        }else{
            return false
        }
    }

    async function get_resources_to_add_from_resources_to_update(resources_to_update){
        if(resources_to_update.length == 0){
            console.log('Adicionando todos os registros que vieram da API (banco zerado)')
            if(resources_from_api.length > 0){
                resources_from_api.forEach(element => {
                    let data_to_add = {}
                    if(typeof element.resourceId !== "undefined"){
                        data_to_add.resourceId = element.resourceId
                    }else{
                        data_to_add.resourceId = '-'
                    }

                    if(typeof element.name !== "undefined"){
                        data_to_add.name = element.name
                    }else{
                        data_to_add.name = '-'
                    }

                    if(typeof element.status !== "undefined"){
                        data_to_add.status = element.status
                    }else{
                        data_to_add.status = '-'
                    }

                    if(typeof element.parentResourceId !== "undefined"){
                        data_to_add.parentResourceId = element.parentResourceId
                    }else{
                        data_to_add.parentResourceId = '-'
                    }

                    if(typeof element.resourceInternalId !== "undefined"){
                        data_to_add.resourceInternalId = element.resourceInternalId
                    }else{
                        data_to_add.resourceInternalId = '-'
                    }

                    if(typeof element.parentResourceInternalId !== "undefined"){
                        data_to_add.parentResourceInternalId = element.parentResourceInternalId
                    }else{
                        data_to_add.parentResourceInternalId = '-'
                    }

                    if(typeof element.resourceType !== "undefined"){
                        data_to_add.resourceType = element.resourceType
                    }else{
                        data_to_add.resourceType = '-'
                    }

                    if(typeof element.XP_COMPANY !== "undefined"){
                        data_to_add.XP_COMPANY = element.XP_COMPANY
                    }else{
                        data_to_add.XP_COMPANY = '-'
                    }

                    resources_to_add.push(data_to_add)
                })
                return true
            }else{
                return false
            }
        }else if(resources_to_update.length > 0){
            console.log('Confrontando registros do banco com os que vieram da API')

            const get_column_from_array = (array, column_number) => array.map(row => row[column_number])
            let resource_internal_id_column = get_column_from_array(resources_to_update, 5).map(function(item) {
                return parseInt(item)
            })

            /*Add => Pega registros da API que não foram encontrados no banco
              e adiciona ao array 'resources_to_add'*/
            let add_resource_internal_id_array = resource_internal_ids_from_api.filter(a => !resource_internal_id_column.includes(a));
            add_resource_internal_id_array = add_resource_internal_id_array.map(function(item) {
                return String(item)
            })

            add_resource_internal_id_array.forEach(resource_internal_id => {
                let found_resource = resources_from_api.filter( resource => resource.resourceInternalId === parseInt(resource_internal_id))
                if(found_resource.length > 0){
                    found_resource = found_resource[0]
                    let data_to_add = {}
                    if(typeof found_resource.resourceId !== "undefined"){
                        data_to_add.resourceId = found_resource.resourceId
                    }else{
                        data_to_add.resourceId = '-'
                    }

                    if(typeof found_resource.name !== "undefined"){
                        data_to_add.name = found_resource.name
                    }else{
                        data_to_add.name = '-'
                    }

                    if(typeof found_resource.status !== "undefined"){
                        data_to_add.status = found_resource.status
                    }else{
                        data_to_add.status = '-'
                    }

                    if(typeof found_resource.parentResourceId !== "undefined"){
                        data_to_add.parentResourceId = found_resource.parentResourceId
                    }else{
                        data_to_add.parentResourceId = '-'
                    }

                    if(typeof found_resource.resourceInternalId !== "undefined"){
                        data_to_add.resourceInternalId = found_resource.resourceInternalId
                    }else{
                        data_to_add.resourceInternalId = '-'
                    }

                    if(typeof found_resource.parentResourceInternalId !== "undefined"){
                        data_to_add.parentResourceInternalId = found_resource.parentResourceInternalId
                    }else{
                        data_to_add.parentResourceInternalId = '-'
                    }

                    if(typeof found_resource.resourceType !== "undefined"){
                        data_to_add.resourceType = found_resource.resourceType
                    }else{
                        data_to_add.resourceType = '-'
                    }

                    if(typeof found_resource.XP_COMPANY !== "undefined"){
                        data_to_add.XP_COMPANY = found_resource.XP_COMPANY
                    }else{
                        data_to_add.XP_COMPANY = '-'
                    }

                    resources_to_add.push(data_to_add)
                }
            })

            /*Update => Pega registros que vieram do banco e compara
              com a API para ver os que realmente precisam de alteração
              e adiciona ao array 'final_resources_to_update'*/

            resources_to_update.forEach(resource_to_update => {
                let current_id = resource_to_update[0]
                let current_resourceId = resource_to_update[1]
                let current_name = resource_to_update[2]
                let current_status = resource_to_update[3]
                let current_parentResourceId = resource_to_update[4]
                let current_resourceInternalId = parseInt(resource_to_update[5])
                let current_parentResourceInternalId = parseInt(resource_to_update[6])
                let current_resourceType = resource_to_update[7]
                let current_XP_COMPANY = resource_to_update[8]

                let resource_from_api = resources_from_api.filter( resource => resource.resourceInternalId === parseInt(current_resourceInternalId))
                if(resource_from_api.length > 0){
                    resource_from_api = resource_from_api[0]

                    let count_diferences = 0
                    if(typeof resource_from_api.resourceId !== "undefined" && current_resourceId != resource_from_api.resourceId){
                        count_diferences++
                    }else if(typeof resource_from_api.name !== "undefined" && current_name != resource_from_api.name){
                        count_diferences++
                    }else if(typeof resource_from_api.status !== "undefined" && current_status != resource_from_api.status){
                        count_diferences++
                    }else if(typeof resource_from_api.parentResourceId !== "undefined" && current_parentResourceId != resource_from_api.parentResourceId){
                        count_diferences++
                    }else if(typeof resource_from_api.resourceInternalId !== "undefined" && current_resourceInternalId != resource_from_api.resourceInternalId){
                        count_diferences++
                    }else if(typeof resource_from_api.parentResourceInternalId !== "undefined" && current_parentResourceInternalId != resource_from_api.parentResourceInternalId){
                        count_diferences++
                    }else if(typeof resource_from_api.resourceType !== "undefined" && current_resourceType != resource_from_api.resourceType){
                        count_diferences++
                    }else if(typeof resource_from_api.XP_COMPANY !== "undefined" && current_XP_COMPANY != resource_from_api.XP_COMPANY){
                        count_diferences++
                    }

                    if(count_diferences > 0){
                        let data_to_update = {}

                        if(typeof current_id !== "undefined"){
                            data_to_update.id = current_id
                        }else{
                            data_to_update.id = '-'
                        }

                        if(typeof resource_from_api.resourceId !== "undefined"){
                            data_to_update.resourceId = resource_from_api.resourceId
                        }else{
                            data_to_update.resourceId = '-'
                        }

                        if(typeof resource_from_api.name !== "undefined"){
                            data_to_update.name = resource_from_api.name
                        }else{
                            data_to_update.name = '-'
                        }

                        if(typeof resource_from_api.status !== "undefined"){
                            data_to_update.status = resource_from_api.status
                        }else{
                            data_to_update.status = '-'
                        }

                        if(typeof resource_from_api.parentResourceId !== "undefined"){
                            data_to_update.parentResourceId = resource_from_api.parentResourceId
                        }else{
                            data_to_update.parentResourceId = '-'
                        }

                        if(typeof resource_from_api.resourceInternalId !== "undefined"){
                            data_to_update.resourceInternalId = resource_from_api.resourceInternalId
                        }else{
                            data_to_update.resourceInternalId = '-'
                        }

                        if(typeof resource_from_api.parentResourceInternalId !== "undefined"){
                            data_to_update.parentResourceInternalId = resource_from_api.parentResourceInternalId
                        }else{
                            data_to_update.parentResourceInternalId = '-'
                        }

                        if(typeof resource_from_api.resourceType !== "undefined"){
                            data_to_update.resourceType = resource_from_api.resourceType
                        }else{
                            data_to_update.resourceType = '-'
                        }

                        if(typeof resource_from_api.XP_COMPANY !== "undefined"){
                            data_to_update.XP_COMPANY = resource_from_api.XP_COMPANY
                        }else{
                            data_to_update.XP_COMPANY = '-'
                        }

                        final_resources_to_update.push(data_to_update)
                    }
                }
            })
            return true
        }else{
            return false
        }
    }

    async function add_resources_to_add(resources) {
        if(resources.length > 0){
            let data = {
                resources_to_add: resources,
            }

            const response = await fetch(
                '../../php/resources_collector/add_resources_to_add.php',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data),
                }
            )
            const add_status = await response.json()
            return add_status
        }else{
            return false
        }
    }

    async function update_resources_to_update(resources) {
        if(resources.length > 0){
            let data = {
                resources_to_update: resources,
            }

            const response = await fetch(
                '../../php/resources_collector/update_resources_to_update.php',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data),
                }
            )
            const update_status = await response.json()
            return update_status
        }else{
            return false
        }
    }

    get_all_resources_from_api().then(all_resources_from_api => {
        resources_from_api = all_resources_from_api['all_resources_from_api']

        if(resources_from_api.length == 0){
            console.log('Nenhum registro encontrado na API')
        }else{
            console.log('Registros coletados na API: ' + resources_from_api.length)
            resources_from_api.forEach(element => {
                if(typeof element.resourceInternalId !== "undefined"){
                    resource_internal_ids_from_api.push(element.resourceInternalId)
                }
            })

            get_resources_from_db_with_resource_internal_id_from_api(resource_internal_ids_from_api).then(resources_from_db => {
                if(resources_from_db != false){
                    resources_to_update = resources_from_db.resources_to_update

                    get_resources_to_add_from_resources_to_update(resources_to_update).then(resources_confrontation_status => {
                        if(resources_confrontation_status == true){
                            console.log('Executando as alterações de update e add')
                            add_resources_to_add(resources_to_add).then(add_status => {
                                if(add_status == false){
                                    console.log('Sem registros para adicionar')
                                }else if(add_status['add_status'] == true){
                                    console.log('Registros adicionados com sucesso:' + resources_to_add.length)
                                }else{
                                    console.log('Erro ao adicionar registros')
                                }
                            })

                            update_resources_to_update(final_resources_to_update).then(update_status => {
                                if(update_status == false){
                                    console.log('Sem registros para atualizar')
                                }else if(update_status['update_status'] == true){
                                    console.log('Registros atualizados com sucesso:' + final_resources_to_update.length)
                                }else{
                                    console.log('Erro ao atualizar registros')
                                }
                                window.close() //Comentar esse comando quando for fazer manutenção
                            })
                        }else{
                            console.log('Erro ao confrontar os dados')
                        }
                    })
                }
            })
        }
    })
})