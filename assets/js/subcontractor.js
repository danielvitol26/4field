import {append_sucess_alert, append_danger_alert, append_warning_alert} from './4field.js'
import * as notification from './notification.js'

//Subcontratada Add Page
export function get_subcontractor_add_page(){
    $('.get_subcontractor_add_page').click(function(){
        $.ajax({
            url: "../new_4field/php/subcontractor/get_subcontractor_add_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                get_regional_with_partner()
                add_subcontratada()
                mask_cnpj()
            }
        })
    })
}

function get_regional_with_partner(){
    $('#parceira').change(function(){
        let company = $(this).val()

        if(company != "") {
            $.ajax({
                url: "../new_4field/php/subcontractor/get_regional_with_partner.php",
                type: "POST",
                data: {
                    company: company,
                },
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                success: function (data) {
                    var options = '<option value="">--Selecione--</option>';
                    for (var i = 0; i < data.length; i++) {
                        options += '<option value="' + data[i].regional + '">' + data[i].regional + '</option>';
                    }
                    $('#regional').html(options).show();
                    $('.carregando').hide();
                }
            });
        }
    });
}

function add_subcontratada(){
    $('.submit').click(function () {
        let parceira = $('#parceira').val()
        let regional = $('#regional').val()
        let mes_subcontratacao = $('#mes_subcontratacao').val()
        let servico = $('#servico').val()
        let empresa = $('#empresa').val()
        let cnpj = $('#cnpj').val()

        if(parceira == ''){
            append_warning_alert('Selecione uma "Parceira" para continuar.')
        }else if(regional == ''){
            append_warning_alert('Selecione uma "Regional" para continuar.')
        }else if(mes_subcontratacao == ''){
            append_warning_alert('Selecione uma "Mês subcontratacao" para continuar.')
        }else if(servico == ''){
            append_warning_alert('Selecione uma "Serviço" para continuar.')
        }else if(empresa == ''){
            append_warning_alert('Insira uma "Empresa" para continuar.')
        }else if(cnpj == ''){
            append_warning_alert('Insira uma "CNPJ" para continuar.')
        }else if(!_cnpj(cnpj)){
            append_warning_alert("CNPJ inválido, corrija para continuar.")
        }else{
            let dados = $('#form_sub').serialize();
            $.ajax({
                url: "../new_4field/php/subcontractor/add_subcontractor.php",
                type: "POST",
                data: dados,
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                success: function (data) {
                    if(data['success'] == true){
                        append_sucess_alert("Cadastro realizado com sucesso! Prazo de 5 dias úteis para análise.")
                        $(".alert-success").delay(6000).slideUp(200, function() {
                            $(this).alert('close');
                        });
                        $('#parceira').val('')
                        $('#regional').val('')
                        $('#mes_subcontratacao').val('')
                        $('#servico').val('')
                        $('#empresa').val('')
                        $('#cnpj').val('')

                        //Criando notificação
                        let titulo = 'Nova solicitação de Subcontratação'
                        let subtitulo = 'Acesse "Subcontratações > Consultar Envios"'
                        let tipo = 'warning'
                        let usuario = 154 /* Daniel, depois trocar para Natasha */
                        let url_destino = ''

                        notification.add_new_notification(titulo, subtitulo, tipo, usuario, url_destino)
                    }else{
                        append_danger_alert("Erro ao realizar cadastro, tente novamente mais tarde.")
                    }
                }
            });
        }
    });
}

function mask_cnpj(){
    $("#cnpj").mask("00.000.000/0000-00");
}

function _cnpj(cnpj) {

    cnpj = cnpj?.replace(/[^\d]+/g, '');

    if (cnpj == '') return false;

    if (cnpj?.length != 14)
        return false;


    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;


    let tamanho = cnpj.length - 2
    let numeros = cnpj.substring(0, tamanho);
    let digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0)) return false;
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;

    return true;

}

//Subcontratada All Page
export function get_all_subcontractor_page(){
    $('.get_all_subcontractor_page').click(function(){
        $.ajax({
            url: "../new_4field/php/subcontractor/get_all_subcontractor_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                show_motivo_reprovacao()
                aprovar_subcontratacao()
                change_filter_to_subcontractor_all_page()
                get_subcontractor_count_to_filter()
            }
        })
    })
}

function change_filter_to_subcontractor_all_page(){
    $('button.filter').click(function(){
        let filter_status = $(this).data('status')

        let data = {
            status_subcontratacao: filter_status,
        }

        $.ajax({
            url: "../new_4field/php/subcontractor/get_all_subcontractor_page.php",
            type: "POST",
            data: data,
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                change_filter_to_subcontractor_all_page()
                show_motivo_reprovacao()
                aprovar_subcontratacao()
                get_subcontractor_count_to_filter()
            }
        })
    })
}

function get_subcontractor_count_to_filter(){
    $(document).ready(function(){
        $.ajax({
            url: "../new_4field/php/subcontractor/get_subcontractor_count_to_filter.php",
            type: "POST",
            data: {count: true},
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                let count_pending = data['count_pending']
                let count_approved = data['count_approved']
                let count_disapproved = data['count_disapproved']

                $('span.pending').text(count_pending)
                $('span.approved').text(count_approved)
                $('span.disapproved').text(count_disapproved)
            }
        })
    })
}

function show_motivo_reprovacao(){
    $('button.reprovar').click(function(){
        let id_subcontratacao = $(this).data('id')
        $(this).removeClass('reprovar')
        $(this).addClass('confirmar-reprovacao')
        $('button#aprovar-' + id_subcontratacao).addClass('none')
        $('button#cancelar-' + id_subcontratacao).removeClass('none')
        $('#motivo-' + id_subcontratacao).removeClass('none')
        close_motivo_reprovacao()
        reprovar_subcontratacao(id_subcontratacao)
    })
}

function close_motivo_reprovacao(){
    $('button.cancelar').click(function(){
        let id_subcontratacao = $(this).data('id')
        $('#motivo-' + id_subcontratacao).addClass('none')
        $('button#cancelar-' + id_subcontratacao).addClass('none')
        $('button#aprovar-' + id_subcontratacao).removeClass('none')
        $('button#reprovar-' + id_subcontratacao).addClass('reprovar')
        $('button#reprovar-' + id_subcontratacao).removeClass('confirmar-reprovacao')
    })
}

function reprovar_subcontratacao(id_subcontratacao){
    $('button.confirmar-reprovacao').click(function(){
        let motivo = $('#motivo-' + id_subcontratacao + ' textarea').val()
        let id_user = $(this).attr("data-user");

        if(motivo == ""){
            $('#motivo-' + id_subcontratacao + ' textarea').focus()
        }else{
            $.ajax({
                url: "../new_4field/php/subcontractor/set_subcontractor_status.php",
                type: "POST",
                data: {
                    motivo: motivo,
                    acao: 'reprovar',
                    id: id_subcontratacao
                },
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                success: function (data) {
                    if(data['success'] == true){
                        append_sucess_alert("Reprovação realizada com sucesso!")
                        $('#action-buttons-' + id_subcontratacao).addClass('none')
                        $('#motivo-' + id_subcontratacao).addClass('none')
                        $('spam#spam-status-' + id_subcontratacao).text('Reprovado')
                        $('spam#spam-motivo-' + id_subcontratacao).text(motivo)
                        $('li#li-motivo-' + id_subcontratacao).removeClass('none')
                        $('div.sub-' + id_subcontratacao).addClass('none')
                        get_subcontractor_count_to_filter()

                        $(".alert-success").delay(2000).slideUp(200, function() {
                            $(this).alert('close');
                        });

                        //Criando notificação
                        let titulo = 'Subcontratação reprovada'
                        let subtitulo = 'Acesse "Subcontratações > Consultar Envios"'
                        let tipo = 'danger'
                        let usuario = id_user
                        let url_destino = ''

                        notification.add_new_notification(titulo, subtitulo, tipo, usuario, url_destino)
                    }else{
                        append_danger_alert("Erro ao realizar reprovação, tente novamente mais tarde.")
                    }
                }
            })
        }
    })
}

function aprovar_subcontratacao(){
    $('.aprovar').click( function(){
        let id_subcontratacao = $(this).attr("data-id")
        let id_user = $(this).attr("data-user")

        $.ajax({
            url: "../new_4field/php/subcontractor/set_subcontractor_status.php",
            type: "POST",
            data: {
                motivo: '',
                acao: 'aprovar',
                id: id_subcontratacao
            },
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                if(data['success'] == true){
                    append_sucess_alert("Aprovação realizada com sucesso!")
                    $('spam#spam-status-' + id_subcontratacao).text('Aprovado')
                    $('div.sub-' + id_subcontratacao).addClass('none')
                    get_subcontractor_count_to_filter()

                    //Criando notificação
                    let titulo = 'Subcontratação aprovada'
                    let subtitulo = 'Acesse "Subcontratações > Consultar Envios"'
                    let tipo = 'success'
                    let usuario = id_user
                    let url_destino = ''

                    notification.add_new_notification(titulo, subtitulo, tipo, usuario, url_destino)

                    $(".alert-success").delay(2000).slideUp(200, function() {
                        $(this).alert('close');
                    });
                }else{
                    append_danger_alert("Erro ao realizar aprovação, tente novamente mais tarde.")
                }
            }
        })

    });
}

//Subcontratada Report Page
export function get_subcontractor_report_page(){
    $('.get_subcontractor_report_page').click(function(){
        $.ajax({
            url: "../new_4field/php/subcontractor/get_subcontractor_report_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
            }
        })
    })
}
