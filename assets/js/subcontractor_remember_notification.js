async function get_users_to_notify(){
    let data = {
        get_users_to_notify: true,
    }

    const response = await fetch(
        '../../php/subcontractor/get_users_to_remember_to_add.php',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        }
    )
    const users_to_notify = await response.json()
    return users_to_notify
}

function send_notification_to_remember_to_add_suncontractor(users_to_notify){
    users_to_notify.forEach(user => {
        console.log(user[1])
        let user_id = user[1]

        let titulo = 'Cadastre suas Subcontratações'
        let subtitulo = 'Acesse "Subcontratações > Cadastrar Nova"'
        let tipo = 'primary'
        let usuario = user_id
        let url_destino = ''

        add_new_notification(titulo, subtitulo, tipo, usuario, url_destino)
    });
}

function add_new_notification(titulo, subtitulo, tipo, usuario, url_destino){
    let data = {
        titulo: titulo,
        subtitulo: subtitulo,
        tipo: tipo,
        usuario: usuario,
        url_destino: url_destino,
    }

    $.ajax({
        url: "../../php/notification/add_new_notification.php",
        type: "POST",
        data: data,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        success: function (data) {
            console.log(data)
        }
    })
}

get_users_to_notify().then(users_to_notify => {
    users_to_notify = users_to_notify['users_to_notify']
    send_notification_to_remember_to_add_suncontractor(users_to_notify)
})
