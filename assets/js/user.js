import {append_sucess_alert, append_danger_alert, append_warning_alert} from './4field.js'

/*Users Individual Functions*/
export function get_user_profile_page(){
    $('.get_user_profile_page').click(function(){
        $.ajax({
            url: "../new_4field/php/user/get_user_profile_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                $('button#update_user_profile').click(function(){
                    let user_id = $(this).data('id')
                    let resource_id = $('input[name=resource_id]').val()
                    let name = $('input[name=name]').val()
                    let company = $('select[name=company]').val()
                    let type = $('select[name=type]').val()
                    let email = $('input[name=email]').val()
                    let phone = $('input[name=phone]').val()
                    update_user_profile(user_id, resource_id, name, company, type, email, phone)
                })
                $('button#change_password').click(function(){
                    let password = $('input[name=password]').val()
                    let newpassword = $('input[name=newpassword]').val()
                    let renewpassword = $('input[name=renewpassword]').val()
                    update_user_password(password, newpassword, renewpassword)
                })
            }
        })
    })
}

function update_user_profile(user_id, resource_id, name, company, type, email, phone, status='', activation='', is_adm_requisition=false){
    let data = {
        user_id: user_id,
        resource_id: resource_id,
        name: name,
        company: company,
        type: type,
        email: email,
        phone: phone,
    }

    if(status != ''){
        data.status = status
    }

    if(activation == 0){
        data.activation = false
    }else if(activation == 1){
        data.activation = true
    }

    if(is_adm_requisition != false){
        data.is_adm_requisition = true
    }

    $.ajax({
        async: false,
        url: "../new_4field/php/user/update_user_profile.php",
        type: "POST",
        data: data,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        success: function (data) {
            var update_status = data['update_status'];
            if(update_status == 'invalid_resource_id'){
                append_warning_alert('O campo "usuário" deve conter pelo menos 4 caracteres, corrija para continuar.')
            }else if(update_status == 'invalid_name'){
                append_warning_alert('O campo "nome" deve conter pelo menos 4 caracteres, corrija para continuar.')
            }else if(update_status == 'invalid_email'){
                append_warning_alert('E-mail inválido, corrija para continuar.')
            }else if(update_status == 'invalid_phone'){
                append_warning_alert('O telefone deve conter 13 dígitos numéricos, corrija para continuar.')
            }else if(update_status == 'invalid_company'){
                append_warning_alert('Selecione uma empresa para continuar.')
            }else if(update_status == 'error'){
                append_danger_alert('Erro ao atualizar dados, tente novamente mais tarde.')
            }else if(update_status == 'success'){
                append_sucess_alert('Dados atualizados com sucesso.')

                if(is_adm_requisition == true){
                    get_user_count_to_filter()
                    let current_filter = get_user_current_filter()

                    if(current_filter == 'active' && status == 0){
                        $('div.sub-' + resource_id).addClass('none')
                    }else if(current_filter == 'pending' && status == 1 || status == 0){
                        $('div.sub-' + resource_id).addClass('none')
                    }else if(current_filter == 'inactive' && status == 1){
                        $('div.sub-' + resource_id).addClass('none')
                    }
                }
            }
        }
    })
}

function update_user_password(password, newpassword, renewpassword, resource_id=''){
    let data = {
        password: password,
        newpassword: newpassword,
        renewpassword: renewpassword,
    }

    var user_update_status = ''
    if(resource_id != ''){
        data.resource_id = resource_id
    }

    $.ajax({
        async: false,
        url: "../new_4field/php/user/update_user_password.php",
        type: "POST",
        data: data,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        success: function (data) {
            if(data['update_status'] == 'blank_password'){
                append_warning_alert('Preencha todos os campos para alterar sua senha.')
                user_update_status = false
            }else if(data['update_status'] == 'old_password_diferent'){
                append_warning_alert('A senha atual informada está incorreta.')
                user_update_status = false
            }else if(data['update_status'] == 'diferent_password'){
                append_warning_alert('A nova senha e confirmação de senha devem ser iguais.')
                user_update_status = false
            }else if(data['update_status'] == 'same_password'){
                append_warning_alert('A nova senha não pode ser igual a senha atual.')
                user_update_status = false
            }else if(data['update_status'] == 'invalid_password'){
                append_warning_alert('A nova senha deve conter pelo menos 8 caracteres, com letra maiúscula, letra minúscula, número e carácter especial.')
                user_update_status = false
            }else if(data['update_status'] == 'error'){
                append_danger_alert('Erro ao atualizar senha, tente novamente mais tarde.')
                user_update_status = false
            }else if(data['update_status'] == 'data_not_received'){
                append_danger_alert('Erro ao atualizar senha, tente novamente mais tarde.')
                user_update_status = false
            }else if(data['update_status'] == 'success'){
                append_sucess_alert('Senha atualiza com sucesso.')
                if(resource_id == ''){
                    $('input[name=password]').val('')
                    $('input[name=newpassword]').val('')
                    $('input[name=renewpassword]').val('')
                }
                user_update_status = true
            }
        }
    })
    if(resource_id != ''){
        return user_update_status
    }
}

/*Users Manage Functions*/
export function get_user_manage_page(){
    $('.get_user_manage_page').click(function(){
        $.ajax({
            url: "../new_4field/php/user/get_user_manage_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                change_filter_to_manage_page()
                update_user_to_manage_page()
                recolect_user_data_to_manage_page()
                get_user_count_to_filter()
            }
        })
    })
}

function change_filter_to_manage_page(){
    $('button.filter').click(function(){
        let filter_status = $(this).data('status')
        let filter_activation = $(this).data('activation')

        let data = {
            status: filter_status,
            activation: filter_activation,
        }

        $.ajax({
            url: "../new_4field/php/user/get_user_manage_page.php",
            type: "POST",
            data: data,
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                change_filter_to_manage_page()
                update_user_to_manage_page()
                recolect_user_data_to_manage_page()
                get_user_count_to_filter()
            }
        })
    })
}

function update_user_to_manage_page(){
    $('button.save-changes').click(function(){
        let user_id = $(this).data('id')
        let form = $(this).parent().parent().find('form')
        let resource_id = form.find('input[name=resource_id]').val()
        let name = form.find('input[name=name]').val()
        let email = form.find('input[name=email]').val()
        let phone = form.find('input[name=phone]').val()
        let company = form.find('select[name=company]').val()
        let type = form.find('select[name=type]').val()
        let status = form.find('select[name=status]').val()
        let is_adm_requisition = true
        let activation = $(this).data('pending')

        let password = form.find('input[name=old_password]').val()
        let newpassword = form.find('input[name=password]').val()
        let renewpassword = form.find('input[name=repassword]').val()

        if(newpassword != '' && renewpassword != ''){
            let status_update_password = update_user_password(
                password,
                newpassword,
                renewpassword,
                resource_id
            )

            if(status_update_password == true){
                update_user_profile(
                    user_id = user_id,
                    resource_id = resource_id,
                    name = name,
                    company = company,
                    type = type,
                    email = email,
                    phone = phone,
                    status = status,
                    activation = activation,
                    is_adm_requisition = is_adm_requisition,
                )
            }
        }else{
            update_user_profile(
                user_id = user_id,
                resource_id = resource_id,
                name = name,
                company = company,
                type = type,
                email = email,
                phone = phone,
                status = status,
                activation = activation,
                is_adm_requisition = is_adm_requisition,
            )
        }
    })
}

function recolect_user_data_to_manage_page(){
    $('button.recolect-data').click(function(){
        let form = $(this).parent().parent().find('form')
        let resource_id_field = form.find('input[name=resource_id]')
        let name_field = form.find('input[name=name]')
        let company_field = form.find('select[name=company]')
        let type_field = form.find('select[name=type]')
        let status_field = form.find('select[name=status]')
        let is_adm_requisition = true

        let resource_id = resource_id_field.val()
        let data = {
            matricula: resource_id,
            is_adm_requisition: is_adm_requisition,
        }

        $.ajax({
            url: "../new_4field/php/user/get_user_data_to_sing_in.php",
            type: "POST",
            data: data,
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                if(data['updated_status'] == 'inactive'){
                    status_field.val(0)
                }

                let company_id = parseInt(data['updated_company'])

                if(data['updated_status'] == 'active'){
                    name_field.val(data['updated_name'])
                    company_field.val(company_id)
                    type_field.val(data['updated_user_type'])
                    status_field.val(1)

                    name_field.prop('disabled', true);
                    company_field.prop('disabled', true);
                    status_field.prop('disabled', true);
                }

                if(data['updated_status'] == 'out_of_toa'){
                    if(typeof data['updated_user_type'] !== 'undefined'){
                        type_field.val(data['updated_user_type'])
                    }

                    if(typeof data['updated_company'] !== 'undefined'){
                        company_field.val(company_id)
                        company_field.prop('disabled', true);
                    }
                }
            }
        })
    })
}

function get_user_count_to_filter(){
    $(document).ready(function(){
        $.ajax({
            url: "../new_4field/php/user/get_user_count_to_filter.php",
            type: "POST",
            data: {count: true},
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                let count_active = data['count_active']
                let count_pending = data['count_pending']
                let count_incative = data['count_incative']

                $('span.active').text(count_active)
                $('span.pending').text(count_pending)
                $('span.inactive').text(count_incative)
            }
        })
    })
}

function get_user_current_filter(){
    let button_filter_selected_span = $('button.selected').find('span')
    let current_filter = ''

    if(button_filter_selected_span.hasClass('active')){
        current_filter = 'active'
    }else if(button_filter_selected_span.hasClass('pending')){
        current_filter = 'pending'
    }else if(button_filter_selected_span.hasClass('inactive')){
        current_filter = 'inactive'
    }

    return current_filter
}

/*Users Recolet From API Functions*/
export function get_recolect_all_users_page(){
    $('.recolect_all_users_from_api').click(function(){
        $.ajax({
            url: "../new_4field/php/user/get_recolect_all_users_page.php",
            type: "POST",
            data: {},
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                $('.main-content').html(data)
                start_recolect_all_users()
            }
        })
    })
}

function show_recolect_form_step(){
    $('.recolect_form').removeClass('none')
    $('.recolect_progress').addClass('none')
    $('.recolect_result').addClass('none')
}

async function show_recolect_progress_step(callback=false){
    $('.recolect_form').addClass('none')
    $('.recolect_progress').removeClass('none')
    $('.recolect_result').addClass('none')

    if(callback && typeof callback === 'function'){
        await callback()
    }
}

function show_recolect_result_step(total_users_sended, total_users_founded_in_api, total_users_updated, total_users_updated_error, discovered_name, discovered_company, discovered_user_type, discovered_status){
    $('.recolect_form').addClass('none')
    $('.recolect_progress').addClass('none')
    $('.recolect_result').removeClass('none')

    $('span.total_users_sended').text(total_users_sended)
    $('span.total_users_founded_in_api').text(total_users_founded_in_api)
    $('span.total_users_updated').text(total_users_updated)
    $('span.total_users_updated_error').text(total_users_updated_error)
    $('span.discovered_name').text(discovered_name)
    $('span.discovered_company').text(discovered_company)
    $('span.discovered_user_type').text(discovered_user_type)
    $('span.discovered_status').text(discovered_status)
}

function change_progress_bar_percentual(percentual){
    let progress_bar = document.querySelector('div.progress-bar')
    progress_bar.style.width = percentual
}

var recolect_data = {
    update_name: false,
    update_company: false,
    update_type: false,
    update_status: false,

    progress_bar_percentual_per_user: 0,
    progress_bar_current_percentual: 0,
    progress_bar_current_percentual_str: '0%',

    total_users_sended: 0,
    total_users_updated: 0,
    total_users_founded_in_api: 0,
    total_users_updated_error: 0,

    discovered_name: 0,
    discovered_company: 0,
    discovered_user_type: 0,
    discovered_status: 0,
}

function start_recolect_all_users(){
    $('button.start_recollect').click(async function(){

        let users_to_recollect = $('select#users_to_rescollect').val()
        let field_name = $('input[type="checkbox"]#name')
        let field_company = $('input[type="checkbox"]#company')
        let field_type = $('input[type="checkbox"]#type')
        let field_status = $('input[type="checkbox"]#status')

        if (field_name.is(":checked")){
            recolect_data.update_name = true
        }
        if (field_company.is(":checked")){
            recolect_data.update_company = true
        }
        if (field_type.is(":checked")){
            recolect_data.update_type = true
        }
        if (field_status.is(":checked")){
            recolect_data.update_status = true
        }

        let users = await get_users_to_recollect_data(users_to_recollect)

        recolect_data.progress_bar_percentual_per_user = 100/users.length
        recolect_data.total_users_sended = users.length

        show_recolect_progress_step(async function(){
            await update_users_proccess(users)
        })
    })
}

async function update_users_proccess(users){
    if(users.length > 0){
        let user = users.shift()
        let user_resource_id = user[1]
        let user_data = await recollect_user_data_to_update(user_resource_id)

        let updated_login = user_data['updated_login']??false
        let updated_name = user_data['updated_name']??false
        let updated_company = user_data['updated_company']??false
        let updated_user_type = user_data['updated_user_type']??false
        let updated_status = user_data['updated_status']??false

        let user_data_to_update = {
            resource_id: user_resource_id,
        }

        if(recolect_data.update_name == true && updated_name != false){
            user_data_to_update.name = updated_name
            recolect_data.discovered_name++
        }

        if(recolect_data.update_company == true && updated_company != false){
            user_data_to_update.company = updated_company
            recolect_data.discovered_company++
        }

        if(recolect_data.update_type == true && updated_user_type != false){
            user_data_to_update.type = updated_user_type
            recolect_data.discovered_user_type++
        }

        if(recolect_data.update_status == true && updated_status != false && updated_status != 'out_of_toa'){
            user_data_to_update.status = updated_status
            recolect_data.discovered_status++
        }

        if(updated_status == 'out_of_toa'){
            user_data_to_update.is_in_toa = 0
        }else{
            user_data_to_update.is_in_toa = 1
            recolect_data.total_users_founded_in_api++
        }

        let final_update_status = await update_user_profile_to_recollect(user_data_to_update)

        if(final_update_status == 'success'){
            recolect_data.total_users_updated++
        }else if(final_update_status == 'error'){
            recolect_data.total_users_updated_error++
        }

        recolect_data.progress_bar_current_percentual += recolect_data.progress_bar_percentual_per_user
        recolect_data.progress_bar_current_percentual_str = '' + (recolect_data.progress_bar_current_percentual.toFixed(2)) + '%'
        change_progress_bar_percentual(recolect_data.progress_bar_current_percentual_str)

        await update_users_proccess(users)
    }else{
        show_recolect_result_step(
            recolect_data.total_users_sended,
            recolect_data.total_users_founded_in_api,
            recolect_data.total_users_updated,
            recolect_data.total_users_updated_error,
            recolect_data.discovered_name,
            recolect_data.discovered_company,
            recolect_data.discovered_user_type,
            recolect_data.discovered_status
        )

        //Resetando o recolect_data para caso sejam feitas novas coletas
        recolect_data.update_name = false
        recolect_data.update_company = false
        recolect_data.update_type = false
        recolect_data.update_status = false

        recolect_data.progress_bar_percentual_per_user = 0
        recolect_data.progress_bar_current_percentual = 0
        recolect_data.progress_bar_current_percentual_str = '0%'

        recolect_data.total_users_sended = 0
        recolect_data.total_users_updated = 0
        recolect_data.total_users_founded_in_api = 0
        recolect_data.total_users_updated_error = 0

        recolect_data.discovered_name = 0
        recolect_data.discovered_company = 0
        recolect_data.discovered_user_type = 0
        recolect_data.discovered_status = 0
    }
}

async function get_users_to_recollect_data(users_to_recollect){
    let data = {
        users_to_recollect: users_to_recollect,
    }

    let users
    $.ajax({
        async: false,
        url: '../new_4field/php/user/get_users_to_recollect_data.php',
        type: 'POST',
        data: data,
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
        success: function (data) {
            users = data['users_to_upload'];
        }
    })
    return users;
}

async function recollect_user_data_to_update(resource_id){
    let data = {
        resource_id: resource_id,
    }

    const response = await fetch(
        '../new_4field/php/user/recolect_user_data_to_update.php',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        }
    );

    return await response.json()
}

async function update_user_profile_to_recollect(user_data_to_update){
    const response = await fetch(
        '../new_4field/php/user/update_user_profile_to_recollect.php',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user_data_to_update),
        }
    )
    return (await response.json()).update_status
}
