$(document).ready(function(){
    function clear_feedback_message(){
        $('.feedback-msg p').text('')
        $('.feedback-msg p').removeClass('red-text')
        $('.feedback-msg p').removeClass('green-text')
    }

    function set_feedback_message_wrong(message){
        $('.feedback-msg p').addClass('red-text')
        $('.feedback-msg p').text(message)
    }

    function set_feedback_message_right(message){
        $('.feedback-msg p').addClass('green-text')
        $('.feedback-msg p').text(message)
    }

    function go_to_home(){
        location.href = '../../prod_online.php';
    }

    function user_log_in(){
        $('button.continue').click(function(){
            clear_feedback_message()
            resource_id = $('input.resource-id').val()
            password = $('input.senha').val()

            if(resource_id == ''){
                set_feedback_message_wrong('Preencha o campo matrícula para continuar.')
            }else if(password == ''){
                set_feedback_message_wrong('Preencha o campo senha para continuar.')
            }else{
                $.ajax({
                    url: "../new_4field/php/user/get_user_data_to_log_in.php",
                    type: "POST",
                    data: {
                        resource_id:resource_id,
                        password:password,
                    },
                    dataType: 'json',
                    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                    success: function (data) {
                        if(data['login_status'] == 'inactive_adm'){
                            set_feedback_message_wrong('Usuário inativo, entre em contato com o time de PCP.')
                        }
                        if(data['login_status'] == 'inactive_toa'){
                            set_feedback_message_wrong('Usuário inativo no TOA, entre em contato com o time de PCP.')
                        }
                        if(data['login_status'] == 'inactive_type'){
                            set_feedback_message_wrong('Tipo de perfil sem liberação de acesso.')
                        }
                        if(data['login_status'] == 'failed'){
                            set_feedback_message_wrong('E-mail ou senha incorretos, tente novamente.')
                        }
                        if(data['login_status'] == 'data_not_sended'){
                            set_feedback_message_wrong('Dados de acesso não recebidos, tente novamente.')
                        }
                        if(data['login_status'] == 'success'){
                            set_feedback_message_right('Login feito com sucesso, aguarde.')
                            go_to_home()
                        }
                    }
                })
            }
        })
    }
    user_log_in()
})