$(document).ready(function(){
    function clear_feedback_message(){
        $('.feedback-msg p').text('')
        $('.feedback-msg p').removeClass('red-text')
        $('.feedback-msg p').removeClass('green-text')
    }

    function set_feedback_message_wrong(message){
        $('.feedback-msg p').addClass('red-text')
        $('.feedback-msg p').text(message)
    }

    function set_feedback_message_right(message){
        $('.feedback-msg p').addClass('green-text')
        $('.feedback-msg p').text(message)
    }

    function show_fields_api(){
        $('div.fields-api').removeClass('none')
    }

    function show_fields_user(){
        $('div.fields-user').removeClass('none')
    }

    function close_fields(){
        $('div.resource-id').addClass('none')
        $('div.fields-user').addClass('none')
        $('div.fields-api').addClass('none')
    }

    function change_button_to_continue(){
        $('.continue').addClass('none')
        $('.send').removeClass('none')
    }

    function change_button_to_login(){
        $('.send').addClass('none')
        $('.login').removeClass('none')
    }

    var activation = 1 //Caso o usuário não seja encontrado no TOA activation = 0

    function get_data_account(){
        $('button.continue').click(function(){
            clear_feedback_message()
            resource_id = $('input.resource-id').val()
            if (resource_id == ''){
                set_feedback_message_wrong('Preencha o campo matrícula para continuar.')
            }else{
                $.ajax({
                    url: "../new_4field/php/user/get_user_data_to_sing_in.php",
                    type: "POST",
                    data: {matricula:resource_id},
                    dataType: 'json',
                    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                    success: function (data) {
                        if(data['updated_status'] == 'already_registred'){
                            set_feedback_message_wrong('Essa matrícula já possui cadastro, entre em contato com o time de PCP para recuperar a senha.')
                        }

                        if(data['updated_status'] == 'inactive'){
                            set_feedback_message_wrong('Matrícula inativa, entre em contato com o time de PCP.')
                        }

                        if(data['updated_status'] == 'active'){
                            $('input.nome').val(data['updated_name'])
                            $('select.perfil').val(data['updated_user_type'])
                            $('select.empresa').val(data['updated_company'])

                            $('input.resource-id').prop('disabled', true);
                            $('input.nome').prop('disabled', true);
                            $('select.perfil').prop('disabled', true);
                            $('select.empresa').prop('disabled', true);

                            change_button_to_continue()
                            show_fields_api()
                            show_fields_user()
                        }

                        if(data['updated_status'] == 'out_of_toa'){
                            $('input.resource-id').prop('disabled', true);
                            if(typeof data['updated_user_type'] === 'undefined'){
                                $('select.perfil').prop('disabled', false);
                            }else{
                                $('select.perfil').val(data['updated_user_type'])
                                $('select.perfil').prop('disabled', true);
                            }

                            if(typeof data['updated_company'] === 'undefined'){
                                $('select.empresa').prop('disabled', false);
                            }else{
                                $('select.empresa').val(data['updated_company'])
                                $('select.empresa').prop('disabled', true);
                            }

                            activation = 0
                            change_button_to_continue()
                            show_fields_api()
                            show_fields_user()
                        }
                    }
                });
            }
        })
    }
    get_data_account()

    function create_account(){
        $('button.send').click(function(){
            clear_feedback_message()

            resource_id = $('input.resource-id').val()
            name = $('input.nome').val()
            company = $('select[name=empresa]').val()
            type = $('select[name=perfil]').val()
            email = $('input.email').val()
            phone = $('input.phone').val()
            senha = $('input.senha').val()

            data = {
                resource_id:resource_id,
                name:name,
                company:company,
                type:type,
                email:email,
                phone:phone,
                senha:senha,
                activation:activation,
            }

            $.ajax({
                url: "../new_4field/php/user/add_user_data_to_users.php",
                type: "POST",
                data: data,
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                success: function (data) {
                    if(data['add_status'] == 'invalid_resource'){
                        set_feedback_message_wrong('Por favor, insira uma matrícula com pelo menos 1 carácter.')
                    }
                    if(data['add_status'] == 'invalid_name'){
                        set_feedback_message_wrong('Por favor, insira um nome com pelo menos 4 caracteres.')
                    }
                    if(data['add_status'] == 'invalid_company'){
                        set_feedback_message_wrong('Por favor, selecione uma empresa para continuar.')
                    }
                    if(data['add_status'] == 'invalid_type'){
                        set_feedback_message_wrong('Por favor, selecione um perfil para continuar.')
                    }
                    if(data['add_status'] == 'invalid_email'){
                        set_feedback_message_wrong('Por favor, insira um endereço de e-mail válido.')
                    }
                    if(data['add_status'] == 'invalid_phone'){
                        set_feedback_message_wrong('Por favor, insira um telefone com 13 dígitos numéricos.')
                    }
                    if(data['add_status'] == 'invalid_password'){
                        set_feedback_message_wrong('A sua senha deve conter pelo menos 8 caracteres, com letra maiúscula, letra minúscula, número e carácter especial.')
                    }
                    if(data['add_status'] == 'error_insert'){
                        set_feedback_message_wrong('Erro ao cadastrar usuário, tente novamente mais tarde.')
                    }
                    if(data['add_status'] == 'success_insert'){
                        if (activation == 1){
                            set_feedback_message_right('Cadastro realizado com sucesso.')
                        }else{
                            set_feedback_message_right('Cadastro realizado com sucesso. Solicite a liberação de acesso para o time de PCP.')
                        }
                        close_fields()
                        change_button_to_login()

                        //Enviando notificação para os responsáveis
                        get_users_to_notify().then(users_to_notify => {
                            users_to_notify = users_to_notify['users_to_notify']
                            send_notification_to_responsibles(users_to_notify, activation, name)
                        })
                    }
                }
            })
        })
    }
    create_account()

    function back_to_login(){
        $('button.login').click(function(){
            location.href = '../../login.php';
        })
    }
    back_to_login()

    // Funções para envio de notificações após cadastro de usuários
    async function get_users_to_notify(){
        let data = {
            get_users_to_notify: true,
        }

        const response = await fetch(
            '../new_4field/php/user/get_users_to_receive_notification.php',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            }
        )
        const users_to_notify = await response.json()
        return users_to_notify
    }

    function send_notification_to_responsibles(users_to_notify, activation, user_name){
        users_to_notify.forEach(user => {
            let user_id = user[1]

            let titulo
            let subtitulo
            let tipo
            if(activation == 1){
                // Para usuários que não precisam de validação
                titulo = 'Novo usuário cadastrado'
                subtitulo = '"'+ user_name +'" se cadastrou.'
                tipo = 'primary'
            }else if(activation == 0){
                // Para usuários que precisam de validação
                titulo = 'Novo usuário cadastrado'
                subtitulo = '"'+ user_name +'" aguardando validação.'
                tipo = 'warning'
            }
            let usuario = user_id
            let url_destino = ''

            add_new_notification(titulo, subtitulo, tipo, usuario, url_destino)
        });
    }

    function add_new_notification(titulo, subtitulo, tipo, usuario, url_destino){
        let data = {
            titulo: titulo,
            subtitulo: subtitulo,
            tipo: tipo,
            usuario: usuario,
            url_destino: url_destino,
        }

        $.ajax({
            url: "../new_4Field/php/notification/add_new_notification.php",
            type: "POST",
            data: data,
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            success: function (data) {
                console.log(data)
            }
        })
    }
})