<?php
class mypdo
{
    private $conexao;
    public function __construct($host='localhost', $dbname='4field', $user='root', $password='')
    {
        $this->conexao=new PDO("mysql:host=$host;dbname=$dbname", $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    }

    public function exe_sql($sql)
    {
        $stmt=$this->conexao->prepare($sql);
        return $stmt->execute();
    }
    public function return_array($sql, $mode = 'NUM')
    {
        $ret = [];
        try
        {
            $stmt = $this->conexao->query($sql);
            if($stmt){
                $stmt->execute();
                if($mode === 'NUM'){
                    $ret=$stmt->fetchAll(PDO::FETCH_NUM);
                }else if($mode === 'ASSOC'){
                    $ret=$stmt->fetchAll(PDO::FETCH_ASSOC);
                }else{
                    $ret=$stmt->fetchAll(PDO::FETCH_NUM);
                }
            }else{
                echo false;
            }
        }
        catch ( PDOException $e )
        {
            echo $e->getMessage();
        }
        return $ret;
    }

    public function select($table, $field='all', $value='all'){
        if($field == 'all'){
            $select_sql = "select * from $table";
        }else if(is_int($value) == true){
            $select_sql = "select * from $table where $field = $value";
        }else{
            $select_sql = "select * from $table where $field = '$value'";
        }
        $select = $this->return_array($select_sql);
        return $select;
    }

    public function select_multi_rules($table, $array, $order_by=''){
        if (count($array) > 0){
            $select_conditions = [];
            foreach ($array as $key => $value){
                if (is_int($value) || is_double($value)){
                    $select_conditions[] = "$key = $value";
                }else{
                    $select_conditions[] = "$key = '$value'";
                }
            }
            $select_sql = "";
            if($order_by != ''){
                $select_sql = "select * from $table where " . implode(' and ', $select_conditions) . " order by " . $order_by;
            }else{
                $select_sql = "select * from $table where " . implode(' and ', $select_conditions);
            }
            $select = $this->return_array($select_sql);
            return $select;
        }else{
            return false;
        }
    }

    public function insert($table_name, $array){
        if (count($array) > 0){
            $fields = [];
            $values = [];
            foreach ($array as $key => $value){
                $fields[] = $key;
                if (is_int($value) || is_double($value)){
                    $values[] = $value;
                }else{
                    $values[] = "'$value'";
                }
            }

            $insert_sql = 'insert into '. $table_name .' (' . implode(', ', $fields) . ') values (' . implode(', ', $values) . ')';
            $insert = $this->exe_sql($insert_sql);
            return $insert;
        }else{
            return false;
        }
    }

    public function update($table, $where, $array){
        if (count($array) > 0){
            $fields_to_update = [];
            foreach ($array as $key => $value){
                if (is_int($value) || is_double($value)){
                    $fields_to_update[] = "$key = $value";
                }else{
                    $fields_to_update[] = "$key = '$value'";
                }
            }

            $update_sql = "update $table set " . implode(', ', $fields_to_update) . " where $where";
            $update = $this->exe_sql($update_sql);
            return $update;
        }else{
            return false;
        }
    }

    public function exclude($table, $field, $value){
        $delete_sql = "delete from $table where $field = '$value'";
        $delete = $this->return_array($delete_sql);
        return $delete;
    }
}

?>