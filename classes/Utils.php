<?php
class utils
{
    public function __construct(){

    }

    public function validate_name($name){
        $regex = "/^[a-zA-Z'-]+$/";
        if(preg_match($regex, $name) == true){
            return true;
        }else{
            return false;
        }
    }

    public function validate_email($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }else{
            return false;
        }
    }

    public function validate_password($password){
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        /*Rules: 'Password should be at least 8 characters in
          length and should include at least one upper case
          letter, one number, and one special character.'*/

        if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
            return false;
        }else{
            return true;
        }
    }

    function remove_special_characters($text_to_remove){
        $final_text = preg_replace('/[^a-zA-Z0-9_ -]/s',' ',$text_to_remove);
        return $final_text;
    }

    public function get_company_from_initials($initials){
        $companys = [
            'CFC' => 'Comfica',
            'TLP' => 'Teleperformance',
            'ALP' => 'Alpitel',
            'G4T' => 'Green4t',
            'TEL' => 'Tel',
            'TIM' => '%',
        ];

        $current_company = '';

        foreach($companys as $key => $value){
            if(strtoupper($initials) == $key){
                $current_company = $value;
            }
        }

        if($current_company != ''){
            return $current_company;
        }else{
            return false;
        }
    }

    public function get_company_from_name($initials){
        $companys = [
            'Comfica' => 'Comfica',
            'Teleperformance' => 'Teleperformance',
            'Alpitel' => 'Alpitel',
            'Green4t' => 'Green4t',
            'Tel' => 'Tel',
            '%' => 'Tim',
        ];

        $current_company = '';

        foreach($companys as $key => $value){
            if(strtoupper($initials) == $key){
                $current_company = $value;
            }
        }

        if($current_company != ''){
            return $current_company;
        }else{
            return false;
        }
    }

    public function get_user_session_data(){
        $id = Null;
        $name = Null;
        $type = Null;
        $company = Null;
        $status = Null;
        $activation = Null;
        $resource_id = Null;
        $password = Null;
        $email = Null;
        $phone = Null;
        $is_in_toa = Null;

        session_start();
        if(isset($_SESSION['usuarioID'])){
            $id = $_SESSION['usuarioID'];
        }

        if(isset($_SESSION['usuarioNome'])){
            $name = $_SESSION['usuarioNome'];
        }

        if(isset($_SESSION['usuarioPerfil'])){
            $type = $_SESSION['usuarioPerfil'];
        }

        if(isset($_SESSION['usuarioEmpresa'])){
            $company = $_SESSION['usuarioEmpresa'];
        }

        if(isset($_SESSION['usuarioStatus'])){
            $status = $_SESSION['usuarioStatus'];
        }

        if(isset($_SESSION['usuarioActivation'])){
            $activation = $_SESSION['usuarioActivation'];
        }

        if(isset($_SESSION['usuarioLogin'])){
            $resource_id = $_SESSION['usuarioLogin'];
        }

        if(isset($_SESSION['usuarioSenha'])){
            $password = $_SESSION['usuarioSenha'];
        }

        if(isset($_SESSION['usuarioEmail'])){
            $email = $_SESSION['usuarioEmail'];
        }

        if(isset($_SESSION['usuarioTelefone'])){
            $phone = $_SESSION['usuarioTelefone'];
        }

        if(isset($_SESSION['usuarioToa'])){
            $is_in_toa = $_SESSION['usuarioToa'];
        }

        $user_session = [
            'user_id' => $id,
            'user_name' => $name,
            'user_type' => $type,
            'user_company' => $company,
            'user_status' => $status,
            'user_toa' => $is_in_toa,
            'user_activation' => $activation,
            'user_resource_id' => $resource_id,
            'user_password' => $password,
            'user_email' => $email,
            'user_phone' => $phone,
        ];

        return $user_session;
    }

    public function utf8_converter($array){
        array_walk_recursive($array, function(&$item, $key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
            }
        });
        return $array;
    }

    public function convert_datetime_br_to_datetime_en($datetime_br){
        $datetime_br = trim($datetime_br);
        $array_datetime_br = explode(' ', $datetime_br);
        $data_en = implode('-', array_reverse(explode('/', $array_datetime_br[0])));
        $datetime_en = $data_en . ' ' . $array_datetime_br[1];
        return $datetime_en;
    }
}

?>