<?php

class ResourceDataCollector{
		public function __construct() {
			$this->client_id = 'operationsfield@tim';
			$this->client_secret = 'f629379aeba79898463826f7a26766b7e60f7dd65c66d32e6e0a9441c9dd';
			$this->server_name_resources = 'https://api.etadirect.com/';
			$this->server_path_resources = 'rest/ofscCore/v1/resources';
		}

		public function get_client_id() {
			return $this->client_id;
		}

		public function set_client_id() {
			return 'Is not posible to edit "client_id"';
		}

		public function get_client_secret() {
			return $this->client_secret;
		}

		public function set_client_secret() {
			return 'Is not posible to edit "client_secret"';
		}

		public function get_server_name_resources(){
			return $this->server_name_resources;
		}

		public function set_server_name_resources() {
			return 'Is not posible to edit "server_name_resources"';
		}

		public function get_server_path_resources() {
			return $this->server_path_resources;
		}

		public function set_server_path_resources() {
			return 'Is not posible to edit "server_path_resources"';
		}

		public function create_path_to_all_resources(){
			$server_name_resources = $this->get_server_name_resources();
			$server_path_resources = $this->get_server_path_resources();

			$api_request_resources = ''
				.''. $server_name_resources
				.''. $server_path_resources;

			return $api_request_resources;
		}

		public function get_data_from_api($url){
			$headers = array(
				'Content-Type: application/json',
				'Authorization: Basic '. base64_encode("$this->client_id:$this->client_secret"),
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 900);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

			$result = curl_exec($ch);
			$data_from_api = json_decode(curl_exec($ch));
			$info = curl_getinfo($ch);

			if(curl_errno($ch)){
				throw new Exception(curl_error($ch));
			}

			if(!$result) {
				return false;
			}

			curl_close ($ch);
			return $data_from_api;
		}

		public function get_all_data_from_api($url){
			$all_resources = [];
			$resources = $this->get_data_from_api($url);
			$array = json_decode(json_encode($resources->items),true);
			$all_resources = array_merge($all_resources, $array);

			$has_more = 1;
			if(gettype($resources->links[2]->rel) == null){
				$has_more = 0;
			}

			while($has_more == 1){
				$next_page_url = '';
				if(gettype($resources->links[2]->rel) != null){
					if($resources->links[2]->rel == 'next'){
						$next_page_url = $resources->links[2]->href;
					}else if(isset($resources->links[3]->rel) && gettype($resources->links[3]->rel) != null){
						if($resources->links[3]->rel == 'next' && $resources->links[3]->href != ''){
							$next_page_url = $resources->links[3]->href;
						}else if(
							gettype($resources->links[2]->rel) != null &&
							$resources->links[2]->rel != 'next' &&
							gettype($resources->links[3]->rel) != null &&
							$resources->links[3]->rel != 'next'
						){
							$has_more = 0;
						}
					}else{
						$has_more = 0;
					}
				}

				if($next_page_url != ''){
					$resources = $this->get_data_from_api($next_page_url);
					if($resources != false){
						$array = json_decode(json_encode($resources->items),true);
						$all_resources = array_merge($all_resources, $array);
					}
				}
			}

			return $all_resources;
		}
	}
?>
