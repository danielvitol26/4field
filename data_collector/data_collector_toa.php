<?php
	session_start();
	include_once '../classes/Pdo.php';
	$pdo = new mypdo();

	//$sql = 'select * from usuario';
	//$teste = $pdo->return_array($sql);

	//echo '<pre>';
	//echo print_r($teste);
	//echo '</pre>';

	set_time_limit(0);
	ignore_user_abort(1);
	date_default_timezone_set('America/Fortaleza');
	$DATA_HORA = date('Y-m-d H:i:s');
	$DATA_HOJE = date('Y-m-d');

	function get_data_from_api($url = null, $username = null, $password = null, $dateFile = null, $conn=null){
		$headers = array(
			'Content-Type: application/json',
			'Authorization: Basic '. base64_encode("$username:$password"),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 900);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

		$result = curl_exec($ch);
		$atividades = json_decode(curl_exec($ch));
		$info = curl_getinfo($ch);

		if(curl_errno($ch)){
			throw new Exception(curl_error($ch));
		}

		if(!$result) {
			die("Connection Failure");
		}

		curl_close ($ch);
		echo '<br>';
		echo 'Atividades coletadas com sucesso';
		return $atividades;
	}

	//Preparando dados para conectar com a API de Atividades
	$server_name = 'https://api.etadirect.com/';
	$client_id = 'operationsfield@tim';
	$client_secret = 'f629379aeba79898463826f7a26766b7e60f7dd65c66d32e6e0a9441c9dd';
	$resource_path = 'rest/ofscCore/v1/activities?resources=Planta+Interna&includeNonScheduled=true';
	$date_from = '&dateFrom=' . $DATA_HOJE;
	$date_to = '&dateTo=' . $DATA_HOJE;
	$rules = "&date==$DATA_HOJE+and+status+in+['completed','pending','notdone','started']+and+activityType=='PI_REP'";
	$fields = "&fields=apptNumber,XA_PI_CONTRACT,status,startTime,endTime,date,XA_PI_END_ID,XA_PI_CLOSE_NOTE,XA_PI_PRIORITY,XA_PI_FAIL_SOLUTION,XA_PI_PORTAL_SOLUTION,timeOfBooking,resourceInternalId,resourceId,stateProvince,activityId,XA_PI_CREATE_DATE,XA_PI_CM";
	$api_request_activities = ''
		. $server_name .'/'
		. $resource_path .''
		. $date_from .''
		. $date_to .''
		. $rules .''
		. $fields .'';

	//echo '<br>'. $api_request_activities .'</br>';

	$dateFile = date('Ymd',strtotime($DATA_HOJE));

	$atividades = get_data_from_api($api_request_activities, $client_id, $client_secret, $dateFile);

	echo '<br>';
	echo '<pre>'. print_r($atividades->links) .'</pre>';
	echo '<br>';

	for ($x = 0; $x <= count($atividades->links); $x++){
		if ($atividades->links[$x]->rel == 'next'){
			echo '<br>';
			echo $atividades->links[$x]->rel;
			echo '<br>';
		}
	}

	for ($x = 0; $x <= count($atividades->items); $x++){
		echo '<br>';
		echo $x;
		echo '<br>';
	}

	die;

	/*$includeChildren = "all";
	$includeNonScheduled = "true";
	."&includeChildren=". $includeChildren
	."&includeNonScheduled=". $includeNonScheduled*/

	//Preparando dados para conectar com a API de Usuários

	/*$server_name = 'https://tim.fs.ocs.oraclecloud.com/';
	$client_id = 'operationsfield@tim';
	$client_secret = 'f629379aeba79898463826f7a26766b7e60f7dd65c66d32e6e0a9441c9dd';
	$resource_path = 'rest/ofscCore/v1/users?includeNonScheduled=true';
	$date_from = '&dateFrom=' . $DATA_HOJE;
	$date_to = '&dateTo=' . $DATA_HOJE;
	$rules = "&date==$DATA_HOJE+and+status+in+['completed','pending','notdone','started']+and+activityType=='PI_REP'";
	$fields = "&fields=apptNumber,XA_PI_CONTRACT,status,startTime,endTime,date,XA_PI_END_ID,XA_PI_CLOSE_NOTE,XA_PI_PRIORITY,XA_PI_FAIL_SOLUTION,XA_PI_PORTAL_SOLUTION,timeOfBooking,resourceInternalId,resourceId,stateProvince,activityId,XA_PI_CREATE_DATE,XA_PI_CM";
	$api_request_user = ''. $server_name .'/'. $resource_path .''. $date_from .''. $date_to .''. $rules .''. $fields .'';

	$api_request_user = ''. $server_name .'/'. $resource_path .'';

	echo $api_request_user;*/

	/*--------------------------------
	Posições de $atividades->items:

	[activityId] => 23109399
	[apptNumber] => TSK220900058162
	[status] => cancelled
	[stateProvince] => RJ
	[timeOfBooking] => 2022-09-16 06:17:32
	[XA_PI_CREATE_DATE] => 16/09/2022 06:17:22
	[XA_PI_CM] => TRJ-RJ-GRD01-ZSL01
	[XA_PI_END_ID] => RJRJO_0001 - (Site da móvel)
	[XA_PI_PRIORITY] => ALTA
	[XA_PI_CONTRACT] => TRJ_FMMT
	[resourceId] => TRJ_FMMT
	[resourceInternalId] => 18988
	[date] => 2022-09-16
	----------------------------------*/

	/*---------------------------------
	Posições de $info:
	[url] => https://api.etadirect.com/rest/ofscCore/v1/activities?resources=Planta+Interna&includeNonScheduled=true&dateFrom=2022-09-16&dateTo=2022-09-16&date==2022-09-16+and+status+in+['completed','pending','notdone','started']+and+activityType=='PI_REP'&fields=apptNumber,XA_PI_CONTRACT,status,startTime,endTime,date,XA_PI_END_ID,XA_PI_CLOSE_NOTE,XA_PI_PRIORITY,XA_PI_FAIL_SOLUTION,XA_PI_PORTAL_SOLUTION,timeOfBooking,resourceInternalId,resourceId,stateProvince,activityId,XA_PI_CREATE_DATE,XA_PI_CM&limit=1000&offset=0
	[content_type] => 
	[http_code] => 0
	[header_size] => 0
	[request_size] => 0
	[filetime] => -1
	[ssl_verify_result] => 0
	[redirect_count] => 0
	[total_time] => 0
	[namelookup_time] => 0
	[connect_time] => 0
	[pretransfer_time] => 0
	[size_upload] => 0
	[size_download] => 0
	[speed_download] => 0
	[speed_upload] => 0
	[download_content_length] => -1
	[upload_content_length] => -1
	[starttransfer_time] => 0
	[redirect_time] => 0
	[redirect_url] => 
	[primary_ip] => 
	[certinfo] => Array
		(
		)

	[primary_port] => 0
	[local_ip] => 
	[local_port] => 0
	----------------------------------------*/

	foreach ($atividades->items as $atividade){
		$TIPO_CONTRATO="";
		$MATRICULA="";

		echo '<pre>';
		echo print_r($atividade);
		echo '<pre>';

		if(isset($atividade->resourceId) == false){
			$MATRICULA = "";
		}else{
			$MATRICULA = $atividade->resourceId;
		}

		if(isset($atividade->activityId) == false){
			$ID_ATIVIDADE = "";
		}else{
			$ID_ATIVIDADE = $atividade->activityId;
		}

		if(isset($atividade->apptNumber) == false){
			$N_ATIVIDADE = "";
		}else{
			$N_ATIVIDADE = $atividade->apptNumber;
		}

		if(isset($atividade->XA_PI_CONTRACT) == false){
			$TIPO_CONTRATO = "";
		}else{
			$TIPO_CONTRATO = $atividade->XA_PI_CONTRACT;
		}

		if(isset($atividade->status) == false){
			$STATUS = "";
		}else{
			$STATUS = $atividade->status;
		}

		if(isset($atividade->startTime) == false){
			$INICIO = "";
		}else{
			$INICIO = $atividade->startTime;
		}

		if(isset($atividade->endTime) == false){
			$FIM = "";
		}else{
			$FIM = $atividade->endTime;
		}

		if(isset($atividade->date) == false){
			$DIA_EXECUCAO = "";
		}else{
			$DIA_EXECUCAO = $atividade->date;
		}

		if(isset($atividade->XA_PI_END_ID) == false){
			$END_ID = "";
		}else{
			$END_ID = $atividade->XA_PI_END_ID;
		}

		if(isset($atividade->XA_PI_CLOSE_NOTE) == false){
			$PONTO_CAUSADOR_FALHA = "";
		}else{
			$PONTO_CAUSADOR_FALHA = $atividade->XA_PI_CLOSE_NOTE;
		}

		if(isset($atividade->XA_PI_PRIORITY) == false){
			$PRIORIDADE = "";
		}else{
			$PRIORIDADE = $atividade->XA_PI_PRIORITY;
		}

		if(isset($atividade->XA_PI_FAIL_SOLUTION) == false){
			$ACAO_SOLUCAO_FALHA = "";
		}else{
			$ACAO_SOLUCAO_FALHA = $atividade->XA_PI_FAIL_SOLUTION;
		}

		if(isset($atividade->XA_PI_PORTAL_SOLUTION) == false){
			$SOLUCAO_PORTAL = "";
		}else{
			$SOLUCAO_PORTAL = $atividade->XA_PI_PORTAL_SOLUTION;
		}

		if(isset($atividade->timeOfBooking) == false){
			$DATA_CRIACAO = "";
		}else{
			$DATA_CRIACAO = $atividade->timeOfBooking;
		}

		if(isset($atividade->resourceId) == false){
			$MATRICULA_TECNICO = "";
		}else{
			$MATRICULA_TECNICO = $atividade->resourceId;
		}

		if(isset($atividade->XA_PI_CREATE_DATE) == false){
			$XA_PI_CREATE_DATE = "";
		}else{
			$XA_PI_CREATE_DATE = $atividade->XA_PI_CREATE_DATE;
		}

		if(isset($atividade->XA_PI_CM) == false){
			$XA_PI_CM = "";
		}else{
			$XA_PI_CM = $atividade->XA_PI_CM;
		}

		echo '--------------------------------------<br>';
		echo '<br>Matricula:'. $MATRICULA;
		echo '<br>Id_atividade:'. $ID_ATIVIDADE;
		echo '<br>N_atividade:'. $N_ATIVIDADE;
		echo '<br>Tipo_Contrato:'. $TIPO_CONTRATO;
		echo '<br>Status:'. $STATUS;
		echo '<br>Inicio:'. $INICIO;
		echo '<br>Fim:'. $FIM;
		echo '<br>Dia_execucao:'. $DIA_EXECUCAO;
		echo '<br>End_id:'. $END_ID;
		echo '<br>Ponto_causador_falha:'. $PONTO_CAUSADOR_FALHA;
		echo '<br>Prioridade:'. $PRIORIDADE;
		echo '<br>Acao_solucao_falha:'. $ACAO_SOLUCAO_FALHA;
		echo '<br>Solucao_portal:'. $SOLUCAO_PORTAL;
		echo '<br>Data_criacao:'. $DATA_CRIACAO;
		echo '<br>Matricula_tecnico:'. $MATRICULA_TECNICO;
		echo '<br>Xa_pi_create_date:'. $XA_PI_CREATE_DATE;
		echo '<br>Xa_pi_cm:'. $XA_PI_CM . '<br>';
		echo '--------------------------------------<br><br>';

		die;

		//$MATRICULA_TECNICO = $atividade->resourceInternalId;

		$TIPO_ATIVIDADE = "Planta Interna - Manutenção Corretiva";

		date_default_timezone_set('America/Fortaleza');

		//if($ACESSO == 1) {$ACESSO = "METALICO";};
		//if($ACESSO == 2) {$ACESSO = "GPON";};

		$STATUS_TOA = $STATUS;

		//$SITUACAO == "";
		if($STATUS == "notdone"){
			$STATUS = "IMPRODUTIVO";
		}

		//if($STATUS == "cancelled") {$STATUS = "CANCELADO";};
		if($STATUS == "completed"){
			$STATUS = "CONCLUIDO";
		}

		if($STATUS == "pending"){
			$STATUS = "CAMPO";
		}

		if($STATUS == "started"){
			$STATUS = "CAMPO";
		}

		//echo "Número da Ordem:" . isset($atividade->apptNumber) . ":" . $atividade->apptNumber . "<br>";

		/*$sql_consulta = "SELECT * FROM aux_uf_regional WHERE UF  = '$UF'";
						$result = mysqli_query($conn, $sql_consulta);
		while($row = mysqli_fetch_assoc($result)) {$REGIONAL =	$row['REGIONAL'];$EMPRESA =	$row['PSR'];};	*/

		$ANF = "";
		$sql_consulta_ANF = "SELECT END_ID, ANF FROM end_id_anf WHERE END_ID = '$END_ID'";
		$result = mysqli_query($conn, $sql_consulta_ANF);

		//Passou direto pelo while durante o debug do código
		while($row = mysqli_fetch_assoc($result)){
			$ANF = $row['ANF'];
		}

		$link = mysqli_connect("localhost", "root", "", "u487615999_4field");

		$SEM_PRODUCAO = "N";
		// A "tab_recursos" contêm a matrícula do técnico, nome, uf e psr (empresa) ex.- matricula: TRJ_FMMT
		$QUERY_VALIDA_TECNICO = "SELECT NOME,MATRICULA FROM tab_recursos WHERE MATRICULA like '$MATRICULA_TECNICO'";
		$VALIDA_TECNICO = mysqli_query($link, $QUERY_VALIDA_TECNICO);
		$VALIDA_TECNICO = mysqli_num_rows($VALIDA_TECNICO);

		// Buscando a matrícula do técnico na tabela de usuários - matricula ex.: TRJ_FMMT
		$QUERY_VALIDA_ACESSO_TECNICO = "SELECT NOME,usuario FROM usuarios WHERE usuario like '$MATRICULA_TECNICO'";
		$VALIDA_ACESSO_TECNICO = mysqli_query($link, $QUERY_VALIDA_ACESSO_TECNICO);
		$VALIDA_ACESSO_TECNICO = mysqli_num_rows($VALIDA_ACESSO_TECNICO);

		//Caso a matricula não seja encontrada, criamos um novo registro em "teb_recursos"
		if($VALIDA_TECNICO == 0){
			$NOME = "";
			$usuario = "operationsfield@tim";
			$senha = "f629379aeba79898463826f7a26766b7e60f7dd65c66d32e6e0a9441c9dd";
			$url = "https://api.etadirect.com/rest/ofscCore/v1/resources/$MATRICULA_TECNICO";

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_USERPWD, $usuario . ":" . $senha);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$atividades = json_decode(curl_exec($ch),true);

			if(isset($atividade->name) == false){
				$NOME = "";
			}else{
				$NOME = $atividade->name;
			}

			if(isset($atividade->XP_UF) == false){
				$UF_TEC = "";
			}else{
				$UF_TEC = $atividade->XP_UF;
			}

			if(isset($atividade->XP_COMPANY) == false){
				$psr_tec = "";
			}else{
				$psr_tec = $atividade->XP_COMPANY;
			}

			$sql = "INSERT INTO tab_recursos (MATRICULA,NOME,UF,PSR_TEC)
			VALUES ('$MATRICULA_TECNICO','$NOME','$UF_TEC','$psr_tec')";

			mysqli_query($conn, $sql);
		}

		$TIPO_RECURSO="";
		if(substr($MATRICULA_TECNICO , 0, 2) == "T3" OR $MATRICULA_TECNICO==" T3"){
			$TIPO_RECURSO = "TECNICO";
		}

		if($TIPO_RECURSO == "TECNICO"){
			//$NOME_TECNICO == "";
			$sql_consulta = "SELECT * FROM tab_recursos WHERE MATRICULA = '$MATRICULA_TECNICO'";

			$result = mysqli_query($conn, $sql_consulta);

			while($row = mysqli_fetch_assoc($result)) {
				$NOME_TECNICO =	$row['NOME'];
				$PSR_TECNICO_CONSULTA =	$row['PSR_TEC'];
				$UF_TECNICO_CONSULTA =	$row['UF'];
			}

		}

		if($TIPO_RECURSO <> "TECNICO"){
			//$NOME_TECNICO == "";
			$sql_consulta = "SELECT * FROM tab_recursos WHERE MATRICULA  = '$MATRICULA_TECNICO'";
			$result = mysqli_query($conn, $sql_consulta);

			while($row = mysqli_fetch_assoc($result)) {
				$NOME_TECNICO =	$row['NOME'];
				$UF_TECNICO_CONSULTA =	$row['UF'];
			}
		}

		if($VALIDA_ACESSO_TECNICO == 0){
			$sql = "INSERT INTO usuarios (usuario,nome,senha)
			VALUES ('$MATRICULA_TECNICO','$NOME_TECNICO','$MATRICULA_TECNICO')";
			mysqli_query($conn, $sql);
		}

		$PSR ="";
		$REGIAO ="";
		$REGIONAL = "";
		$sql_consulta = "SELECT * FROM tab_empresa_psr WHERE TIPO_CONTRATO  = '$TIPO_CONTRATO'";
		$result = mysqli_query($conn, $sql_consulta);

		while($row = mysqli_fetch_assoc($result)){
			if(isset($row['EMPRESA']) == false){
				$PSR = "";
			}else {
				$PSR = $row['EMPRESA'];
			}

			if(isset($row['REGIAO']) == false){
				$REGIAO = "";
			}else {
				$REGIAO = $row['REGIAO'];
			}

			if(isset($row['REGIONAL']) == false){
				$REGIONAL = "";
			}else {
				$REGIONAL = $row['REGIONAL'];
			}

			if(isset($row['ACESSO']) == false){
				$ACESSO = "";
			}else {
				$ACESSO = $row['ACESSO'];
			}
		}

		$UF_END_ID  = "";
		$UF_END_ID = substr($END_ID, 0, 2);

		if($UF_END_ID == "" AND $TIPO_RECURSO == "TECNICO") {
			$UF_END_ID = $UF_TECNICO_CONSULTA;
		}

		if($REGIONAL == ""){
			$sql_consulta = "SELECT * FROM uf_regional WHERE UF  = '$UF_END_ID'";
			$result = mysqli_query($conn, $sql_consulta);
			while($row = mysqli_fetch_assoc($result)){
				$REGIONAL =	$row['REGIONAL'];
			}
		}

		if($REGIONAL == ""){
			$REGIONAL = "N DEFINIDO";
		}

		if($TIPO_CONTRATO ==""){
			$REGIAO="N DEFINIDO";
			$ACESSO="N DEFINIDO";
			$EMPRESA="N DEFINIDO";
		}

		$TIPO_RECURSO = "CENTRALIZADO";

		if(substr($MATRICULA_TECNICO , 0, 2) == "T3" OR $MATRICULA_TECNICO == " T3") {
			$TIPO_RECURSO = "TECNICO";
		}

		$PSR_TECNICO_CONSULTA= "";

		if($PSR_TECNICO_CONSULTA  == "ALP" AND $TIPO_RECURSO=="TECNICO"){
			$PSR = "ALPITEL";
		}

		if($PSR_TECNICO_CONSULTA == "EZT" AND $TIPO_RECURSO=="TECNICO") {
			$PSR = "EZENTIS";
		}

		if($PSR_TECNICO_CONSULTA == "TEL" AND $TIPO_RECURSO=="TECNICO") {
			$PSR = "TEL";
		}

		if($PSR_TECNICO_CONSULTA == "TLP" AND $TIPO_RECURSO=="TECNICO") {
			$PSR = "TELEPERFORMANCE";
		}

		if($PSR_TECNICO_CONSULTA == "GREEN 4T" AND $TIPO_RECURSO=="TECNICO") {
			$PSR = "GREEN 4T";
		}

		if(substr($NOME_TECNICO, 0, 4) == "ALP-") {
			$TIPO_RECURSO = "TECNICO";
			$PSR="ALPITEL";
		}

		if(substr($NOME_TECNICO, 0, 4) == "EZT-") {
			$TIPO_RECURSO = "TECNICO";
			$PSR="EZENTIS";
		}

		if(substr($NOME_TECNICO, 0, 4) == "TEL-") {
			$TIPO_RECURSO = "TECNICO";
			$PSR="TEL";
		}

		if(substr($NOME_TECNICO, 0, 4) == "TLP-") {
			$TIPO_RECURSO = "TECNICO";
			$PSR="TELEPERFORMANCE";
		}

		if(substr($NOME_TECNICO, 0, 3) == "GRE") {
			$PSR = "GREEN 4T";
			$TIPO_RECURSO = "TECNICO";
		}

		if(substr($NOME_TECNICO, 0, 3) == "GREEN") {
			$PSR = "GREEN 4T";
			$TIPO_RECURSO = "TECNICO";
		}

		if(substr($NOME_TECNICO, 0, 3) == "G4T") {
			$PSR = "GREEN 4T";
			$TIPO_RECURSO = "TECNICO";
		}

		if($REGIONAL =="TCO"){
			$PSR ="TEL";
		}

		if($REGIONAL =="TLE"){
			$PSR ="TELEPERFORMANCE";
		}

		if($REGIONAL =="TNE"){
			$PSR ="TELEPERFORMANCE";
		}

		if($REGIONAL =="TNO"){
			$PSR ="TEL";
		}

		if($REGIONAL =="TSL"){
			$PSR ="COMFICA";
		}

		if($REGIONAL =="TRJ"){
			$PSR ="TELEPERFORMANCE";
		}

		if(substr($NOME_TECNICO, 0, 12) == "CENTRALIZADO") {
			$TIPO_RECURSO = "CENTRALIZADO";
		}

		if(substr($NOME_TECNICO, 0, 8) == "OPERADOR") {
			$TIPO_RECURSO = "CENTRALIZADO";
		}

		$sql = "DELETE FROM tab_producao_dia WHERE ID_ATIVIDADE = '$ID_ATIVIDADE'";
		mysqli_query($conn, $sql);

		$TECNICO_OCIOSO = "";

		$DATA_HORA = date('Y-m-d H:i:s');

		$sql = "INSERT INTO tab_producao_dia (N_ATIVIDADE,TIPO_CONTRATO,STATUS,INICIO,FIM,DIA_EXECUCAO,DATA_EXECUCAO,END_ID,PONTO_CAUSADOR_FALHA,PRIORIDADE,ACAO_SOLUCAO_FALHA,SOLUCAO_PORTAL,DATA_CRIACAO,MATRICULA_TECNICO,TIPO_ATIVIDADE,DATA_CARGA,NOME_TECNICO,PSR,REGIAO,REGIONAL,ACESSO,STATUS_TOA,TIPO_RECURSO,SEM_PRODUCAO,TECNICO_OCIOSO,ID_ATIVIDADE,XA_PI_CREATE_DATE,CM,ANF )
		VALUES ('$N_ATIVIDADE','$TIPO_CONTRATO','$STATUS','$INICIO','$FIM','$DIA_EXECUCAO','$FIM','$END_ID','$PONTO_CAUSADOR_FALHA','$PRIORIDADE','$ACAO_SOLUCAO_FALHA','$SOLUCAO_PORTAL','$DATA_CRIACAO','$MATRICULA_TECNICO','$TIPO_ATIVIDADE','$DATA_HORA','$NOME_TECNICO','$PSR','$REGIAO','$REGIONAL','$ACESSO','$STATUS_TOA','$TIPO_RECURSO','$SEM_PRODUCAO','$TECNICO_OCIOSO','$ID_ATIVIDADE','$XA_PI_CREATE_DATE','$XA_PI_CM','$ANF')";

		$VALIDA = mysqli_query($link, "SELECT ID_ATIVIDADE FROM tab_producao_dia WHERE ID_ATIVIDADE like '$ID_ATIVIDADE'");
		$VALIDA = mysqli_num_rows($VALIDA);

		if($VALIDA == 0 ){
			mysqli_query($conn, $sql);
		}

		foreach($json["links"] as $link) {
			if ($link["rel"] == "next") break;
			$key++;
		}

		if ($json) {
			$dateFile = date('Ymd_His');
			$content[] = $json["items"];

			$fp = fopen('./json/'.$dateFile.'.csv', 'a+');
			fwrite($fp, json_encode($content));
			fclose($fp);
		}

		if (@$json["links"][$key]["rel"] && (@$json["links"][$key]["rel"] == "next")){
			$retorno = $json["links"][$key]["href"];
		}else{
			$retorno = "finish";
		}

		return $retorno;
	}

	$dateFile = date('Ymd',strtotime($dateTo));

	while ($url != "finish") {
		$url = getActivities($url, $username, $password, $dateFile);
	}

	date_default_timezone_set('America/Fortaleza');
	$DATA_HORA = date('Y-m-d H:i:s');
	$DATA_HOJE = date('Y-m-d');

	$sql_consulta = "SELECT DIA_EXECUCAO,MATRICULA_TECNICO FROM tab_producao_dia WHERE DIA_EXECUCAO  = '$DATA_HOJE' AND TIPO_RECURSO LIKE 'TECNICO' GROUP BY MATRICULA_TECNICO";
	$result = mysqli_query($conn, $sql_consulta);

	while($row = mysqli_fetch_assoc($result)) {
		$link = mysqli_connect("localhost", "root", "","u487615999_4field");

		$MATRICULA_TECNICO = $row['MATRICULA_TECNICO'];
		$CONTA_PRODUCAO = mysqli_query($link, "SELECT * FROM  tab_producao_dia WHERE DIA_EXECUCAO like '$DATA_HOJE' AND STATUS LIKE 'CONCLUIDO' AND MATRICULA_TECNICO LIKE '$MATRICULA_TECNICO'");
		$CONTA_PRODUCAO = mysqli_num_rows($CONTA_PRODUCAO);

		$SEM_PRODUCAO = "N";
		IF($CONTA_PRODUCAO == 0) {
			$SEM_PRODUCAO = "S";
		}

		$sql = "UPDATE tab_producao_dia SET SEM_PRODUCAO = '$SEM_PRODUCAO' WHERE MATRICULA_TECNICO = '$MATRICULA_TECNICO' AND DIA_EXECUCAO = '$DATA_HOJE'";
		mysqli_query($conn, $sql);

		$CONTA_OCIOSO = mysqli_query($link, "SELECT * FROM  tab_producao_dia WHERE DIA_EXECUCAO like '$DATA_HOJE' AND STATUS LIKE 'CAMPO' AND MATRICULA_TECNICO LIKE '$MATRICULA_TECNICO'");
		$CONTA_OCIOSO = mysqli_num_rows($CONTA_OCIOSO);
		$OCIOSO = "N";
		IF($CONTA_OCIOSO ==0){
			$OCIOSO = "S";
		}

		$sql = "UPDATE tab_producao_dia SET TECNICO_OCIOSO = '$OCIOSO' WHERE MATRICULA_TECNICO = '$MATRICULA_TECNICO' AND DIA_EXECUCAO = '$DATA_HOJE'";
		mysqli_query($conn, $sql);

		//echo "PRODUCAO=".$CONTA_PRODUCAO;
	}

	$CONTA_AGENDA = mysqli_query($link, "SELECT * FROM  tab_producao WHERE DIA_EXECUCAO like '$DATA_HOJE'");
	$CONTA_AGENDA = mysqli_num_rows($CONTA_AGENDA);
	echo '<br>Conta agenda: '. $CONTA_AGENDA;

	$CONTA_AGENDA_atual = mysqli_query($link, "SELECT * FROM  tab_producao_dia WHERE DIA_EXECUCAO like '$DATA_HOJE'");
	$CONTA_AGENDA_atual = mysqli_num_rows($CONTA_AGENDA_atual);
	echo '<br>Conta agenda atual: '. $CONTA_AGENDA_atual;
	$CONTA_AGENDA_atual = ($CONTA_AGENDA_atual + 200);
	echo '<br>Conta agenda atual + 200: '. $CONTA_AGENDA_atual;

	IF($CONTA_AGENDA_atual >= $CONTA_AGENDA){
		$sql = "DELETE FROM tab_producao WHERE DIA_EXECUCAO = '$DATA_HOJE'";
		mysqli_query($conn, $sql);

		$sql = "INSERT INTO tab_producao SELECT * FROM  tab_producao_dia WHERE DIA_EXECUCAO = '$DATA_HOJE'";
		mysqli_query($conn, $sql);

		$sql = "DELETE FROM tab_producao_dia_view WHERE ID NOT LIKE 'X'";
		mysqli_query($conn, $sql);

		$sql = "INSERT INTO tab_producao_dia_view SELECT * FROM  tab_producao_dia WHERE DIA_EXECUCAO = '$DATA_HOJE'";
		mysqli_query($conn, $sql);
	}

	echo "<h1>Arquivo gerado...</h1>"
?>
