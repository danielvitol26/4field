<?php

	class UserDataCollector{

		public function __construct() {
			$this->server_name_user = 'https://tim.fs.ocs.oraclecloud.com/';
			$this->server_name_resource = 'https://api.etadirect.com/';
			$this->client_id = 'operationsfield@tim';
			$this->client_secret = 'f629379aeba79898463826f7a26766b7e60f7dd65c66d32e6e0a9441c9dd';
			$this->inactive_enrollments = array();
			$this->active_enrollments = array();
			$this->users_without_company = array();
			$this->users_out_toa = array();
			$this->discovered_companies = 0;
		}

		public function get_server_name() {
			return $this->server_name;
		}

		public function set_server_name() {
			return 'Is not posible to edit "server_name"';
		}

		public function get_client_id() {
			return $this->client_id;
		}

		public function set_client_id() {
			return 'Is not posible to edit "client_id"';
		}

		public function get_client_secret() {
			return $this->client_secret;
		}

		public function set_client_secret() {
			return 'Is not posible to edit "client_secret"';
		}

		public function get_inactive_enrollments() {
			return $this->inactive_enrollments;
		}

		public function set_inactive_enrollments($resource_id) {
			$this->inactive_enrollments[] = $resource_id;
		}

		public function get_active_enrollments() {
			return $this->active_enrollments;
		}

		public function set_active_enrollments($user_data) {
			if ($user_data){
				$this->active_enrollments[] = $user_data;
			}
		}

		public function get_users_without_company() {
			return $this->users_without_company;
		}

		public function set_users_without_company($user_data) {
			$this->users_without_company[] = $user_data;
		}

		public function get_users_out_toa() {
			return $this->users_out_toa;
		}

		public function set_users_out_toa($resource_id) {
			$this->users_out_toa[] = $resource_id;
		}

		public function get_discovered_companies() {
			return $this->discovered_companies;
		}

		public function set_discovered_companies($resource_id) {
			if ($resource_id){
				++$this->discovered_companies;
			}
		}

		public function get_data_from_api($url){
			$headers = array(
				'Content-Type: application/json',
				'Authorization: Basic '. base64_encode("$this->client_id:$this->client_secret"),
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 900);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

			$result = curl_exec($ch);
			$data_from_api = json_decode(curl_exec($ch));
			$info = curl_getinfo($ch);

			if(curl_errno($ch)){
				throw new Exception(curl_error($ch));
			}

			if(!$result) {
				return false;
			}

			curl_close ($ch);
			return $data_from_api;
		}

		public function create_path_to_resource($resource_id){
			$path_resources = 'rest/ofscCore/v1/resources/';
			$api_request_resources = ''
				. $this->server_name_resource .''
				. $path_resources .''
				. $resource_id .'';

			return $api_request_resources;
		}

		public function create_path_to_resource_descendants($resource_id){
			$path_resources_descendants = 'rest/ofscCore/v1/resources/'. $resource_id .'/descendants';
			$api_request_resources_descendants = ''
				. $this->server_name_resource .''
				. $path_resources_descendants .''
				. $resource_id .'';
			return $api_request_resources_descendants;
		}

		public function create_path_to_user($resource_id){
			$path_users = 'rest/ofscCore/v1/users/';
			$api_request_users = ''
				. $this->server_name_user .''
				. $path_users .''
				. $resource_id .'';

			return $api_request_users;
		}

		public function discover_user_data($user_data){
			$resourde_id = null;

			if (isset($user_data->login)){
				$resourde_id = $user_data->login;

				//echo '<br>User: ';
				//print("<pre>". print_r($user_data,true) ."</pre>");
			}

			if (isset($user_data->resourceId)){
				$resourde_id = $user_data->resourceId;

				//echo '<br>Resource: ';
				//print("<pre>". print_r($user_data,true) ."</pre>");
			}

			if (isset($user_data->status) && $user_data->status == 404){
				$this->set_users_out_toa($resourde_id);
			}

			if (isset($user_data->status) && $user_data->status != 404){

				if($user_data->status == 'inactive'){
					$this->set_inactive_enrollments($resourde_id);
					return false;
				}

				if($user_data->status == 'active'){
					$user_type = '';
					$user_company = '';

					//--------------------------------------------------------------------------------------------
					//Encontra solução para descobrir a empresa do usuário através da API (resources descendants)
					//Caso de errado, seguimos o fluxo normal para descobrir a empresa
					//--------------------------------------------------------------------------------------------

					if(!isset($user_data->userType)){
						$path_user = $this->create_path_to_user($resourde_id);
						$data_user_from_users = $this->get_data_from_api($path_user);

						//Verificando se o usuário é um técnico
						if ($resourde_id[0] == 'T' && $data_user_from_users->userType == 'Planta Interna - Técnico'){
							$user_type = '4FIELD_TECNICO';
						}

						//Verificando se o usuário é um supervisor/gerente terceirizado
						if ($resourde_id[0] == 'T' && $data_user_from_users->userType != 'Planta Interna - Técnico'){
							$user_type = '4FIELD_TERCEIRO';
						}

						//Verificando a empresa do usuário terceirizado
						if ($resourde_id[0] == 'T'){
							if(mb_strpos($user_data->name, $resourde_id)){
								$user_company = explode('-', $user_data->name)[0];
								$this->set_discovered_companies($resourde_id);
							}
						}
					}else{
						//Verificando se o usuário é um técnico
						if ($resourde_id[0] == 'T' && $user_data->userType == 'Planta Interna - Técnico'){
							$user_type = '4FIELD_TECNICO';
						}

						//Verificando se o usuário é um supervisor/gerente terceirizado
						if ($resourde_id[0] == 'T' && $user_data->userType != 'Planta Interna - Técnico'){
							$user_type = '4FIELD_TERCEIRO';
						}

						//Verificando a empresa do usuário terceirizado
						if ($resourde_id[0] == 'T'){
							if(mb_strpos($user_data->name, $resourde_id)){
								$user_company = explode('-', $user_data->name)[0];
								$this->set_discovered_companies($resourde_id);
							}
						}
					}

					//Verificando se o usuário terceiro é da empresa Altran
					if ($resourde_id[0] == 'T' && mb_strpos($user_data->name, '|')){
						$user_array = explode('|', $user_data->name);
						if(strtolower(trim($user_array[0], ' ')) == 'altran'){
							$user_company = 'TIM';
							$user_type = '4FIELD_USER';
							$this->set_discovered_companies($resourde_id);
						}
					}

					//Outros padrões nos nomes para definir as empresas
					$other_initials = array(
						'TLP' => 'TLP',
						'TLS' => 'TLP',
						'TLG' => 'TLP',
						'CFA' => 'CFC',
						'CMF' => 'CFC',
						'CMS' => 'CFC',
						'APP' => 'ALP',
						'ALP' => 'ALP',
						'TEL' => 'TEL',
						'CAB-TEL' => 'TEL',
						'TMG' => 'TEL',
						'FFA' => 'FFA',
						'FFA-CAB' => 'FFA',
						'FRJ' => 'FFA',
						'FRJ-CAB' => 'FFA',
						'CAB-FRJ' => 'FFA',
					);

					//Verificando outros padrões para definir as empresas
					if ($resourde_id[0] == 'T' && $user_company == '' && mb_strpos($user_data->name, '-')){
						$user_altran = explode('-', $user_data->name);
						foreach($other_initials as $initials => $company){
							if(strtoupper(trim($user_altran[0], ' ')) == $initials){
								$user_company = $company;
								$this->set_discovered_companies($resourde_id);
								$user_type = '4FIELD_TERCEIRO';
							}
						}
					}

					//Verificando se o usuário é um funcionário/gerente da Tim
					if ($resourde_id[0] == 'F'){
						$user_company = 'TIM';
						$this->set_discovered_companies($resourde_id);
						$user_type = '4FIELD_USER';
					}

					//Matrículas dos ADMs
					$adm_login = ['T3669410', 'T3669410', 'F8077861', 'F8034809', 'F8072144', 'T3645016', 'F8071997', 'F8067384', 'F8081597'];

					//Verificando se o usuário é um adm do 4Field
					if (in_array($resourde_id, $adm_login)) {
						$user_type = '4FIELD_ADM';
						$user_company = 'TIM';
						$this->set_discovered_companies($resourde_id);
					}

					$updated_name = strtoupper($user_data->name);
					$updated_login = strtoupper($resourde_id);
					$updated_status = $user_data->status;
					$updated_user_type = $user_type;
					$updated_company = $user_company;

					//Separando usuários sem definição de empresa
					if ($user_company == ''){
						$this->set_users_without_company(
							array(
								'name'=> $updated_name,
								'matricula'=> $updated_login,
								'status'=> $updated_status,
								'user_type'=> $updated_user_type,
							)
						);
					}

					$user_data_updated = array(
						"updated_name" => $updated_name,
						"updated_login" => $updated_login,
						"updated_status" => $updated_status,
						"updated_user_type" => $updated_user_type,
						"updated_company" => $updated_company,
					);

					$this->set_active_enrollments($user_data_updated);

					return $user_data_updated;
				}
			}
		}

	}

?>
