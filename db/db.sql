DROP DATABASE 4field;
CREATE DATABASE IF NOT EXISTS 4field DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE 4field;


DROP TABLE IF EXISTS estado;

CREATE TABLE estado (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  uf char(2) NOT NULL,

  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

INSERT INTO estado (id, nome, uf) VALUES
(1,'Acre', 'AC'),
(2,'Alagoas', 'AL'),
(3,'Amapá', 'AP'),
(4,'Amazonas', 'AM'),
(5,'Bahia', 'BA'),
(6,'Ceará', 'CE'),
(7,'Distrito Federal', 'DF'),
(8,'Espírito Santo', 'ES'),
(9,'Goiás', 'GO'),
(10,'Maranhão', 'MA'),
(11,'Mato Grosso', 'MT'),
(12,'Mato Grosso do Sul', 'MS'),
(13,'Minas Gerais', 'MG'),
(14,'Pará', 'PA'),
(15,'Paraíba', 'PB'),
(16,'Paraná', 'PR'),
(17,'Pernambuco', 'PE'),
(18,'Piauí', 'PI'),
(19,'Rio de Janeiro', 'RJ'),
(20,'Rio Grande do Norte', 'RN'),
(21,'Rio Grande do Sul', 'RS'),
(22,'Rondônia', 'RO'),
(23,'Roraima', 'RR'),
(24,'Santa Catarina', 'SC'),
(25,'São Paulo', 'SP'),
(26,'Sergipe', 'SE'),
(27,'Tocantins', 'TO');


DROP TABLE IF EXISTS regional;

CREATE TABLE regional (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  sigla varchar(5) NOT NULL,
  status int(1) NOT NULL, /*0 - Inativo | 1 - Ativo*/
  data_cadastro DATE NOT NULL,
  data_ultima_atualizacao DATE NOT NULL,

  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

INSERT INTO regional (id, nome, sigla, status, data_cadastro, data_ultima_atualizacao) VALUES
(1,'Centro Oeste', 'TCO', 1, "2022-09-22", "2022-09-22"),
(2,'Nordeste 1', 'TNE1', 1, "2022-09-22", "2022-09-22"),
(3,'Nordeste 2', 'TNE2', 1, "2022-09-22", "2022-09-22"),
(4,'Noroeste', 'TNO', 1, "2022-09-22", "2022-09-22"),
(5,'Rio de Janeiro', 'TRJ', 0, "2022-09-22", "2022-09-22"),
(6,'Rio de Janeiro 1', 'TRJ1', 1, "2022-09-22", "2022-09-22"),
(7,'Rio de Janeiro 2', 'TRJ2', 1, "2022-09-22", "2022-09-22"),
(8,'Minas Gerais', 'TMG', 1, "2022-09-22", "2022-09-22"),
(9,'Sul', 'TSL', 1, "2022-09-22", "2022-09-22"),
(10,'São Paulo', 'TSP', 0, "2022-09-22", "2022-09-22"),
(11,'São Paulo 1', 'TSP1', 1, "2022-09-22", "2022-09-22"),
(12,'São Paulo 2', 'TSP2', 1, "2022-09-22", "2022-09-22");


DROP TABLE IF EXISTS regional_estado;

CREATE TABLE regional_estado (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_regional int(11) NOT NULL,
  id_estado int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (id_regional) REFERENCES regional(id),
  FOREIGN KEY (id_estado) REFERENCES estado(id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

/* TCO - 1 */
INSERT INTO regional_estado (id_regional, id_estado) values
(1, 1), /*AC*/
(1, 7), /*DF*/
(1, 9), /*GO*/
(1, 12), /*MS*/
(1, 11), /*MT*/
(1, 22), /*RO*/
(1, 27); /*TO*/

/*TNE1 2*/
INSERT INTO regional_estado (id_regional, id_estado) values
(2, 2), /*AL*/
(2, 5), /*BA*/
(2, 18), /*PI*/
(2, 26); /*SE*/

/*TNE2 - 3*/
INSERT INTO regional_estado (id_regional, id_estado) values
(3, 6), /*CE*/
(3, 15), /*PB*/
(3, 17), /*PE*/
(3, 20); /*RN*/

/*TNO - 4*/
INSERT INTO regional_estado (id_regional, id_estado) values
(4, 4), /*AM*/
(4, 3), /*AP*/
(4, 10), /*MA*/
(4, 14), /*PA*/
(4, 23); /*RR*/

/*TRJ - 5*/
INSERT INTO regional_estado (id_regional, id_estado) values
(5, 8), /*ES*/
(5, 19); /*RJ*/

/*TRJ1 - 6*/
INSERT INTO regional_estado (id_regional, id_estado) values
(6, 19); /*RJ*/

/*TRJ2 - 7*/
INSERT INTO regional_estado (id_regional, id_estado) values
(7, 19);/*RJ*/

/*TMG - 8*/
INSERT INTO regional_estado (id_regional, id_estado) values
(8, 13); /*MG*/

/*TSL - 9*/
INSERT INTO regional_estado (id_regional, id_estado) values
(9, 16), /*PR*/
(9, 21), /*RS*/
(9, 24); /*SC*/

/*TSP - 10*/
INSERT INTO regional_estado (id_regional, id_estado) values
(10, 25); /*SP*/

/*TSP1 - 11*/
INSERT INTO regional_estado (id_regional, id_estado) values
(11, 25); /*SP*/

/*TSP2 - 12*/
INSERT INTO regional_estado (id_regional, id_estado) values
(12, 25); /*SP*/


DROP TABLE IF EXISTS contrato;

CREATE TABLE contrato (
  id INT(9) NOT NULL AUTO_INCREMENT,
  nome varchar(150) NOT NULL,
  descricao varchar(150) NOT NULL,
  data_cadastro DATE NOT NULL,
  data_ultima_atualizacao DATE NOT NULL,

  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1;

INSERT INTO contrato(id, nome, descricao, data_cadastro, data_ultima_atualizacao) VALUES
(1, "FMO", "Manutenção de fibra", "2022-09-22", "2022-09-22"),
(2, "FMMT", "Manutenção de equipamento", "2022-09-22", "2022-09-22");


DROP TABLE IF EXISTS empresa;

CREATE TABLE empresa (
  id int(9) NOT NULL AUTO_INCREMENT,
  nome varchar(150),
  sigla varchar(150),
  status int(1), /*0 - Inativo | 1 - Ativo*/
  data_cadastro DATE,
  data_ultima_atualizacao DATE,

  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=1;

INSERT INTO empresa(id, nome, sigla, status, data_cadastro, data_ultima_atualizacao) VALUES
(1, "Ezentis", "EZT", 0, "2022-09-22", "2022-09-22"),
(2, "Comfica", "CFC", 1, "2022-09-22", "2022-09-22"),
(3, "Teleperformance", "TLP", 1, "2022-09-22", "2022-09-22"),
(4, "Alpitel", "ALP", 1, "2022-09-22", "2022-09-22"),
(5, "Green4t", "G4T", 1, "2022-09-22", "2022-09-22"),
(6, "Tel", "TEL", 1, "2022-09-22", "2022-09-22"),
(7, "Tim", "TIM", 1, "2022-09-22", "2022-09-22");


DROP TABLE IF EXISTS empresa_contrato;

CREATE TABLE empresa_contrato (
  id int(9) NOT NULL AUTO_INCREMENT,
  id_empresa int(9) NOT NULL,
  id_contrato int(9) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (id_empresa) REFERENCES empresa(id),
  FOREIGN KEY (id_contrato) REFERENCES contrato(id)
) ENGINE=INNODB AUTO_INCREMENT=1;

INSERT INTO empresa_contrato (id_empresa, id_contrato) VALUES
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 1),
(7, 2);


DROP TABLE IF EXISTS empresa_regional;

CREATE TABLE empresa_regional (
  id INT(9) NOT NULL AUTO_INCREMENT,
  id_empresa INT(9) NOT NULL,
  id_regional INT(9) NOT NULL,
  data_cadastro DATE NOT NULL,
  data_ultima_atualizacao DATE NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (id_empresa) REFERENCES empresa(id),
  FOREIGN KEY (id_regional) REFERENCES regional(id)
) ENGINE=INNODB AUTO_INCREMENT=1;

INSERT INTO empresa_regional (id_empresa, id_regional, data_cadastro, data_ultima_atualizacao) VALUES
(2, 9, "2022-09-22", "2022-09-22"),
(3, 5, "2022-09-22", "2022-09-22"),
(3, 6, "2022-09-22", "2022-09-22"),
(3, 7, "2022-09-22", "2022-09-22"),
(3, 2, "2022-09-22", "2022-09-22"),
(3, 3, "2022-09-22", "2022-09-22"),
(4, 12, "2022-09-22", "2022-09-22"),
(6, 4, "2022-09-22", "2022-09-22"),
(6, 1, "2022-09-22", "2022-09-22"),
(7, 1, "2022-09-22", "2022-09-22"),
(7, 2, "2022-09-22", "2022-09-22"),
(7, 3, "2022-09-22", "2022-09-22"),
(7, 4, "2022-09-22", "2022-09-22"),
(7, 5, "2022-09-22", "2022-09-22"),
(7, 6, "2022-09-22", "2022-09-22"),
(7, 7, "2022-09-22", "2022-09-22"),
(7, 8, "2022-09-22", "2022-09-22"),
(7, 9, "2022-09-22", "2022-09-22"),
(7, 10, "2022-09-22", "2022-09-22"),
(7, 11, "2022-09-22", "2022-09-22"),
(7, 12, "2022-09-22", "2022-09-22");
