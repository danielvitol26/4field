USE 4field;

DROP TABLE IF EXISTS resources;

CREATE TABLE resources (
  id int(11) NOT NULL AUTO_INCREMENT,
  resourceId varchar(100),
  name varchar(100),
  status varchar(100),
  parentResourceId varchar(100),
  resourceInternalId varchar(100),
  parentResourceInternalId varchar(100),
  resourceType varchar(100),
  XP_COMPANY varchar(100),
  registration_date DATE,
  update_date DATE,

  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

DROP TABLE IF EXISTS backlog_activities;

CREATE TABLE backlog_activities (
  id int(11) NOT NULL AUTO_INCREMENT,
  XA_PI_CREATE_DATE DATETIME,
  date DATE,
  A_TIME_OF_BOOKING DATETIME,
  activityId varchar(100),
  apptNumber varchar(100),
  XA_PI_EVENT varchar(7000),
  activityType varchar(100),
  status varchar(100),
  resourceInternalId varchar(100),
  resourceId varchar(100),
  XA_ORIGIN_BUCKET varchar(100),
  regional varchar(100),
  contract varchar(100),
  company varchar(100),
  stateProvince varchar(100),
  city varchar(100),
  XA_EXECUTOR_USER varchar(100),
  startTime varchar(100),
  endTime varchar(100),
  XA_PI_ALARM_TYPE varchar(100),
  XA_PI_FAIL_TYPE varchar(100),
  XA_PI_CM varchar(100),
  XA_PI_END_ID varchar(100),
  XA_PI_NETWORK_ELEMENT varchar(100),
  XA_PI_NE_TYPE varchar(100),
  XA_PI_NETWORK varchar(100),
  XA_PI_NOTDONE_REASON varchar(100),
  XA_PI_OP varchar(100),
  XA_PI_OPENING_NOTE varchar(7000),
  XA_PI_PRIORITY varchar(100),
  XA_PI_RESPONSABLE varchar(100),
  XA_PI_SUB_AREA varchar(100),
  XA_PI_SUSPEND_REASON varchar(100),
  XA_PI_TRAM_REASON varchar(4000),
  XA_PI_TRAM_SUS varchar(100),
  collect_date DATE,

  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET utf8 COLLATE utf8_unicode_ci;

DROP TABLE IF EXISTS backlog_activities_historic;

CREATE TABLE backlog_activities_historic (
  id int(11) NOT NULL AUTO_INCREMENT,
  XA_PI_CREATE_DATE DATETIME,
  date DATE,
  A_TIME_OF_BOOKING DATETIME,
  activityId varchar(100),
  apptNumber varchar(100),
  XA_PI_EVENT varchar(7000),
  activityType varchar(100),
  status varchar(100),
  resourceInternalId varchar(100),
  resourceId varchar(100),
  XA_ORIGIN_BUCKET varchar(100),
  regional varchar(100),
  contract varchar(100),
  company varchar(100),
  stateProvince varchar(100),
  city varchar(100),
  XA_EXECUTOR_USER varchar(100),
  startTime varchar(100),
  endTime varchar(100),
  XA_PI_ALARM_TYPE varchar(100),
  XA_PI_FAIL_TYPE varchar(100),
  XA_PI_CM varchar(100),
  XA_PI_END_ID varchar(100),
  XA_PI_NETWORK_ELEMENT varchar(100),
  XA_PI_NE_TYPE varchar(100),
  XA_PI_NETWORK varchar(100),
  XA_PI_NOTDONE_REASON varchar(100),
  XA_PI_OP varchar(100),
  XA_PI_OPENING_NOTE varchar(7000),
  XA_PI_PRIORITY varchar(100),
  XA_PI_RESPONSABLE varchar(100),
  XA_PI_SUB_AREA varchar(100),
  XA_PI_SUSPEND_REASON varchar(100),
  XA_PI_TRAM_REASON varchar(4000),
  XA_PI_TRAM_SUS varchar(100),
  collect_date DATE,

  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET utf8 COLLATE utf8_unicode_ci;
