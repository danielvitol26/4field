DROP TABLE IF EXISTS notificacao;

CREATE TABLE notificacao (
    id INT(9) NOT NULL AUTO_INCREMENT,
    titulo varchar(200) NOT NULL,
    subtitulo varchar(200) NOT NULL,
    tipo varchar(150) NOT NULL, /* warning (amarelo), danger (vermelho), success (verde), primary (azul) */
    status_notification int(1) NOT NULL, /*(1 - visualizada ou 0 - não visualizada)*/
    usuario int(9) NOT NULL,
    data_criacao DATETIME NOT NULL,
    data_visualizacao DATETIME,
    url_destino varchar(500),

    PRIMARY KEY (id),
    FOREIGN KEY (usuario) REFERENCES usuario(id)
) ENGINE=INNODB AUTO_INCREMENT=1 CHARACTER SET utf8 COLLATE utf8_unicode_ci;


/* Tabela com usuários a serem notifcados após solicitação de subcontratação */
DROP TABLE IF EXISTS subcontratacao_notificacao;

CREATE TABLE subcontratacao_notificacao (
  id int(11) NOT NULL AUTO_INCREMENT,
  usuario int(150) DEFAULT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usuario) REFERENCES usuario(id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

insert into subcontratacao_notificacao(id, usuario) values 
(1, 13),
(2, 136),
(3, 154);


/* Tabela com usuários a serem notifcados após cadastro de novo usuário */
DROP TABLE IF EXISTS usuario_notificacao;

CREATE TABLE usuario_notificacao (
  id int(11) NOT NULL AUTO_INCREMENT,
  usuario int(150) DEFAULT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usuario) REFERENCES usuario(id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

insert into usuario_notificacao(id, usuario) values 
(1, 46), /* Lindamara */
(2, 10), /* Isaias */
(3, 136), /* Carlos */
(4, 144), /* Quaglioz */
(5, 155); /* Daniel */