DROP TABLE IF EXISTS subcontratacao;

CREATE TABLE subcontratacao (
  id int(11) NOT NULL AUTO_INCREMENT,
  regional varchar(5) DEFAULT NULL,
  parceira varchar(20) DEFAULT NULL,
  mes_subcontracao varchar(20) DEFAULT NULL,
  servico varchar(150) DEFAULT NULL,
  empresa varchar(100) DEFAULT NULL,
  cnpj varchar(20) DEFAULT NULL,
  data_criacao datetime DEFAULT NULL,
  usuario_criacao int(150) DEFAULT NULL,
  status_opm varchar(20) DEFAULT NULL,
  usuario_validacao_opm int(200) DEFAULT NULL,
  data_validacao_opm datetime DEFAULT NULL,
  motivo_reprovacao_opm varchar(500) DEFAULT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usuario_criacao) REFERENCES usuario(id),
  FOREIGN KEY (usuario_validacao_opm) REFERENCES usuario(id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

DROP TABLE IF EXISTS subcontratacao_servico;

CREATE TABLE subcontratacao_servico (
  id int(11) NOT NULL AUTO_INCREMENT,
  servico varchar(200) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

insert into subcontratacao_servico(id, servico) values 
(1,'Serviços de alpinista industrial'),
(2,'Andaime'),
(3,'Cesto aéreo'),
(4,'Munk'),
(5,'Escolta armada'),
(6,'Proteção física (equipamentos, bastidores)'),
(7,'Locação de gmg s de alta capacidade'),
(8,'Servicos de remoção de abelhas, vespas e aves'),
(9,'Dedetização de pragas e insetos (ex: formigas,baratas,ratos,pulgas,carrapatos)'),
(10,'Retirada de entulho'),
(11,'Descarte de residuos sólidos /líquidos /gases /lâmpadas e reatores'),
(12,'Emissão do laudo de spda (sistema de protecao contra descargas atmosfericas)'),
(13,'Emissão do laudo de spci (sistema de protecao contra incendios)'),
(14,'Análise e laudo da qualidade do ar interior (area de conforto)'),
(15,'Análise e laudo de ruido ambiental'),
(16,'Manutenção e limpeza com utilização de produtos químicos (fispqs, epcs e epis)'),
(17,'Execução de obras civis'),
(18,'Materiais'),
(19,'Calibração de instrumentos'),
(20,'Instalação e manutenção'),
(21,'Servicos de zeladoria'),
(22,'Outros');
