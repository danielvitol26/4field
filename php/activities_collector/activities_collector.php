<!DOCTYPE html>
<html>
    <head>
        <title>Activities Collector</title>
    </head>
    <body>
        <?php

            /* Relação da colunas da API com as colunas do Franscesco

            XA_PI_CREATE_DATE => Criação do NTT
            date => Não tem na tabela
            A_TIME_OF_BOOKING => Não tem na tabela
            activityId => Não tem na tabela
            apptNumber => Número de Ordem
            XA_PI_EVENT => Não tem na tabela
            activityType => Não tem na tabela
            status => Estado
            resourceInternalId => Não tem na tabela
            resourceId => Não tem na tabela
            XA_ORIGIN_BUCKET => Tipo Contrato
            regional => Não tem na tabela
            contract => Não tem na tabela
            company => Não tem na tabela
            stateProvince => UF
            city => Não tem na tabela
            XA_EXECUTOR_USER => Não tem na tabela
            startTime => Não tem na tabela
            endTime => Não tem na tabela
            XA_PI_ALARM_TYPE => Título do Alarme
            XA_PI_FAIL_TYPE => Tipo da Falha
            XA_PI_CM => Não tem na tabela
            XA_PI_END_ID => END_ID
            XA_PI_NETWORK_ELEMENT => Não tem na tabela
            XA_PI_NE_TYPE => Tipo de NE
            XA_PI_NETWORK => Rede
            XA_PI_NOTDONE_REASON => Não tem na tabela
            XA_PI_OP => Não tem na tabela
            XA_PI_OPENING_NOTE => Nota de Abertura
            XA_PI_PRIORITY => Prioridade
            XA_PI_RESPONSABLE => Não tem na tabela
            XA_PI_SUB_AREA => Não tem na tabela
            XA_PI_SUSPEND_REASON => Não tem na tabela
            XA_PI_TRAM_REASON => Não tem na tabela
            XA_PI_TRAM_SUS => Não tem na tabela
            */

            include_once '../../data_collector/data_collector_activitie.php';
            $activitie_collector = new ActivitieDataCollector();
            $path_activities =  $activitie_collector->create_path_to_activities('2022-12-08', '2023-01-08');
            $activities = $activitie_collector->get_all_data_from_api($path_activities);
            $final_activities = $activitie_collector->discover_activitie_data($activities);

            echo '<br>';
            echo count($final_activities);
            echo '<pre>'; print_r($final_activities);

            // foreach($final_activities as $activities){
            //     echo '<br>';
                // echo $activities['XA_PI_CREATE_DATE']; //Ok
                // echo $activities['date']; //Ok
                // echo $activities['A_TIME_OF_BOOKING']; //Ok
                // echo $activities['activityId']; //Ok
                // echo $activities['apptNumber']; //Ok
                // echo $activities['XA_PI_EVENT']; //Ok
                // echo $activities['activityType']; //Ok
                // echo $activities['status']; //Ok
                // echo $activities['resourceInternalId']; //Ok
                // echo $activities['resourceId']; //Ok
                // echo $activities['XA_ORIGIN_BUCKET']; //Ok
                // echo $activities['regional']; //Ok
                // echo $activities['contract']; //Ok
                // echo $activities['company']; //Ok
                // echo $activities['stateProvince']; //Ok
                // echo $activities['city']; //Ok
                // echo $activities['XA_EXECUTOR_USER']; //Ok
                // echo $activities['startTime']; //Ok
                // echo $activities['endTime']; //Ok
                // echo $activities['XA_PI_ALARM_TYPE']; //Ok
                // echo $activities['XA_PI_FAIL_TYPE']; //Ok
                // echo $activities['XA_PI_CM']; //Ok
                // echo $activities['XA_PI_END_ID']; //Ok
                // echo $activities['XA_PI_NETWORK_ELEMENT']; //Ok
                // echo $activities['XA_PI_NE_TYPE']; //Ok
                // echo $activities['XA_PI_NETWORK']; //Ok
                // echo $activities['XA_PI_NOTDONE_REASON']; //Ok
                // echo $activities['XA_PI_OP']; //Ok
                // echo $activities['XA_PI_OPENING_NOTE']; //Ok
                // echo $activities['XA_PI_PRIORITY']; //Ok
                // echo $activities['XA_PI_RESPONSABLE']; //Ok
                // echo $activities['XA_PI_SUB_AREA']; //Ok
                // echo $activities['XA_PI_SUSPEND_REASON']; //Ok
                // echo $activities['XA_PI_TRAM_REASON']; //Ok
                // echo $activities['XA_PI_TRAM_SUS']; //Ok
            //}
        ?>
    </body>
</html>