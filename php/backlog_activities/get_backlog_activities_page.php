<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';
    $utils = new Utils();
    $dbo_4field = new mypdo();
?>

<div class="pagetitle">
    <h1>Atividades de Backlog</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Início</a></li>
            <li class="breadcrumb-item active">Atividades de Backlog</li>
        </ol>
    </nav>
</div>

<section class="section backlog_activities">
  <div class="row">
    <div class="col-md-12 col-lg-12">
      <div class="card">
        <div class="card-body">
          <!--<div class="filters">
              <div class="row">
                <div class="col-sm-6 col-md-2 col-lg-2">
                  <h5 class="card-title">Empresa</h5>
                </div>
                <div class="col-sm-6 col-md-2 col-lg-2">
                  <h5 class="card-title">Tipo Contrato</h5>
                </div>
                <div class="col-sm-6 col-md-2 col-lg-2">
                  <h5 class="card-title">Tempo de abertura</h5>
                </div>
                <div class="col-sm-6 col-md-2 col-lg-2">
                  <h5 class="card-title">UF</h5>
                </div>
                <div class="col-sm-6 col-md-2 col-lg-2">
                  <h5 class="card-title">Prioridade</h5>
                </div>
              </div>
          </div>-->

          <div class="col-md-12 backlog_report">
            <div class="row">
              <div class="col-sm-12 col-md-6 col-lg-4">
                <h5 class="card-title">Consolidado Diário</h5>
                <canvas id="consolidated-daily" style="height: 400px;"></canvas>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-4">
                <h5 class="card-title">Prioridade Diária</h5>
                <canvas id="priority-daily" style="height: 400px;"></canvas>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <h5 class="card-title">Contrato x Tempo de Abertura</h5>
                <div class="table-content">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Empresa</th>
                        <th scope="col">Contrato</th>
                        <th scope="col">Até 4h</th>
                        <th scope="col">4h a 6h</th>
                        <th scope="col">6h a 8h</th>
                        <th scope="col">8h a 24h</th>
                        <th scope="col">24h a 48h</th>
                        <th scope="col">48h a 72h</th>
                        <th scope="col">72h a 96h</th>
                        <th scope="col">Maior 96h</th>
                        <th scope="col">Backlog</th>
                        <th scope="col">Total	Fila</th>
                        <th scope="col">Total	> 24h</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>28</p></td>
                        <td><p>0,48</p></td>
                        <td><p>15</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>28</p></td>
                        <td><p>0,48</p></td>
                        <td><p>15</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>28</p></td>
                        <td><p>0,48</p></td>
                        <td><p>15</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>28</p></td>
                        <td><p>0,48</p></td>
                        <td><p>15</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>28</p></td>
                        <td><p>0,48</p></td>
                        <td><p>15</p></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <h5 class="card-title">Contrato x Status</h5>
                <div class="table-content">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Empresa</th>
                        <th scope="col">Contrato</th>
                        <th scope="col">Téc. Concluído</th>
                        <th scope="col">Téc. Pendente</th>
                        <th scope="col">Téc. Suspenso</th>
                        <th scope="col">Cen. Concluído</th>
                        <th scope="col">Cen. Pendente</th>
                        <th scope="col">Cen. Suspenso</th>
                        <th scope="col">Total Concluído</th>
                        <th scope="col">Total Pendente</th>
                        <th scope="col">Total Suspenso</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>9</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>9</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>9</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>9</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>9</p></td>
                      </tr>
                      <tr>
                        <td><p class="large-col">Teleperformance</p></td>
                        <td><p>TCO_FMO</p></td>
                        <td><p>1</p></td>
                        <td><p>2</p></td>
                        <td><p>3</p></td>
                        <td><p>4</p></td>
                        <td><p>5</p></td>
                        <td><p>6</p></td>
                        <td><p>7</p></td>
                        <td><p>8</p></td>
                        <td><p>9</p></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <h5 class="card-title">Lista de Atividades em Backlog</h5>
                <div class="table-content">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Backlog </th>
                        <th scope="col">Aging</th>
                        <th scope="col">Período</th>
                        <th scope="col">Número de Ordem</th>
                        <th scope="col">Tipo Contrato</th>
                        <th scope="col">Estado</th>
                        <th scope="col">UF</th>
                        <th scope="col">Criação do NTT</th>
                        <th scope="col">END_ID</th>
                        <th scope="col">Nota de Abertura</th>
                        <th scope="col">Tipo da Falha</th>
                        <th scope="col">Tipo de NE</th>
                        <th scope="col">Título do Alarme</th>
                        <th scope="col">Prioridade</th>
                        <th scope="col">Rede</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><p>ID2</p></td>
                        <td><p>30/nov</p></td>
                        <td><p class="large-col">Entre 24hs - 48hs</p></td>
                        <td><p>Histórico Backlog</p></td>
                        <td><p class="large-col">TSK221100126038	</p></td>
                        <td><p>TNO_FMMT	</p></td>
                        <td><p class="large-col">Não iniciado</p></td>
                        <td><p>PR</p></td>
                        <td><p class="large-col">08/12/2022 09:35</p></td>
                        <td><p>SPSPO_0729</p></td>
                        <td><p class="large-col text-justify">Exemplo de de nota de abertura do ticket para teste</p></td>
                        <td><p>Sinalização</p></td>
                        <td><p>NODE-B</p></td>
                        <td><p class="large-col">UMTS NODEB DOWN</p></td>
                        <td><p>Média</p></td>
                        <td><p>COMMON</p></td>
                      </tr>
                      <tr>
                        <td><p>ID2</p></td>
                        <td><p>30/nov</p></td>
                        <td><p class="large-col">Entre 24hs - 48hs</p></td>
                        <td><p>Histórico Backlog</p></td>
                        <td><p class="large-col">TSK221100126038	</p></td>
                        <td><p>TNO_FMMT	</p></td>
                        <td><p class="large-col">Não iniciado</p></td>
                        <td><p>PR</p></td>
                        <td><p class="large-col">08/12/2022 09:35</p></td>
                        <td><p>SPSPO_0729</p></td>
                        <td><p class="large-col text-justify">Exemplo de de nota de abertura do ticket para teste</p></td>
                        <td><p>Sinalização</p></td>
                        <td><p>NODE-B</p></td>
                        <td><p class="large-col">UMTS NODEB DOWN</p></td>
                        <td><p>Média</p></td>
                        <td><p>COMMON</p></td>
                      </tr>
                      <tr>
                        <td><p>ID2</p></td>
                        <td><p>30/nov</p></td>
                        <td><p class="large-col">Entre 24hs - 48hs</p></td>
                        <td><p>Histórico Backlog</p></td>
                        <td><p class="large-col">TSK221100126038	</p></td>
                        <td><p>TNO_FMMT	</p></td>
                        <td><p class="large-col">Não iniciado</p></td>
                        <td><p>PR</p></td>
                        <td><p class="large-col">08/12/2022 09:35</p></td>
                        <td><p>SPSPO_0729</p></td>
                        <td><p class="large-col text-justify">Exemplo de de nota de abertura do ticket para teste</p></td>
                        <td><p>Sinalização</p></td>
                        <td><p>NODE-B</p></td>
                        <td><p class="large-col">UMTS NODEB DOWN</p></td>
                        <td><p>Média</p></td>
                        <td><p>COMMON</p></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-6">
                <h5 class="card-title">Gráfico de barras -  prioridade</h5>
              </div>

              <!--
                -- Tabela por contrato x tempo de abertura (contrato | empresa | Tempo de abertura)
                -- Tabela por contrato x status (contrato | status técnico | status centralizado | status total)
                -- Tabela de atividades baseada na exportação do Franscesco (com botão de exportação)
                -- Gráfico de barras - consolidado diário (tempo de abertura)
                -- Gráfico de barras -  prioridade
              -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>