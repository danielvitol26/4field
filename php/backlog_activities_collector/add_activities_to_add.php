<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(
        isset($jsonObj['activities_to_add']) &&
        isset($jsonObj['collect_date'])
    ){
        $activities_to_add = $jsonObj['activities_to_add'];
        $collect_date = $jsonObj['collect_date'];

        if(count($activities_to_add) > 500){
            $sliced_array = array_chunk($activities_to_add, 500);

            $count_slice_execution = 0;
            foreach($sliced_array as $slice){
                $add_array = [];
                foreach($slice as $activity){
                    $add_array[] = '("' . $activity['A_TIME_OF_BOOKING'] . '", "' . $activity['XA_EXECUTOR_USER'] . '", "' . $activity['XA_ORIGIN_BUCKET'] . '", "' . $activity['XA_PI_ALARM_TYPE'] . '", "' . $activity['XA_PI_CM'] . '", "' . $activity['XA_PI_CREATE_DATE'] . '", "' . $activity['XA_PI_END_ID'] . '", "' . $activity['XA_PI_EVENT'] . '", "' . $activity['XA_PI_FAIL_TYPE'] . '", "' . $activity['XA_PI_NETWORK'] . '", "' . $activity['XA_PI_NETWORK_ELEMENT'] . '", "' . $activity['XA_PI_NE_TYPE'] . '", "' . $activity['XA_PI_NOTDONE_REASON'] . '", "' . $activity['XA_PI_OP'] . '", "' . $activity['XA_PI_OPENING_NOTE'] . '", "' . $activity['XA_PI_PRIORITY'] . '", "' . $activity['XA_PI_RESPONSABLE'] . '", "' . $activity['XA_PI_SUB_AREA'] . '", "' . $activity['XA_PI_SUSPEND_REASON'] . '", "' . $activity['XA_PI_TRAM_REASON'] . '", "' . $activity['XA_PI_TRAM_SUS'] . '", "' . $activity['activityId'] . '", "' . $activity['activityType'] . '", "' . $activity['apptNumber'] . '", "' . $activity['city'] . '", "' . $activity['company'] . '", "' . $activity['contract'] . '", "' . $activity['date'] . '", "' . $activity['endTime'] . '", "' . $activity['regional'] . '", "' . $activity['resourceId'] . '", "' . $activity['resourceInternalId'] . '", "' . $activity['startTime'] . '", "' . $activity['stateProvince'] . '", "' . $activity['status'] . '", "' . $collect_date . '")';
                }

                $sql_to_insert = 'insert into backlog_activities (A_TIME_OF_BOOKING, XA_EXECUTOR_USER, XA_ORIGIN_BUCKET, XA_PI_ALARM_TYPE, XA_PI_CM, XA_PI_CREATE_DATE, XA_PI_END_ID, XA_PI_EVENT, XA_PI_FAIL_TYPE, XA_PI_NETWORK, XA_PI_NETWORK_ELEMENT, XA_PI_NE_TYPE, XA_PI_NOTDONE_REASON, XA_PI_OP, XA_PI_OPENING_NOTE, XA_PI_PRIORITY, XA_PI_RESPONSABLE, XA_PI_SUB_AREA, XA_PI_SUSPEND_REASON, XA_PI_TRAM_REASON, XA_PI_TRAM_SUS, activityId, activityType, apptNumber, city, company, contract, date, endTime, regional, resourceId, resourceInternalId, startTime, stateProvince, status, collect_date) values '. implode(', ', $add_array) .'';

                $dbo_4field = new mypdo();
                $add_status = $dbo_4field->exe_sql($sql_to_insert);

                if($add_status == true){
                    $count_slice_execution++;
                }
            }

            if(count($sliced_array) == $count_slice_execution){
                $add_status = true;
            }else{
                $add_status = false;
            }
        }else{
            $sql_to_insert = '';
            if(count($activities_to_add) > 0){
                $add_array = [];
                foreach($activities_to_add as $activity){
                    $add_array[] = '("'
                        . $activity['A_TIME_OF_BOOKING'] . '", "'
                        . $activity['XA_EXECUTOR_USER'] . '", "'
                        . $activity['XA_ORIGIN_BUCKET'] . '", "'
                        . $activity['XA_PI_ALARM_TYPE'] . '", "'
                        . $activity['XA_PI_CM'] . '", "'
                        . $activity['XA_PI_CREATE_DATE'] . '", "'
                        . $activity['XA_PI_END_ID'] . '", "'
                        . $activity['XA_PI_EVENT'] . '", "'
                        . $activity['XA_PI_FAIL_TYPE'] . '", "'
                        . $activity['XA_PI_NETWORK'] . '", "'
                        . $activity['XA_PI_NETWORK_ELEMENT'] . '", "'
                        . $activity['XA_PI_NE_TYPE'] . '", "'
                        . $activity['XA_PI_NOTDONE_REASON'] . '", "'
                        . $activity['XA_PI_OP'] . '", "'
                        . $activity['XA_PI_OPENING_NOTE'] . '", "'
                        . $activity['XA_PI_PRIORITY'] . '", "'
                        . $activity['XA_PI_RESPONSABLE'] . '", "'
                        . $activity['XA_PI_SUB_AREA'] . '", "'
                        . $activity['XA_PI_SUSPEND_REASON'] . '", "'
                        . $activity['XA_PI_TRAM_REASON'] . '", "'
                        . $activity['XA_PI_TRAM_SUS'] . '", "'
                        . $activity['activityId'] . '", "'
                        . $activity['activityType'] . '", "'
                        . $activity['apptNumber'] . '", "'
                        . $activity['city'] . '", "'
                        . $activity['company'] . '", "'
                        . $activity['contract'] . '", "'
                        . $activity['date'] . '", "'
                        . $activity['endTime'] . '", "'
                        . $activity['regional'] . '", "'
                        . $activity['resourceId'] . '", "'
                        . $activity['resourceInternalId'] . '", "'
                        . $activity['startTime'] . '", "'
                        . $activity['stateProvince'] . '", "'
                        . $activity['status'] . '", "'
                        . $collect_date . '")';
                }

                $sql_to_insert = 'insert into backlog_activities (A_TIME_OF_BOOKING, XA_EXECUTOR_USER, XA_ORIGIN_BUCKET, XA_PI_ALARM_TYPE, XA_PI_CM, XA_PI_CREATE_DATE, XA_PI_END_ID, XA_PI_EVENT, XA_PI_FAIL_TYPE, XA_PI_NETWORK, XA_PI_NETWORK_ELEMENT, XA_PI_NE_TYPE, XA_PI_NOTDONE_REASON, XA_PI_OP, XA_PI_OPENING_NOTE, XA_PI_PRIORITY, XA_PI_RESPONSABLE, XA_PI_SUB_AREA, XA_PI_SUSPEND_REASON, XA_PI_TRAM_REASON, XA_PI_TRAM_SUS, activityId, activityType, apptNumber, city, company, contract, date, endTime, regional, resourceId, resourceInternalId, startTime, stateProvince, status, collect_date) values '. implode(', ', $add_array) .'';
            }
            $dbo_4field = new mypdo();
            $add_status = $dbo_4field->exe_sql($sql_to_insert);
        }

        echo json_encode(['add_status' => $add_status]);
    }else{
        echo json_encode(['error'=>'access_denied']);
    }
