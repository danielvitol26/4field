<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(
        isset($jsonObj['check_activities_date_and_add_to_historic']) &&
        isset($jsonObj['date'])
    ){
        $date = $jsonObj['date'];
        $dbo_4field = new mypdo();

        $sql_to_check_if_exists_old_registers = 'select count(*) from backlog_activities where DATE(collect_date) < "'. $date .'"';
        $count_old_registers = $dbo_4field->return_array($sql_to_check_if_exists_old_registers);
        $count_old_registers = intval($count_old_registers[0][0]);

        if($count_old_registers > 0){
            $sql_to_get_from_backlog_and_add_to_historic = 'insert into backlog_activities_historic select * from backlog_activities where DATE(collect_date) < "'. $date .'"';
            $transfer_to_backlog_historic_status = $dbo_4field->exe_sql($sql_to_get_from_backlog_and_add_to_historic);

            if($transfer_to_backlog_historic_status == true){
                $sql_to_delete_from_backlog = 'delete from backlog_activities where DATE(collect_date) < "'. $date .'"';
                $delete_from_backlog_status = $dbo_4field->exe_sql($sql_to_delete_from_backlog);
                if($delete_from_backlog_status == true){
                    echo json_encode(['checked_activities' => true]);
                }else{
                    echo json_encode(['checked_activities' => false]);
                }
            }else{
                echo json_encode(['checked_activities' => false]);
            }
        }else{
            echo json_encode(['checked_activities' => 'without_registers_to_histotic']);
        }

    }else{
        echo json_encode(['error'=>'access_denied']);
    }
