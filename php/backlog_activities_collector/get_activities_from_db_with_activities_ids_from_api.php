<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(isset($jsonObj['activities_ids'])){
        $activities_ids = implode(', ', $jsonObj['activities_ids']);

        $dbo_4field = new mypdo();
        $sql_to_update = 'select * from backlog_activities where activityId in ('. $activities_ids .')';
        $activities_to_update = $dbo_4field->return_array($sql_to_update);

        echo json_encode(['activities_to_update' => $activities_to_update]);
    }else{
        echo json_encode(['error'=>'access_denied']);
    }
