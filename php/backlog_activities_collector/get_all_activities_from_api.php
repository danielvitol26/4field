<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';
    include_once '../../data_collector/data_collector_activitie.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(
        isset($jsonObj['get_all_activities_from_api']) &&
        isset($jsonObj['start_date']) &&
        isset($jsonObj['end_date']) &&
        isset($jsonObj['non_scheduled'])
    ){
        $start_date = $jsonObj['start_date'];
        $end_date = $jsonObj['end_date'];
        $non_scheduled = $jsonObj['non_scheduled'];

        $activitie_collector = new ActivitieDataCollector($non_scheduled=$non_scheduled);
        $path_activities =  $activitie_collector->create_path_to_activities($start_date, $end_date);
        $activities = $activitie_collector->get_all_data_from_api($path_activities);
        $final_activities = $activitie_collector->discover_activitie_data($activities);

        echo json_encode([
            'all_activities_from_api'=>$final_activities,
        ]);
    }else{
        echo json_encode(['all_activities_from_api'=>'access_denied']);
    }