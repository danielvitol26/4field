<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(
        isset($jsonObj['activities_to_update']) &&
        isset($jsonObj['collect_date'])
    ){
        $activities_to_update = $jsonObj['activities_to_update'];
        $collect_date = $jsonObj['collect_date'];
        $dbo_4field = new mypdo();

        $count_updates = 0;
        foreach($activities_to_update as $activity){
            $sql_update = 'update backlog_activities set '
            .'XA_PI_CREATE_DATE = "'. $activity['XA_PI_CREATE_DATE'] .'", '
            .'date = "'. $activity['date'] .'", '
            .'A_TIME_OF_BOOKING = "'. $activity['A_TIME_OF_BOOKING'] .'", '
            .'activityId = "'. $activity['activityId'] .'", '
            .'apptNumber = "'. $activity['apptNumber'] .'", '
            .'XA_PI_EVENT = "'. $activity['XA_PI_EVENT'] .'", '
            .'activityType = "'. $activity['activityType'] .'", '
            .'status = "'. $activity['status'] .'", '
            .'resourceInternalId = "'. $activity['resourceInternalId'] .'", '
            .'resourceId = "'. $activity['resourceId'] .'", '
            .'XA_ORIGIN_BUCKET = "'. $activity['XA_ORIGIN_BUCKET'] .'", '
            .'regional = "'. $activity['regional'] .'", '
            .'contract = "'. $activity['contract'] .'", '
            .'company = "'. $activity['company'] .'", '
            .'stateProvince = "'. $activity['stateProvince'] .'", '
            .'city = "'. $activity['city'] .'", '
            .'XA_EXECUTOR_USER = "'. $activity['XA_EXECUTOR_USER'] .'", '
            .'startTime = "'. $activity['startTime'] .'", '
            .'endTime = "'. $activity['endTime'] .'", '
            .'XA_PI_ALARM_TYPE = "'. $activity['XA_PI_ALARM_TYPE'] .'", '
            .'XA_PI_FAIL_TYPE = "'. $activity['XA_PI_FAIL_TYPE'] .'", '
            .'XA_PI_CM = "'. $activity['XA_PI_CM'] .'", '
            .'XA_PI_END_ID = "'. $activity['XA_PI_END_ID'] .'", '
            .'XA_PI_NETWORK_ELEMENT = "'. $activity['XA_PI_NETWORK_ELEMENT'] .'", '
            .'XA_PI_NE_TYPE = "'. $activity['XA_PI_NE_TYPE'] .'", '
            .'XA_PI_NETWORK = "'. $activity['XA_PI_NETWORK'] .'", '
            .'XA_PI_NOTDONE_REASON = "'. $activity['XA_PI_NOTDONE_REASON'] .'", '
            .'XA_PI_OP = "'. $activity['XA_PI_OP'] .'", '
            .'XA_PI_OPENING_NOTE = "'. $activity['XA_PI_OPENING_NOTE'] .'", '
            .'XA_PI_PRIORITY = "'. $activity['XA_PI_PRIORITY'] .'", '
            .'XA_PI_RESPONSABLE = "'. $activity['XA_PI_RESPONSABLE'] .'", '
            .'XA_PI_SUB_AREA = "'. $activity['XA_PI_SUB_AREA'] .'", '
            .'XA_PI_SUSPEND_REASON = "'. $activity['XA_PI_SUSPEND_REASON'] .'", '
            .'XA_PI_TRAM_REASON = "'. $activity['XA_PI_TRAM_REASON'] .'", '
            .'XA_PI_TRAM_SUS = "'. $activity['XA_PI_TRAM_SUS'] .'", '
            .'collect_date = "'. $collect_date .'" '
            .'WHERE id = '. $activity['id'] .'';

            $update_status = $dbo_4field->exe_sql($sql_update);
            if($update_status == true){
                $count_updates++;
            }
        }
        if($count_updates == count($activities_to_update)){
            echo json_encode(['update_status' => true]);
        }else{
            echo json_encode(['update_status' => false]);
        }
    }else{
        echo json_encode(['error'=>'access_denied']);
    }
