<?php
    include_once '../classes/Utils.php';
    $utils = new Utils();
    $user_session = $utils->get_user_session_data();
?>

<ul class="sidebar-nav" id="sidebar-nav">
    <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
        <i class="bi bi-bar-chart"></i><span>Gestão Online</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
        <li class="get_backlog_activities_page">
            <a>
                <i class="bi bi-circle"></i><span>Atividades de Backlog</span>
            </a>
        </li>
        <li>
            <a>
                <i class="bi bi-circle"></i><span>Controle de Produtividade</span>
            </a>
        </li>
    </ul>
    </li>

    <li class="nav-item">
    <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
        <i class="bi bi-grid"></i><span>Dispatching & Tracking</span><i class="bi bi-chevron-down ms-auto"></i>
    </a>
    <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
        <li class="get_mask_generator_page">
            <a>
                <i class="bi bi-circle"></i><span>Gerador de máscaras</span>
            </a>
        </li>
        <li class="get_downtime_calculator_page">
            <a>
                <i class="bi bi-circle"></i><span>Calculadora de downtime</span>
            </a>
        </li>
    </ul>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#charts-nav" data-bs-toggle="collapse" href="#">
            <i class="bi bi-card-list"></i><span>Subcontratações</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="charts-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
            <li class="get_subcontractor_add_page">
                <a>
                    <i class="bi bi-circle"></i><span>Cadastrar Nova</span>
                </a>
            </li>
            <li class="get_all_subcontractor_page">
                <a>
                    <i class="bi bi-circle"></i><span>Consultar Envios</span>
                </a>
            </li>
            <!--<li class="get_subcontractor_report_page">
                <a>
                    <i class="bi bi-circle"></i><span>Relatório de Envios</span>
                </a>
            </li>-->
        </ul>
    </li>

    <?php
    if($user_session['user_type'] == 'adm'){
    ?>
        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-person"></i><span>Gestão de Acesso</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
        <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
            <li class="get_user_manage_page">
                <a>
                    <i class="bi bi-circle"></i><span>Controle de Usuários</span>
                </a>
            </li>
            <li class="recolect_all_users_from_api">
                <a>
                    <i class="bi bi-circle"></i><span>Coletor de Usuários</span>
                </a>
            </li>
        </ul>
        </li><!-- End Tables Nav -->
    <?php
    }
    ?>
</ul>
