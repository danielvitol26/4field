<?php
if(isset($_POST['DATA_FALHA'])){

    $data_falha = $_POST['DATA_FALHA'];
    date_default_timezone_set('America/Fortaleza');
    $data_hoje = date('Y-m-d H:i', time());

    $data_falha = new DateTime($data_falha, new DateTimeZone('America/Fortaleza'));
    $data_hoje = new DateTime($data_hoje, new DateTimeZone('America/Fortaleza'));

    $intervalo = $data_falha->diff($data_hoje);
    if($intervalo->m == 0){
        $DOWNTIME = $intervalo->d." Dia(s), ".$intervalo->h." Hora(s) e ".$intervalo->i." Min.";
    }else{
        $DOWNTIME = $intervalo->m." Mês(es), ".$intervalo->d." Dia(s), ".$intervalo->h." Hora(s) e ".$intervalo->i." Min.";
    }

    echo $DOWNTIME;
}

?>