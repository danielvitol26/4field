<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';
    $utils = new Utils();
    $dbo_4field = new mypdo();
?>

<div class="pagetitle">
    <h1>Gerador de máscaras</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Início</a></li>
            <li class="breadcrumb-item active">Gerador de máscaras</li>
        </ol>
    </nav>
</div>

<section class="section mask_generator">
      <div class="row">
        <div class="col-md-6 col-lg-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Selecione a máscara desejada e preencha o formulário para seguir:</h5>

              <?php
                $fields = [
                  'ticket' => 'Ticket',
                  'escalonamento' => 'Escalonamento',
                  'atualizacao' => 'Atualização',
                  'data_falha' => 'Data falha',
                  'anel' => 'Anel',
                  'equipamento' => 'Equipamento',
                  'grupo_acionado' => 'Grupo Acionado',
                  'motivo_pendenciamento' => 'Motivo do pendenciamento',
                  'chamado_travado' => 'Chamado travado?',
                  'horario_retorno' => 'Horário de retorno',
                  'motivo_alteracao_status' => 'Motivo da alteração de status',
                  'atualizacao_chamado' => 'Atualização do chamado',
                  'segmento' => 'Segmento',
                  'data_falha' => 'Data da falha',
                  'down_time' => 'Downtime',
                  'telefone' => 'Telefone',
                  'motivo_tramite' => 'Motivo do tramite',
                  'clientes_impactados' => 'Clientes Impactados',
                  'cliente' => 'Cliente',
                  'concentrador' => 'Concentrador',
                  'numero_dl' => 'Numero DL',
                  'anf' => 'ANF',
                  'elemento_selecao' => 'Elemento seleção',
                  'rompimento' => 'Rompimento',
                  'btn_gerar_mascara' => 'Gerar máscara'
                ];

                /* Campos das máscaras
                Ticket
                Escalonamento
                Atualização
                Data falha
                Anel
                Equipamento
                Grupo Acionado
                Motivo do pendenciamento
                Chamado travado?
                Horário de retorno
                Motivo da alteração de status
                Atualização do chamado
                Segmento
                Data da falha
                Downtime
                Telefone
                Motivo do tramite
                */

                $masks = [
                          'infra_live_tim' => [
                            'name' => 'Infra live tim',
                            'fields' => [
                              'ticket',
                              'equipamento',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'live_tim' => [
                            'name' => 'Live tim',
                            'fields' => [
                              'ticket',
                              'msan_equipamento',
                              'anel',
                              'clientes_impactados',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'live_tim_alta_hierarquia' => [
                            'name' => 'Live tim - Alta hierarquia',
                            'fields' => [
                              'ticket',
                              'equipamento',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'cliente_corporativo' => [
                            'name' => 'Cliente corporativo',
                            'fields' => [
                              'ticket',
                              'cliente',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'rede_acesso_dl' => [
                            'name' => 'Rede acesso - DL',
                            'fields' => [
                              'ticket',
                              'concentrador',
                              'numero_dl',
                              'anf',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'rede_acesso_massivas' => [
                            'name' => 'Rede acesso - Massivas',
                            'fields' => [
                              'ticket',
                              'equipamento',
                              'anf',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'rede_transporte' => [
                            'name' => 'Rede transporte',
                            'fields' => [
                              'ticket',
                              'equipamento',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'backbone_longa_distancia' => [
                            'name' => 'Backbone longa distância',
                            'fields' => [
                              'ticket',
                              'elemento_selecao',
                              'rompimento',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'agro_fca_aeroporto' => [
                            'name' => 'Agro/Fca/Aeroporto',
                            'fields' => [
                              'ticket',
                              'segmento',
                              'equipamento',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'gestao_fila_field' => [
                            'name' => 'Gestão de fila field',
                            'fields' => [
                              'ticket',
                              'motivo_tramite',
                              'data_falha',
                              'down_time',
                              'grupo_acionado',
                              'escalonamento',
                              'telefone',
                              'atualizacao',
                            ],
                          ],
                          'triagem' => [
                            'name' => 'Triagem',
                            'fields' => [
                              'ticket',
                              'anel',
                              'equipamento',
                              'grupo_acionado',
                            ],
                          ],
                          'alteracao_status' => [
                            'name' => 'Alteração de status',
                            'fields' => [
                              'ticket',
                              'motivo_alteracao_status',
                              'data_falha',
                              'down_time',
                              'chamado_travado',
                            ],
                          ],
                          'atualizacao_chamado' => [
                            'name' => 'Atualização de chamado',
                            'fields' => [
                              'ticket',
                              'atualizacao_chamado',
                              'data_falha',
                              'down_time',
                            ],
                          ],
                          'escalonamento_email' => [
                            'name' => 'Escalonamento via e-mail',
                            'fields' => [
                              'ticket',
                              'escalonamento',
                              'data_falha',
                              'down_time',
                              'atualizacao',
                            ],
                          ],
                          'pendenciamento_chamado' => [
                            'name' => 'Pendenciamento de chamado',
                            'fields' => [
                              'ticket',
                              'motivo_pendenciamento',
                              'data_falha',
                              'down_time',
                              'chamado_travado',
                              'horario_retorno',
                            ],
                          ],
                        ];

                /* Máscaras
                Escalonamento via E-mail
                Triagem
                Pendenciamento de chamado
                Alteração de status
                Atualização de chamado
                Agro/Fca/Aeroporto
                Gestão de Fila Field
                */

              ?>
              <form class="form_mask">
                <div class="first_step">
                  <div class="row mb-3">
                    <label class="col-sm-2 col-form-label">Máscara</label>
                    <div class="col-sm-10">
                      <select class="form-select" id='select_mask' name='select_mask'>
                        <option selected>Selecione a máscara desejada</option>
                        <?php
                          foreach($masks as $key => $value){
                            echo '<option value="'. $key .'" data-title="'. $value['name'] .'" data-fields="'. implode(',',$value['fields']) .'">'. $value['name'] .'</option>';
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="second_step">
                  <div class="row mb-3 ticket none">
                    <label for="ticket" class="col-sm-2 col-form-label">Ticket</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='ticket' name='ticket' data-title='Ticket'>
                    </div>
                  </div>
                  <div class="row mb-3 escalonamento none">
                    <label for="escalonamento" class="col-sm-2 col-form-label">Escalonamento</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='escalonamento' name='escalonamento'  data-title='Escalonamento'>
                    </div>
                  </div>
                  <div class="row mb-3 atualizacao none">
                    <label for="atualizacao" class="col-sm-2 col-form-label">Atualização</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='atualizacao' name='atualizacao'>
                    </div>
                  </div>
                  <div class="row mb-3 data_falha none">
                    <label for="data_falha" class="col-sm-2 col-form-label">Data falha</label>
                    <div class="col-sm-10">
                      <input type="date" class="form-control" id='data_falha' name='data_falha'>
                    </div>
                  </div>
                  <div class="row mb-3 down_time none">
                    <label for="down_time" class="col-sm-2 col-form-label">Downtime</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='down_time' name='down_time'>
                    </div>
                  </div>
                  <div class="row mb-3 anel none">
                    <label for="anel" class="col-sm-2 col-form-label">Anel</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='anel' name='anel'>
                    </div>
                  </div>
                  <div class="row mb-3 equipamento none">
                    <label for="equipamento" class="col-sm-2 col-form-label">Equipamento</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='equipamento' name='equipamento'>
                    </div>
                  </div>
                  <div class="row mb-3 msan_equipamento none">
                    <label for="msan_equipamento" class="col-sm-2 col-form-label">MSAN | Equipamento</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='msan_equipamento' name='msan_equipamento'>
                    </div>
                  </div>
                  <div class="row mb-3 motivo_pendenciamento none">
                    <label for="motivo_pendenciamento" class="col-sm-2 col-form-label">Motivo do pendenciamento</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='motivo_pendenciamento' name='motivo_pendenciamento'>
                    </div>
                  </div>
                  <div class="row mb-3 motivo_alteracao_status none">
                    <label for="motivo_alteracao_status" class="col-sm-2 col-form-label">Motivo da alteração de status</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='motivo_alteracao_status' name='motivo_alteracao_status'>
                    </div>
                  </div>
                  <div class="row mb-3 grupo_acionado none">
                    <label for="grupo_acionado" class="col-sm-2 col-form-label">Grupo acionado</label>
                    <div class="col-sm-10">
                      <select class="form-select" id='grupo_acionado' name='grupo_acionado'>
                        <option value="" selected>Selecione o grupo acionado</option>
                        <option value="1">Teste</option>
                        <?php

                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="row mb-3 atualizacao_chamado none">
                    <label for="atualizacao_chamado" class="col-sm-2 col-form-label">Atualização de chamado</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='atualizacao_chamado' name='atualizacao_chamado'>
                    </div>
                  </div>
                  <div class="row mb-3 segmento none">
                    <label for="segmento" class="col-sm-2 col-form-label">Segmento</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='segmento' name='segmento'>
                    </div>
                  </div>
                  <div class="row mb-3 telefone none">
                    <label for="telefone" class="col-sm-2 col-form-label">Telefone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='telefone' name='telefone'>
                    </div>
                  </div>
                  <div class="row mb-3 clientes_impactados none">
                    <label for="clientes_impactados" class="col-sm-2 col-form-label">Clientes Impactados</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='clientes_impactados' name='clientes_impactados'>
                    </div>
                  </div>
                  <div class="row mb-3 cliente none">
                    <label for="cliente" class="col-sm-2 col-form-label">Cliente</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='cliente' name='cliente'>
                    </div>
                  </div>
                  <div class="row mb-3 concentrador none">
                    <label for="concentrador" class="col-sm-2 col-form-label">Concentrador</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='concentrador' name='concentrador'>
                    </div>
                  </div>
                  <div class="row mb-3 numero_dl none">
                    <label for="numero_dl" class="col-sm-2 col-form-label">Número DL</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='numero_dl' name='numero_dl'>
                    </div>
                  </div>
                  <div class="row mb-3 anf none">
                    <label for="anf" class="col-sm-2 col-form-label">ANF</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='anf' name='anf'>
                    </div>
                  </div>
                  <div class="row mb-3 elemento_selecao none">
                    <label for="elemento_selecao" class="col-sm-2 col-form-label">Elemento seleção</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='elemento_selecao' name='elemento_selecao'>
                    </div>
                  </div>
                  <div class="row mb-3 rompimento none">
                    <label for="rompimento" class="col-sm-2 col-form-label">Rompimento</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id='rompimento' name='rompimento'>
                    </div>
                  </div>
                </div>
              </form>
              <div class="text-center action-buttons btn_gerar_mascara none">
                <p class="return_message_mask"></p>
                <button class="btn btn-primary" id='btn_gerar_mascara'>Gerar Máscara</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 generated_mask none">
          <div class="card">
            <div class="card-body">
              <div class="header_generated_mask">
                <h5 class="card-title">Máscara gerada:</h5>
                <button class="btn btn-primary" id='copy'>Copiar</button>
              </div>

              <div class="generated_mask_fields">
                <p class="title">*Field D&T (ALTERAÇÃO DE STATUS)*</p>
                <p class="text">*Ticket:* asdasd</p>
                <p class="text">*Motivo da alteração de status:* asdasda</p>
                <p class="text">*Data Falha:* 10/10/2022 22:22</p>
                <p class="text">*Downtime:* 10 Dia(s), 0 Hora(s) e 0 Min.</p>
                <p class="text">*Chamado travado:* Não</p>
                <p class="text">tracking_status_dt</p>

                <p class="text mask_end">dt_3669410</p>
                <p class="text">portal_4field</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>