<?php
	include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';

	if(
		isset($_POST['titulo']) &&
		isset($_POST['subtitulo']) &&
		isset($_POST['tipo']) &&
		isset($_POST['usuario']) &&
		isset($_POST['url_destino'])
	) {
        $titulo = $_POST['titulo'];
        $subtitulo = $_POST['subtitulo'];
        $tipo = $_POST['tipo'];
        $usuario = $_POST['usuario'];
        $url_destino = $_POST['url_destino'];

        date_default_timezone_set('America/Fortaleza');
        $data_criacao = date("Y-m-d H:i:s");

        $dbo_4field = new mypdo();

        $insert_array = [
            'titulo' => $titulo,
            'subtitulo' => $subtitulo,
            'tipo' => $tipo,
            'usuario' => $usuario,
            'data_criacao' => $data_criacao,
            //'url_destino' => $url_destino,
        ];

        $add_notification = $dbo_4field->insert('notificacao', $insert_array);

		echo json_encode(['add_notification' => $add_notification]);
    }else{
        echo json_encode(['add_notification' => 'access_denied']);
    }

?>