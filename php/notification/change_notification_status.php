<?php
	include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';

	if(isset($_POST['usuario'])){
        $user_id = intval($_POST['usuario']);
        $dbo_4field = new mypdo();

        $update_array = [
            'status_notification' => 1,
        ];

        $change_notification_status = $dbo_4field->update('notificacao', 'usuario = '. $user_id . '', $update_array);

        echo json_encode(['change_notification_status' => $change_notification_status]);
    }else{
        echo json_encode(['change_notification_status' => 'access_denied']);
    }