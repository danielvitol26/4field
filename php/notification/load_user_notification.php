<?php
	include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';

	if(isset($_POST['usuario'])){
        $user_id = $_POST['usuario'];
        $dbo_4field = new mypdo();

        $select_array_unviewed = [
            'usuario' => $user_id,
            'status_notification' => 0
        ];

        $user_notifications_unviewed = $dbo_4field->select_multi_rules('notificacao', $select_array_unviewed, 'data_criacao desc');

        $notification_count_icon = '';
        $notification_header_text = '';

        if(count($user_notifications_unviewed) == 0){
            $notification_header_text = 'Você não possui notificações pendentes';
        }else{
            $notification_count_icon = '<span class="badge bg-primary badge-number notification_icon">'. count($user_notifications_unviewed) .'</span>';
            if(count($user_notifications_unviewed) == 1){
                $notification_header_text = 'Você tem '. count($user_notifications_unviewed) .' nova notificação';
            }else{
                $notification_header_text = 'Você tem '. count($user_notifications_unviewed) .' novas notificações';
            }
        }

        $final_notifications = '';

        $notification_start_content = '<li class="nav-item dropdown">
                                            <a class="show_notifications nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                                            <i class="bi bi-bell"></i>
                                            '. $notification_count_icon .'
                                            </a><!-- End Notification Icon -->';

        $notification_end_content = '</li>';

        $notification_ul = '<ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">';
        $notification_ul_end = '</ul>';

        $notification_divisor = '<li><hr class="dropdown-divider"></li>';


        $notification_resume = '<li class="dropdown-header">
                                    '. $notification_header_text .'
                                    <a><span class="badge rounded-pill bg-primary p-2 ms-2"></span></a>
                                </li>';

        $final_notifications = $final_notifications . $notification_start_content . $notification_ul . $notification_resume . $notification_divisor;

        $select_array_all = [
            'usuario' => $user_id,
        ];

        $user_notifications = $dbo_4field->select_multi_rules('notificacao', $select_array_all, 'data_criacao desc');

        if(count($user_notifications) > 0){
            foreach($user_notifications as $notification){
                $id = $notification[0];
                $titulo = $notification[1];
                $subtitulo = $notification[2];
                $tipo = $notification[3];
                $status_notification = $notification[4];
                $usuario = $notification[5];
                $data_criacao = $notification[6];
                $data_visualizacao = $notification[7];
                $url_destino = $notification[8];

                //Calculando difença da hora de abertura e a hora atual
                date_default_timezone_set('America/Fortaleza');
                $create_date = new DateTime($data_criacao);
                $now_date = new DateTime(date("Y-m-d H:i:s"));
                $date_difference = $create_date->diff($now_date);

                $months_difference = intval($date_difference->format('%m'));
                $days_difference = intval($date_difference->format('%d'));
                $hours_difference = intval($date_difference->format('%h'));
                $minutes_difference = intval($date_difference->format('%i'));
                $seconds_difference = intval($date_difference->format('%s'));

                //Criando classe para as notificações ainda não visualizadas
                $unviewed_notification = '';
                if($status_notification == 0){
                    $unviewed_notification = 'unviewed-notification';
                }

                $final_date_difference = '';
                if($months_difference > 0){
                    if($months_difference == 1){
                        $final_date_difference = $months_difference. ' mês atrás';
                    }else{
                        $final_date_difference = $months_difference. ' meses atrás';
                    }
                }else if($days_difference > 0){
                    if($days_difference == 1){
                        $final_date_difference = $days_difference. ' dia atrás';
                    }else{
                        $final_date_difference = $days_difference. ' dias atrás';
                    }
                }else if($hours_difference > 0){
                    if($hours_difference == 1){
                        $final_date_difference = $hours_difference. ' hora atrás';
                    }else{
                        $final_date_difference = $hours_difference. ' horas atrás';
                    }
                }else if($minutes_difference > 0){
                    if($minutes_difference == 1){
                        $final_date_difference = $minutes_difference. ' minuto atrás';
                    }else{
                        $final_date_difference = $minutes_difference. ' minutos atrás';
                    }
                }else if($seconds_difference > 0){
                    if($seconds_difference == 1){
                        $final_date_difference = $seconds_difference. ' segundo atrás';
                    }else{
                        $final_date_difference = $seconds_difference. ' segundos atrás';
                    }
                }

                $current_notification = '';
                if($tipo == 'warning'){
                    $current_notification = '
                        <li class="notification-item '. $unviewed_notification .'">
                            <i class="bi bi-exclamation-circle text-warning"></i>
                            <div>
                            <h4>'. $titulo .'</h4>
                            <p>'. $subtitulo .'</p>
                            <p>'. $final_date_difference .'</p>
                            </div>
                        </li>
                    ';
                }else if($tipo == 'danger'){
                    $current_notification = '
                        <li class="notification-item '. $unviewed_notification .'">
                            <i class="bi bi-x-circle text-danger"></i>
                            <div>
                            <h4>'. $titulo .'</h4>
                            <p>'. $subtitulo .'</p>
                            <p>'. $final_date_difference .'</p>
                            </div>
                        </li>
                    ';
                }else if($tipo == 'success'){
                    $current_notification = '
                        <li class="notification-item '. $unviewed_notification .'">
                            <i class="bi bi-check-circle text-success"></i>
                            <div>
                            <h4>'. $titulo .'</h4>
                            <p>'. $subtitulo .'</p>
                            <p>'. $final_date_difference .'</p>
                            </div>
                        </li>
                    ';
                }else if($tipo == 'primary'){
                    $current_notification = '
                        <li class="notification-item '. $unviewed_notification .'">
                            <i class="bi bi-info-circle text-primary"></i>
                            <div>
                            <h4>'. $titulo .'</h4>
                            <p>'. $subtitulo .'</p>
                            <p>'. $final_date_difference .'</p>
                            </div>
                        </li>
                    ';
                }
                $final_notifications = $final_notifications . $current_notification . $notification_divisor;
            }
        }

        $notification_footer = '
            <li class="dropdown-footer">
                <a></a>
            </li>
        ';

        $final_notifications = $final_notifications . $notification_footer . $notification_ul_end  . $notification_end_content;

        echo $final_notifications;
    }else{
        echo json_encode(['load_notification' => 'access_denied']);
    }