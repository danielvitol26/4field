<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(isset($jsonObj['resources_to_add'])){
        $resources_to_add = $jsonObj['resources_to_add'];

        date_default_timezone_set('America/Fortaleza');
        $current_date = date("Y-m-d");

        if(count($resources_to_add) > 5000){
            $sliced_array = array_chunk($resources_to_add, 5000);

            $count_slice_execution = 0;
            foreach($sliced_array as $slice){
                $add_array = [];
                foreach($slice as $resource){
                    $add_array[] = '("'. $resource['resourceId'] .'", "'. $resource['name'] .'", "'. $resource['status'] .'", "'. $resource['parentResourceId'] .'", "'. $resource['resourceInternalId'] .'", "'. $resource['parentResourceInternalId'] .'", "'. $resource['resourceType'] .'", "'. $resource['XP_COMPANY'] .'",  "'. $current_date .'",  "'. $current_date .'")';
                }
                $sql_to_insert = 'insert into resources (resourceId, name, status, parentResourceId, resourceInternalId, parentResourceInternalId, resourceType, XP_COMPANY, registration_date, update_date) values '. implode(', ', $add_array) .'';

                $dbo_4field = new mypdo();
                $add_status = $dbo_4field->exe_sql($sql_to_insert);

                if($add_status == true){
                    $count_slice_execution++;
                }
            }

            if(count($sliced_array) == $count_slice_execution){
                $add_status = true;
            }else{
                $add_status = false;
            }
        }else{
            $sql_to_insert = '';
            if(count($resources_to_add) == 1){
                $sql_to_insert = 'insert into resources (resourceId, name, status, parentResourceId, resourceInternalId, parentResourceInternalId, resourceType, XP_COMPANY, registration_date, update_date) values ("'. $resources_to_add[0]['resourceId'] .'", "'. $resources_to_add[0]['name'] .'", "'. $resources_to_add[0]['status'] .'", "'. $resources_to_add[0]['parentResourceId'] .'", "'. $resources_to_add[0]['resourceInternalId'] .'", "'. $resources_to_add[0]['parentResourceInternalId'] .'", "'. $resources_to_add[0]['resourceType'] .'", "'. $resources_to_add[0]['XP_COMPANY'] .'", ,  "'. $current_date .'",  "'. $current_date .'")';
            }else if(count($resources_to_add) > 1){
                $add_array = [];
                foreach($resources_to_add as $resource){
                    $add_array[] = '("'. $resource['resourceId'] .'", "'. $resource['name'] .'", "'. $resource['status'] .'", "'. $resource['parentResourceId'] .'", "'. $resource['resourceInternalId'] .'", "'. $resource['parentResourceInternalId'] .'", "'. $resource['resourceType'] .'", "'. $resource['XP_COMPANY'] .'",  "'. $current_date .'",  "'. $current_date .'")';
                }
                $sql_to_insert = 'insert into resources (resourceId, name, status, parentResourceId, resourceInternalId, parentResourceInternalId, resourceType, XP_COMPANY, registration_date, update_date) values '. implode(', ', $add_array) .'';
            }
            $dbo_4field = new mypdo();
            $add_status = $dbo_4field->exe_sql($sql_to_insert);
        }

        echo json_encode(['add_status' => $add_status]);
    }else{
        echo json_encode(['error'=>'access_denied']);
    }
