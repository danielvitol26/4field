<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';
    include_once '../../data_collector/data_collector_resource.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(isset($jsonObj['get_all_resources_from_api'])){
        $resource_collector = new ResourceDataCollector();
        $path_resources =  $resource_collector->create_path_to_all_resources();
        $resources_from_api = $resource_collector->get_all_data_from_api($path_resources);

        echo json_encode(['all_resources_from_api'=>$resources_from_api]);
    }else{
        echo json_encode(['all_resources_from_api'=>'access_denied']);
    }