<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(isset($jsonObj['get_all_resources_from_db'])){
        $dbo_4field = new mypdo();
        $all_current_resources = $dbo_4field->select('resources');

        echo json_encode(['all_resources_from_db'=>$all_current_resources]);
    }else{
        echo json_encode(['all_resources_from_db'=>'access_denied']);
    }