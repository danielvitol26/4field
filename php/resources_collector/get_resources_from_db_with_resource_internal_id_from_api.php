<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(isset($jsonObj['resource_internal_ids'])){
        $resource_internal_ids = implode(', ', $jsonObj['resource_internal_ids']);

        $dbo_4field = new mypdo();
        $sql_to_update = 'select * from resources where resourceInternalId in ('. $resource_internal_ids .')';
        $resources_to_update = $dbo_4field->return_array($sql_to_update);

        echo json_encode(['resources_to_update' => $resources_to_update]);
    }else{
        echo json_encode(['error'=>'access_denied']);
    }
