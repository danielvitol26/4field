<?php
	include_once '../../classes/Pdo.php';
    include_once '../../classes/Utils.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(isset($jsonObj['resources_to_update'])){
        $resources_to_update = $jsonObj['resources_to_update'];
        $dbo_4field = new mypdo();

        date_default_timezone_set('America/Fortaleza');
        $current_date = date("Y-m-d");

        $count_updates = 0;
        foreach($resources_to_update as $resource){
            $sql_update = 'update resources set '
            .'resourceId = "'. $resource['resourceId'] .'", '
            .'name = "'. $resource['name'] .'", '
            .'status = "'. $resource['status'] .'", '
            .'parentResourceId = "'. $resource['parentResourceId'] .'", '
            .'resourceInternalId = "'. $resource['resourceInternalId'] .'", '
            .'parentResourceInternalId = "'. $resource['parentResourceInternalId'] .'", '
            .'resourceType = "'. $resource['resourceType'] .'", '
            .'XP_COMPANY = "'. $resource['XP_COMPANY'] .'", '
            .'update_date = "'. $current_date .'" '
            .'WHERE id = '. $resource['id'] .'';

            $update_status = $dbo_4field->exe_sql($sql_update);
            if($update_status == true){
                $count_updates++;
            }
        }
        if($count_updates == count($resources_to_update)){
            echo json_encode(['update_status' => true]);
        }else{
            echo json_encode(['update_status' => false]);
        }
    }else{
        echo json_encode(['error'=>'access_denied']);
    }
