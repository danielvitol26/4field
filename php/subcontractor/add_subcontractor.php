<?php

	include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';
	if(
		isset($_POST['regional']) &&
		isset($_POST['parceira']) &&
		isset($_POST['mes_subcontratacao']) &&
		isset($_POST['servico']) &&
		isset($_POST['empresa']) &&
		isset($_POST['cnpj'])
	) {
		session_start();

		$utils = new Utils();
		$dbo_4field = new mypdo();

		date_default_timezone_set('America/Fortaleza');
		$hoje = date("Y-m-d H:i:s");
		$year = date("Y");

		$regional = $_POST['regional'];
		$parceira = $_POST['parceira'];
		$subcontratacao = $_POST['mes_subcontratacao'];
		$trat_subcontratacao = explode("-", $subcontratacao);

		if($trat_subcontratacao[1] == "01") {
			$subcontratacao = 'Janeiro'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "02") {
			$subcontratacao = 'Fevereiro'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "03") {
			$subcontratacao = 'Março'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "04") {
			$subcontratacao = 'Abril'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "05") {
			$subcontratacao = 'Maio'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "06") {
			$subcontratacao = 'Junho'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "07") {
			$subcontratacao = 'Julho'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "08") {
			$subcontratacao = 'Agosto'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "09") {
			$subcontratacao = 'Setembro'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "10") {
			$subcontratacao = 'Outubro'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "11") {
			$subcontratacao = 'Novembro'.' de '.$year;
		}
		if($trat_subcontratacao[1] == "12") {
			$subcontratacao = 'Dezembro'.' de '.$year;
		}

		$servico = $_POST['servico'];
		$empresa = $_POST['empresa'];
		$cnpj = $_POST['cnpj'];
		$user_id = $_SESSION['usuarioID'];

		$result_post = "INSERT INTO subcontratacao (regional, parceira, mes_subcontracao, servico, empresa, cnpj, data_criacao, usuario_criacao, status_opm ) 
												  VALUES ('$regional', '$parceira', '$subcontratacao','$servico','$empresa','$cnpj','$hoje', $user_id, 'Pendente')";

		$resultado_post = $dbo_4field->exe_sql($result_post);

		$response = array("success" => $resultado_post);

		echo json_encode($response);
	}else{
		echo json_encode(array("success" => false));
	}

?>