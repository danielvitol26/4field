<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';
    session_start();
    $utils = new Utils();
    $dbo_4field = new mypdo();

    //Aprovados
    $status_subcontratacao = 'Pendente';
    if(isset($_POST['status_subcontratacao'])){
        $status_subcontratacao = $_POST['status_subcontratacao'];
    }

    $select_pending = [
        'status_opm' => 'Pendente',
    ];

    $select_approved = [
        'status_opm' => 'Aprovado',
    ];

    $select_disapproved = [
        'status_opm' => 'Reprovado',
    ];

    $id_company = $_SESSION['usuarioEmpresa'];
    $company_array = $dbo_4field->select('empresa', 'id', $id_company);
    $company_nome = $company_array[0][1];
    $company_sigla = $company_array[0][2];

    $sql_pending_subcontractor = "";
    if($company_sigla != 'TIM'){
        $select_pending['parceira'] = $company_nome;
        $select_approved['parceira'] = $company_nome;
        $select_disapproved['parceira'] = $company_nome;
    }

    $pending_selected = '';
    $approved_selected = '';
    $disapproved_selected = '';

    $current_list = Null;

    if($status_subcontratacao == 'Pendente'){
        $pending_subcontractor = $dbo_4field->select_multi_rules('subcontratacao', $select_pending, 'data_criacao asc');
        $current_list = $pending_subcontractor;
        $pending_selected = 'selected';
    }else if($status_subcontratacao == 'Aprovado'){
        $approved_subcontractor = $dbo_4field->select_multi_rules('subcontratacao', $select_approved, 'data_validacao_opm desc');
        $current_list = $approved_subcontractor;
        $approved_selected = 'selected';
    }else if($status_subcontratacao == 'Reprovado'){
        $disapproved_subcontractor = $dbo_4field->select_multi_rules('subcontratacao', $select_disapproved, 'data_validacao_opm desc');
        $current_list = $disapproved_subcontractor;
        $disapproved_selected = 'selected';
    }
?>

<div class="pagetitle">
    <h1>Fluxo de subcontratações</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Início</a></li>
            <li class="breadcrumb-item active">Fluxo de subcontratações</li>
        </ol>
    </nav>
</div>

<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card subcontractor">
                <div class="card-body">
                    <button class="filter <?php echo $pending_selected; ?>" data-status="Pendente">Pendentes (<span class="pending"></span>)</button>
                    <button class="filter <?php echo $approved_selected; ?>" data-status="Aprovado">Aprovados (<span class="approved"></span>)</button>
                    <button class="filter <?php echo $disapproved_selected; ?>" data-status="Reprovado">Reprovados (<span class="disapproved"></span>)</button>
                </div>

                <div class="card-body">
                    <?php if(count($current_list) == 0){ ?>
                        <p class="without_registers">Sem registros disponíveis nesse filtro.</p>
                    <?php }else{ ?>
                        <div class="accordion accordion-flush" id="accordionList">
                            <?php
                                foreach($current_list as $subcontractor){
                                    $id = $subcontractor[0];
                                    $regional = $subcontractor[1];
                                    $parceira = $subcontractor[2];
                                    $empresa = $subcontractor[5];
                                    $cnpj = $subcontractor[6];
                                    $mes_subcontratação = $subcontractor[3];
                                    $servico = $subcontractor[4];
                                    $status = $subcontractor[9];
                                    $motivo_reprovação = $subcontractor[12];

                                    $user_id = $subcontractor[8];
                                    $user_array = $dbo_4field->select('usuario', 'id', $user_id);

                                    $user_resource_id = $user_array[0][1];
                                    $user_name = $user_array[0][2];
                                    $user_phone = $user_array[0][4];
                                    //echo '<pre>';print_r($user_array);
                            ?>
                                <div class="accordion-item sub-<?php echo $id; ?>">
                                    <h2 class="accordion-header" id="flush-headingsub-<?php echo $id; ?>">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sub-<?php echo $id; ?>" aria-expanded="false" aria-controls="sub-<?php echo $id; ?>">
                                            <p><?php echo $parceira ." - ". $empresa ." (". $status .")";?></p>
                                        </button>
                                    </h2>
                                    <div id="sub-<?php echo $id; ?>" class="accordion-collapse collapse" aria-labelledby="flush-headingsub-<?php echo $id; ?>" data-bs-parent="#accordionList">
                                        <div class="accordion-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <ul>
                                                        <li>Parceira: <?php echo $parceira; ?></li>
                                                        <li>Regional: <?php echo $regional; ?></li>
                                                        <li>Solicitante: <?php echo $user_name .' ('. $user_resource_id .')'; ?></li>
                                                        <li>Telefone: <?php echo $user_phone; ?></li>
                                                        <li>Empresa: <?php echo $empresa; ?></li>
                                                        <li>CNPJ: <?php echo $cnpj; ?></li>
                                                        <li>Mês subcontratação: <?php echo $mes_subcontratação; ?></li>
                                                        <li>Serviço: <?php echo $servico; ?></li>
                                                        <li>Status: <spam id="spam-status-<?php echo $id; ?>"><?php echo $status; ?></spam></li>

                                                        <?php
                                                            $display_li_motivo = "";
                                                            if($status == "Pendente" || $status == "Aprovado"){
                                                                $display_li_motivo = "none";
                                                            }
                                                        ?>

                                                        <li id="li-motivo-<?php echo $id; ?>" class="<?php echo $display_li_motivo; ?>">Motivo reprovação: <spam id="spam-motivo-<?php echo $id; ?>"><?php echo $motivo_reprovação; ?></spam></li>
                                                    </ul>
                                                    <div class="motivo_reprovacao none" id="motivo-<?php echo $id; ?>">
                                                        <label>Motivo da reprovação:</label>
                                                        <textarea class="form-control" id="motivo_reprovacao"></textarea>
                                                    </div>
                                                    <?php
                                                    $user_type = $_SESSION['usuarioPerfil'];
                                                    if($status == "Pendente" && $user_type == 'adm'){
                                                    ?>
                                                        <div class="text-center action-buttons" id="action-buttons-<?php echo $id; ?>">
                                                            <div class="text-center">
                                                                <button id="aprovar-<?php echo $id; ?>" class="btn aprovar" data-id="<?php echo $id; ?>" data-user="<?php echo $user_id; ?>">Aprovar</button>
                                                                <button id="reprovar-<?php echo $id; ?>" class="btn reprovar" data-id="<?php echo $id; ?>" data-user="<?php echo $user_id; ?>">Reprovar</button>
                                                                <button id="cancelar-<?php echo $id; ?>" class="btn cancelar none" data-id="<?php echo $id; ?>">Cancelar</button>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>