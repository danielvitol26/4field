<?php
include_once '../../classes/Pdo.php';

if(isset($_POST['company'])){
  $company_name = trim($_POST['company']);
  $dbo_4field = new mypdo();

  $company_array = $dbo_4field->select('empresa', 'nome', $company_name);
  $id_company = $company_array[0][0];

  $company_regionals = $dbo_4field->select('empresa_regional', 'id_empresa', $id_company);
  $regionals = [];

  foreach($company_regionals as $company_regional){
    $id_regional = $company_regional[2];

    $rules_select = [
      'id' => $id_regional,
      'status'=> 1,
    ];

    $regional = $dbo_4field->select_multi_rules('regional', $rules_select, 'sigla asc');

    if(count($regional) > 0){
      $regional = [
        'id' => $regional[0][0],
        'regional' => $regional[0][2],
      ];
      $regionals[] = $regional;
    }
  }
  echo json_encode($regionals);
}
?>