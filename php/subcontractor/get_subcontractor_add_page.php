<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';

    session_start();
    $utils = new Utils();
    $dbo_4field = new mypdo();
    $dbo_old = new mypdo("localhost", "u487615999_4field", "root", "");
?>

<div class="pagetitle">
    <h1>Cadastrar nova subcontratada</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Início</a></li>
            <li class="breadcrumb-item active">Cadastrar subcontratada</li>
        </ol>
    </nav>
</div>



<section class="section">
    <div class="row">
        <div class="col-lg-12 subcontractor">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Preencha o formulário para cadastrar uma nova subcontratada.</h5>

                    <!-- Vertical Form -->
                    <form id="form_sub" class="row g-3" autocomplete="off">
                        <div class="col-md-4">
                            <label class="form-label">Parceira</label>

                            <select name="parceira" class="form-control parc" id="parceira" >
                                <option value="">--Selecione--</option>
                                <?php
                                    $id_company = $_SESSION['usuarioEmpresa'];
                                    $company_array = $dbo_4field->select('empresa', 'id', $id_company);
                                    $company_nome = $company_array[0][1];
                                    $company_sigla = $company_array[0][2];

                                    if($company_sigla == 'TIM'){
                                        $sql_empresa = "SELECT nome FROM empresa where nome not in('Ezentis','Green4t','Tim') order by nome";
                                        $empresas = $dbo_4field->return_array($sql_empresa);

                                        foreach($empresas as $empresa){
                                            echo '<option value="'. $empresa[0] .'"> '. $empresa[0] .' </option>';
                                        }
                                    }else{
                                        echo '<option value="'. $company_nome .'" > '. $company_nome .' </option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label">Regional</label>
                            <select name="regional" class="form-control" id="regional" >
                                <option value="">--Selecione--</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label">Mês subcontratação</label>
                            <input type="month" name = "mes_subcontratacao" class="form-control" id="mes_subcontratacao" >
                        </div>
                        <div class="col-md-4">
                            <label class="form-label">Serviço</label>
                            <select name = "servico" class="form-control" id="servico" >
                                <option selected="selected" value="">-- Selecione --</option>
                                    <?php
                                        $sql_subcontratacao_servico = "SELECT * FROM subcontratacao_servico order by servico asc";
                                        echo $sql_subcontratacao_servico;
                                        $servicos = $dbo_4field->return_array($sql_subcontratacao_servico);

                                        foreach($servicos as $servico) {
                                           echo '<option value="'. $servico[1] .'"> '. $servico[1] .' </option>';
                                        }
                                    ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label">Empresa</label>
                            <input type="text" name="empresa" class="form-control" id="empresa" >
                        </div>
                        <div class="col-md-4">
                            <label class="form-label">CNPJ</label>
                            <input type="text" name="cnpj" class="form-control cnpj_validate" id="cnpj" >
                        </div>
                    </form><!-- Vertical Form -->
                    <div class="text-center action-buttons">
                        <button class="btn btn-primary submit">Cadastrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>