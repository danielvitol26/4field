<?php
    include_once '../../classes/Pdo.php';
    session_start();

    if(isset($_POST['count'])){
        $dbo_4field = new mypdo();

        $select_pending = [
            'status_opm' => 'Pendente',
        ];

        $select_approved = [
            'status_opm' => 'Aprovado',
        ];

        $select_disapproved = [
            'status_opm' => 'Reprovado',
        ];

        $id_company = $_SESSION['usuarioEmpresa'];
        $company_array = $dbo_4field->select('empresa', 'id', $id_company);
        $company_nome = $company_array[0][1];
        $company_sigla = $company_array[0][2];

        $sql_pending_subcontractor = "";
        if($company_sigla != 'TIM'){
            $select_pending['parceira'] = $company_nome;
            $select_approved['parceira'] = $company_nome;
            $select_disapproved['parceira'] = $company_nome;
        }

        $pending_subcontractor = $dbo_4field->select_multi_rules('subcontratacao', $select_pending);
        $approved_subcontractor = $dbo_4field->select_multi_rules('subcontratacao', $select_approved);
        $disapproved_subcontractor = $dbo_4field->select_multi_rules('subcontratacao', $select_disapproved);

        $count_filters = [
            'count_pending' => count($pending_subcontractor),
            'count_approved' => count($approved_subcontractor),
            'count_disapproved' => count($disapproved_subcontractor),
        ];

        echo json_encode($count_filters);
    }
?>