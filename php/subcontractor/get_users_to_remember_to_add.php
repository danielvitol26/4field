<?php
	include_once '../../classes/Pdo.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);

    if(isset($jsonObj['get_users_to_notify'])){
        $dbo_4field = new mypdo();
        $users_to_notify = $dbo_4field->select('subcontratacao_notificacao');

        echo json_encode(['users_to_notify'=>$users_to_notify]);
    }else{
        echo json_encode(['users_to_notify'=>'access_denied']);
    }