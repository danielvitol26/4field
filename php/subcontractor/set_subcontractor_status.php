<?php

    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';

    if(
        isset($_POST['motivo']) &&
        isset($_POST['acao']) &&
        isset($_POST['id'])
    ){
        session_start();
        $dbo_4field = new mypdo();

        $data_hoje = date("Y-m-d H:i:s");
        $acao = $_POST['acao'];
        $id_subcontratacao = $_POST['id'];
        $motivo = $_POST['motivo'];
        $user_id = $_SESSION['usuarioID'];

        if($acao == "aprovar"){
            $sql_aprovar = "update subcontratacao set status_opm ='Aprovado', data_validacao_opm = '$data_hoje', usuario_validacao_opm = $user_id where id = $id_subcontratacao ";
            $resultado_aprovar = $dbo_4field->exe_sql($sql_aprovar);
            $response = array('success' => $resultado_aprovar);

            echo json_encode($response);
        }

        if($acao == "reprovar"){
            $sql_reprovar = "update subcontratacao set status_opm ='Reprovado', motivo_reprovacao_opm = '$motivo', data_validacao_opm = '$data_hoje', usuario_validacao_opm = $user_id where id = $id_subcontratacao ";
            $resultado_reprovar = $dbo_4field->exe_sql($sql_reprovar);
            $response = array('success' => $resultado_reprovar);

            echo json_encode($response);
        }
    }
?>