<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../classes/Utils.php';

    if (
        isset($_POST['resource_id']) &&
        isset($_POST['name']) &&
        isset($_POST['company']) &&
        isset($_POST['type']) &&
        isset($_POST['email']) &&
        isset($_POST['phone']) &&
        isset($_POST['senha']) &&
        isset($_POST['activation'])
    ){
        $resource_id = trim($_POST['resource_id']);
        $name = trim(mb_strtoupper($_POST['name'],'UTF-8'));
        $company = intval(trim($_POST['company']));
        $type = trim($_POST['type']);
        $email = trim($_POST['email']);
        $phone = trim($_POST['phone']);
        $password = $_POST['senha'];
        $activation = intval($_POST['activation']);

        $utils = new utils();
        if(strlen($resource_id) < 1){
            echo json_encode(['add_status'=>'invalid_resource']);
        }else if(strlen($name) < 4){
            echo json_encode(['add_status'=>'invalid_name']);
        }else if(strlen($company) < 1){
            echo json_encode(['add_status'=>'invalid_company']);
        }else if(strlen($type) < 1){
            echo json_encode(['add_status'=>'invalid_type']);
        }else if($utils->validate_email($email) == false){
            echo json_encode(['add_status'=>'invalid_email']);
        }else if(strlen($phone) > 13 || strlen($phone) < 13){
            echo json_encode(['add_status'=>'invalid_phone']);
        }else if($utils->validate_password($password) == false){
            echo json_encode(['add_status'=>'invalid_password']);
        }else{
            $dbo_4field = new mypdo();

            date_default_timezone_set('America/Fortaleza');
		    $today_date = date("Y-m-d");
		    $today_date_time = date("Y-m-d H:i:s");

            $new_user = [
                'matricula' => $resource_id,
                'nome' => $name,
                'email' => $email,
                'telefone' => $phone,
                'id_empresa' => $company,
                'tipo' => $type,
                'senha' => $password,
                'status' => 1,
                'is_in_toa' => $activation,
                'activation' => $activation,
                'data_cadastro' => $today_date,
                'data_ultima_atualizacao' => $today_date_time,
                'ultimo_acesso' => $today_date_time,
            ];

            $result = $dbo_4field->insert('usuario', $new_user);

            if($result == true){
                echo json_encode([
                    'add_status' => 'success_insert',
                ]);
            }else{
                echo json_encode([
                    'add_status' => 'error_insert',
                ]);
            }
        }
    }else{
        echo json_encode(['add_status'=>'data_not_received']);
    }

?>