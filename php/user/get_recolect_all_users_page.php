<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';
?>

<div class="pagetitle">
    <h1>Coletor de Usuários</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Início</a></li>
            <li class="breadcrumb-item active">Coletor de Usuários</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card user-colector">
                <div class="card-body recolect_form">
                    <h5 class="card-title">Preencha o formulário abaixo com as definições da coleta</h5>
                    <form class="row">
                        <div class="col-md-6">
                            <label for="users_to_rescollect" class="form-label">Atualizar base completa?</label>
                            <select id="users_to_rescollect" class="form-select">
                                <option value="just_users_from_api" selected>Somente usuários que vieram da API</option>
                                <option value="all_users">Recoletar todos os usuários</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label">Quais campos você deseja atualizar?</label>
                            <div class="checkbox-list">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="name" checked>
                                    <label class="form-check-label" for="name">
                                    Nome
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="company" checked>
                                    <label class="form-check-label" for="company">
                                    Empresa
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="type">
                                    <label class="form-check-label" for="type">
                                    Perfil
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="status" checked>
                                    <label class="form-check-label" for="status">
                                    Status
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="text-center action-buttons">
                        <button class="btn btn-primary start_recollect">Iniciar Coleta de Dados</button>
                    </div>
                </div>
                <div class="card-body recolect_progress none">
                    <h5 class="card-title">Aguarde o final da coleta para visualizar os resultados</h5>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="card-body recolect_result none">
                    <h5 class="card-title">Resultados da coleta</h5>
                    <div class="row colect_result">
                        <div class="col-md-6 result">
                            <p>Usuários enviados: <span class="total_users_sended"></span></p>
                            <p>Usuários encontrados na API: <span class="total_users_founded_in_api"></span></p>
                            <p>Usuários atualizados: <span class="total_users_updated"></span></p>
                            <p>Usuários com erro durante atualização: <span class="total_users_updated_error"></span></p>
                        </div>
                        <div class="col-md-6 result">
                            <p>Nomes atualizados: <span class="discovered_name"></span></p>
                            <p>Empresas atualizadas: <span class="discovered_company"></span></p>
                            <p>Perfis atualizados: <span class="discovered_user_type"></span></p>
                            <p>Status atualizados: <span class="discovered_status"></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>