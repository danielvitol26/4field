<?php
    include_once '../../classes/Pdo.php';

    if(isset($_POST['count'])){
        $dbo_4field = new mypdo();
        //$dbo_old = new mypdo("localhost", "u487615999_4field", "root", "");

        $select_active = [
            'status' => 1,
            'activation' => 1,
        ];

        $select_inactive = [
            'status' => 0,
            'activation' => 1,
        ];

        $select_pending = [
            'status' => 1,
            'activation' => 0,
        ];

        $users_active = $dbo_4field->select_multi_rules('usuario', $select_active, 'nome asc');
        $users_inactive = $dbo_4field->select_multi_rules('usuario', $select_inactive, 'nome asc');
        $users_pending = $dbo_4field->select_multi_rules('usuario', $select_pending, 'nome asc');

        $count_filters = [
            'count_active' => count($users_active),
            'count_pending' => count($users_pending),
            'count_incative' => count($users_inactive),
        ];

        echo json_encode($count_filters);
    }
?>