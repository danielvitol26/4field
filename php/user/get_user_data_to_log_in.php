<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../data_collector/data_collector_user.php';

    if (isset($_POST['resource_id']) && isset($_POST['password'])){
        $resource_id = $_POST['resource_id'];
        $password = $_POST['password'];

        $dbo_4field = new mypdo();
        //$dbo_old = new mypdo("localhost", "u487615999_4field", "root", "");

        $user = $dbo_4field->select('usuario', 'matricula', $resource_id);
        if (count($user) > 0){
            if($user[0][7] == $password){
                $id = $user[0][0];
                $resource_id = $user[0][1];
                $name = $user[0][2];
                $email = $user[0][3];
                $telefone = $user[0][4];
                $company = $user[0][5];
                $type = $user[0][6];
                $password = $user[0][7];
                $status = intval($user[0][8]);
                $is_in_toa = intval($user[0][9]);
                $activation = intval($user[0][10]);

                if($activation == 0){
                    echo json_encode(['login_status'=>'inactive_adm']);
                }else if($status == 0){
                    echo json_encode(['login_status'=>'inactive_toa']);
                }else if($type == "4FIELD_TECNICO"){
                    echo json_encode(['login_status'=>'inactive_type']);
                }else{
                    session_destroy();
                    session_start();
                    $_SESSION['usuarioID'] = $id;
                    $_SESSION['usuarioNome'] = $name;
                    $_SESSION['usuarioPerfil'] = $type;
                    $_SESSION['usuarioEmpresa'] = $company;
                    $_SESSION['usuarioStatus'] = $status;
                    $_SESSION['usuarioActivation'] = $activation;
                    $_SESSION['usuarioToa'] = $is_in_toa;
                    $_SESSION['usuarioLogin'] = $resource_id;
                    $_SESSION['usuarioSenha'] = $password;
                    $_SESSION['usuarioEmail'] = $email;
                    $_SESSION['usuarioTelefone'] = $telefone;
                    echo json_encode(['login_status'=>'success']);
                }
            }else{
                echo json_encode(['login_status'=>'failed']);
            }
        }else{
        }
    }else{
        echo json_encode(['login_status'=>'data_not_sended']);
    }
?>