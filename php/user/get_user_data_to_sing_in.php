<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../data_collector/data_collector_user.php';

    if (isset($_POST['matricula']) || isset($_GET['matricula'])){

        $resource_id = '';
        if(isset($_POST['matricula'])){
            $resource_id = $_POST['matricula'];
        }else if(isset($_GET['matricula'])){
            $resource_id = $_GET['matricula'];
        }
        $resource_id = trim($resource_id);

        $is_adm_requisition = false;
        if(isset($_POST['is_adm_requisition'])){
            $is_adm_requisition = boolval($_POST['is_adm_requisition']);
        }

        $dbo_4field = new mypdo();
        //$dbo_old = new mypdo("localhost", "u487615999_4field", "root", "");
        $usuarios = $dbo_4field->select('usuario', 'matricula', $resource_id);

        if(count($usuarios) > 0 && $is_adm_requisition == false){
            echo json_encode(['updated_status'=>'already_registred']);
        }else{
            $collector = new UserDataCollector();
            $path_resource = $collector->create_path_to_resource($resource_id);
            $user_resource_data = $collector->get_data_from_api($path_resource);
            $updated_user = null;

            //Primeiro devemos tentar pegar o usuário pelo resources
            if($user_resource_data != false && isset($user_resource_data->status) && $user_resource_data->status != 404){
                $updated_user = $collector->discover_user_data($user_resource_data);
            }else{
                //Caso de errado, pegamos o usuário pelo users
                $path_user = $collector->create_path_to_user($resource_id);
                $user_data = $collector->get_data_from_api($path_user);

                if ($user_data != false && isset($user_data->status) && $user_data->status != 404){
                    $updated_user = $collector->discover_user_data($user_data);
                }else{
                    $collector->set_users_out_toa($resource_id);
                    if(strtoupper($resource_id[0]) == 'F'){
                        $updated_user = [
                            'updated_user_type'=>'4FIELD_USER',
                            'updated_company'=>'TIM',
                            'updated_status'=>'out_of_toa',
                        ];
                    }else if(strtoupper($resource_id[0]) == 'T'){
                        $updated_user = [
                            'updated_user_type'=>'4FIELD_TERCEIRO',
                            'updated_status'=>'out_of_toa',
                        ];
                    }else{
                        $updated_user = [
                            'updated_status'=>'out_of_toa',
                        ];
                    }
                }
            }

            //Trocando sigla da empresa por id do novo banco
            if(isset($updated_user['updated_company'])){
                $company_initials = $updated_user['updated_company'];
                $company = $dbo_4field->select('empresa', 'sigla', strtoupper($company_initials));
                $updated_user['updated_company'] = $company[0][0];
            }

            //Trocando user_type para o padrão do novo banco
            if(isset($updated_user['updated_user_type'])){
                $final_user_type = '';
                $user_type = $updated_user['updated_user_type'];
                if($user_type == '4FIELD_ADM'){
                    $final_user_type = 'adm';
                }else if($user_type == '4FIELD_USER'){
                    $final_user_type = 'user';
                }else if($user_type == '4FIELD_TERCEIRO'){
                    $final_user_type = 'terceiro';
                }else if($user_type == '4FIELD_TECNICO'){
                    $final_user_type = 'tecnico';
                }
                $updated_user['updated_user_type'] = $final_user_type;
            }

            if($updated_user == false){
                echo json_encode(['updated_status'=>'inactive']);
            }else{
                echo json_encode($updated_user);
            }
        }
    }else{
        echo json_encode(['updated_status'=>'resource_id_not_sended']);
    }
?>