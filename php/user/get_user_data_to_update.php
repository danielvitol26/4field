<form action="./get_user_data_to_update.php" method="post">
	<label for="matriculas">Cole as matrículas de usuários a serem consultados e clique no botão:</label>
	<br>
	<br>
	<textarea id="matriculas" name="matriculas" rows="10" cols="100"></textarea>
	<br>
	<br>
	<input type="submit" value="Fazer consulta">
</form>

<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../data_collector/data_collector_user.php';

	$pdo = new mypdo();

    if (isset($_POST['matriculas'])){
		$users_out_toa = array();
		$matriculas = explode(',', $_POST['matriculas']);

		$collector = new UserDataCollector();

		$steps_of_resource_id = array();
		for ($x = 0; $x < count($matriculas); $x++){
			if($x <= 1000){
				$steps_of_resource_id[0][] = $matriculas[$x];
			}
			if($x <= 2000){
				$steps_of_resource_id[1][] = $matriculas[$x];
			}
			if($x <= 3000){
				$steps_of_resource_id[2][] = $matriculas[$x];
			}
			if($x <= 4000){
				$steps_of_resource_id[3][] = $matriculas[$x];
			}
			if($x <= 5000){
				$steps_of_resource_id[4][] = $matriculas[$x];
			}
			if($x <= 6000){
				$steps_of_resource_id[5][] = $matriculas[$x];
			}
		}

		foreach($steps_of_resource_id[0] as $resource_id){
			$resource_id = trim($resource_id);
			$path_resource = $collector->create_path_to_resource($resource_id);

			$user_resource_data = $collector->get_data_from_api($path_resource);
			$updated_user = null;

			//Primeiro devemos tentar pegar o usuário pelo resources
			if($user_resource_data != false && isset($user_resource_data->status) && $user_resource_data->status != 404){
				$updated_user = $collector->discover_user_data($user_resource_data);
			}else{
				//Caso de errado, pegamos o usuário pelo users
				$path_user = $collector->create_path_to_user($resource_id);

				$user_data = $collector->get_data_from_api($path_user);

				if ($user_data != false && isset($user_data->status) && $user_data->status != 404){
					$updated_user = $collector->discover_user_data($user_data);
				}else{
					$collector->set_users_out_toa($resource_id);
				}
			}
		}

		$inactive_enrollments = $collector->get_inactive_enrollments();
		$active_enrollments = $collector->get_active_enrollments();
		$users_without_company = $collector->get_users_without_company();
		$users_out_toa = $collector->get_users_out_toa();
		$discovered_companies = $collector->get_discovered_companies();

		echo '<br><br>Usuários inativos: '. count($inactive_enrollments) .'<br>';
		echo '<br>Usuários ativos: '. count($active_enrollments) .'<br>';
		echo '<br>Usuários sem empresa: '. count($users_without_company) .'<br>';
		echo '<br>Usuários com empresa encontrada: '. $discovered_companies .'<br>';
		echo '<br>Usuários fora do TOA: '. count($users_out_toa) .'<br><br>';

		echo '<br>Usuários inativos: <br>';
		//print("<pre>".print_r($inactive_enrollments,true)."</pre>");

		$inactive_enrollments_resource_id = '';
		for($x=0; $x < count($inactive_enrollments); $x++){
			if ($x == (count($inactive_enrollments)-1)){
				$inactive_enrollments_resource_id.= '"' . $inactive_enrollments[$x] . '"';
			}else{
				$inactive_enrollments_resource_id.= '"' . $inactive_enrollments[$x] . '", ';
			}
		}

		echo '<br>UPDATE usuarios SET status = 0, is_in_toa = 1 WHERE usuario in ( '. $inactive_enrollments_resource_id .' )<br>';

		echo '<br><br><br>Usuários ativos: <br>';
		//print("<pre>".print_r($active_enrollments,true)."</pre>");

		for($x=0; $x < count($active_enrollments); $x++){
			$name = $active_enrollments[$x]['updated_name'];
			$matricula = $active_enrollments[$x]['updated_login'];
			$user_type = $active_enrollments[$x]['updated_user_type'];
			$company = $active_enrollments[$x]['updated_company'];

			if($company == "TIM"){
				$company = "%";
			}
			if($company == "EZT"){
				$company = "EZENTIS";
			}
			if($company == "CFC"){
				$company = "COMFICA";
			}
			if($company == "TLP"){
				$company = "TELEPERFORMANCE";
			}
			if($company == "ALP"){
				$company = "ALPITEL";
			}
			if($company == "G4T"){
				$company = "GREEN4T";
			}
			if($company == "TEL"){
				$company = "TEL";
			}

			echo '<br>UPDATE usuarios SET nome = "'. $name .'", empresa = "'. $company .'", PERFIL = "'. $user_type .'", status = 1, is_in_toa = 1 WHERE usuario = "'. $matricula .'"; <br>';
		}

		echo '<br><br><br>Usuários sem empresa: <br>';

		for($x=0; $x < count($users_without_company); $x++){
			$name = $users_without_company[$x]['name'];
			$matricula = $users_without_company[$x]['matricula'];
			$user_type = $users_without_company[$x]['user_type'];

			echo '<br>UPDATE usuarios SET nome = "'. $name .'", PERFIL = "'. $user_type .'", status = 1, is_in_toa = 1 WHERE usuario = "'. $matricula .'"; <br>';
		}

		echo '<br><br><br>Usuários fora do TOA: <br>';

		$users_out_toa_resource_id = '';
		for($x=0; $x < count($users_out_toa); $x++){
			if ($x == (count($users_out_toa)-1)){
				$users_out_toa_resource_id.= '"' . $users_out_toa[$x] . '"';
			}else{
				$users_out_toa_resource_id.= '"' . $users_out_toa[$x] . '", ';
			}
		}

		echo '<br>UPDATE usuarios SET status = 1, is_in_toa = 0 WHERE usuario in ( '. $users_out_toa_resource_id .' )<br>';

		die;

		/*foreach($matriculas as $resource_id){
			$resource_id = trim($resource_id);
			$path_resource = $collector->create_path_to_resource($resource_id);

			$user_resource_data = $collector->get_data_from_api($path_resource);
			$updated_user = null;

			//Primeiro devemos tentar pegar o usuário pelo resources
			if($user_resource_data != false && isset($user_resource_data->status) && $user_resource_data->status != 404){
				$updated_user = $collector->discover_user_data($user_resource_data);
			}else{
				//Caso de errado, pegamos o usuário pelo users
				$path_user = $collector->create_path_to_user($resource_id);

				$user_data = $collector->get_data_from_api($path_user);

				if ($user_data != false && isset($user_data->status) && $user_data->status != 404){
					$updated_user = $collector->discover_user_data($user_data);
				}else{
					$collector->set_users_out_toa($resource_id);
				}
			}

			//echo '<br>';
			//echo '<pre>';
			//echo print_r($updated_user);
			//echo '<pre>';
			//echo '<br>';
		}

		$inactive_enrollments = $collector->get_inactive_enrollments();
		$active_enrollments = $collector->get_active_enrollments();
		$users_without_company = $collector->get_users_without_company();
		$users_out_toa = $collector->get_users_out_toa();
		$discovered_companies = $collector->get_discovered_companies();

		echo '<br>Usuários inativos: '. count($inactive_enrollments) .'<br>';
		echo '<br>Usuários ativos: '. count($active_enrollments) .'<br>';
		echo '<br>Usuários sem empresa: '. count($users_without_company) .'<br>';
		echo '<br>Usuários com empresa encontrada: '. $discovered_companies .'<br>';
		echo '<br>Usuários fora do TOA: '. count($users_out_toa) .'<br>';

		die;*/
	}
?>