<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';
    $utils = new Utils();

    $status = 1;
    if(isset($_POST['status'])){
        $status = $_POST['status'];
    }

    $activation = 1;
    if(isset($_POST['activation'])){
        $activation = $_POST['activation'];
    }

    $dbo_4field = new mypdo();
    //$dbo_old = new mypdo("localhost", "u487615999_4field", "root", "");

    $select_active = [
        'status' => 1,
        'activation' => 1,
    ];

    $select_inactive = [
        'status' => 0,
        'activation' => 1,
    ];

    $select_pending = [
        'status' => 1,
        'activation' => 0,
    ];

    $users_active = $dbo_4field->select_multi_rules('usuario', $select_active, 'nome asc');
    $users_inactive = $dbo_4field->select_multi_rules('usuario', $select_inactive, 'nome asc');
    $users_pending = $dbo_4field->select_multi_rules('usuario', $select_pending, 'nome asc');

    $active_selected = '';
    $inactive_selected = '';
    $pending_selected = '';
    $is_pending_user = 1;
    $current_list = Null;
    if($status == 1 && $activation == 1){
        $current_list = $users_active;
        $active_selected = 'selected';
    }else if($status == 0 && $activation == 1){
        $current_list = $users_inactive;
        $inactive_selected = 'selected';
    }else if($status == 1 && $activation == 0){
        $current_list = $users_pending;
        $pending_selected = 'selected';
        $is_pending_user = 0;
    }
?>

<div class="pagetitle">
    <h1>Controle de usuários</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Início</a></li>
            <li class="breadcrumb-item active">Controle de usuários</li>
        </ol>
    </nav>
</div>

<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card user-manage">
                <div class="card-body">
                    <button class="filter <?php echo $active_selected; ?>" data-status="1" data-activation="1">Usuários Ativos (<span class="active"></span>)</button>
                    <button class="filter <?php echo $pending_selected; ?>" data-status="1" data-activation="0">Usuários Pendentes (<span class="pending"></span>)</button>
                    <button class="filter <?php echo $inactive_selected; ?>" data-status="0" data-activation="1">Usuários Inativos (<span class="inactive"></span>)</button>
                </div>

                <div class="card-body">
                    <div class="accordion accordion-flush" id="accordionList">
                        <?php
                            foreach($current_list as $user){
                                $user_id = $user[0];
                                $user_matricula =  $user[1];
                                $user_nome =  $user[2];
                                $user_email =  $user[3];
                                $user_telefone =  $user[4];
                                $user_id_empresa =  intval($user[5]);
                                $user_type =  $user[6];
                                $user_senha =  $user[7];
                                $user_status =  intval($user[8]);
                                $user_is_in_toa =  intval($user[9]);
                                $user_activation =  intval($user[10]);

                                $disabled = '';
                                if($user_is_in_toa == 1){
                                    $disabled = 'disabled';
                                }
                        ?>
                            <div class="accordion-item sub-<?php echo strtoupper($user_matricula); ?>">
                                <h2 class="accordion-header" id="flush-heading<?php echo strtoupper($user_matricula); ?>">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sub-<?php echo $user_matricula; ?>" aria-expanded="false" aria-controls="<?php echo $user_matricula; ?>">
                                        <p><?php echo mb_strtoupper($user_nome); ?> (<?php echo strtoupper($user_matricula); ?>)</p>
                                    </button>
                                </h2>
                                <div id="sub-<?php echo $user_matricula; ?>" class="accordion-collapse collapse" aria-labelledby="flush-heading<?php echo strtoupper($user_matricula); ?>" data-bs-parent="#accordionList">
                                    <div class="accordion-body">
                                        <form class="row g-3">
                                            <div class="col-md-6">
                                                <label class="form-label">Usuário</label>
                                                <input type="text" name="resource_id" class="form-control resource_id" value="<?php echo $user_matricula; ?>" <?php echo $disabled; ?>>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Nome</label>
                                                <input type="text" name="name" class="form-control name" value="<?php echo $user_nome; ?>" <?php echo $disabled; ?>>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Email</label>
                                                <input type="text" name="email" class="form-control email" value="<?php echo $user_email; ?>">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Telefone</label>
                                                <input type="text" name="phone" class="form-control phone" value="<?php echo $user_telefone; ?>">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Senha</label>
                                                <input type="password" class='none' name="old_password" class="form-control password" value="<?php echo $user_senha; ?>">
                                                <input type="password" name="password" class="form-control password" value="">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Confirmação de senha</label>
                                                <input type="password" name="repassword" class="form-control repassword">
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-label">Empresa</label>
                                                <select class="form-select company" name="company" <?php echo $disabled; ?>>
                                                    <option value=''>Selecione</option>
                                                    <?php
                                                        $companys = $dbo_4field->select('empresa', 'status', 1);
                                                        foreach($companys as $company_option){
                                                            if($company_option[0] == $user_id_empresa){
                                                                echo '<option value="' . $company_option[0] . '" selected>' . $company_option[1] . '</option>';
                                                            }else{
                                                                echo '<option value="' . $company_option[0] . '">' . $company_option[1] . '</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-label">Perfil</label>
                                                <select class="form-select user_type" name="type">
                                                    <?php
                                                        $adm_type_selected = '';
                                                        $user_type_selected = '';
                                                        $terceiro_type_selected = '';
                                                        $tecnico_type_selected = '';

                                                        if($user_type == 'adm'){
                                                            $adm_type_selected = 'selected';
                                                        }else if($user_type == 'user'){
                                                            $user_type_selected = 'selected';
                                                        }else if($user_type == 'terceiro'){
                                                            $terceiro_type_selected = 'selected';
                                                        }else if($user_type == 'tecnico'){
                                                            $tecnico_type_selected = 'selected';
                                                        }
                                                    ?>
                                                    <option value="">Selecione seu perfil</option>
                                                    <option value="adm" <?php echo $adm_type_selected; ?>>Administrador</option>
                                                    <option value="user" <?php echo $user_type_selected; ?>>Funcionário da Tim</option>
                                                    <option value="terceiro" <?php echo $terceiro_type_selected; ?>>Parceiro</option>
                                                    <option value="tecnico" <?php echo $tecnico_type_selected; ?>>Técnico Parceiro</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-label">Status</label>
                                                <select class="form-select status" name="status" <?php echo $disabled; ?>>
                                                    <option selected>Selecione</option>
                                                    <?php
                                                        if($user_status == 1){
                                                    ?>
                                                        <option value="1" selected>Ativo</option>
                                                        <option value="0">Inativo</option>
                                                    <?php
                                                        }else{
                                                    ?>
                                                        <option value="0" selected>Inativo</option>
                                                        <option value="1">Ativo</option>
                                                    <?php
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </form>
                                        <div class="text-center action-buttons">
                                            <?php
                                                $save_button_text = '';
                                                if($is_pending_user == 1){
                                                    $save_button_text = 'Salvar Alterações';
                                                }else if($is_pending_user == 0){
                                                    $save_button_text = 'Habilitar Usuário';
                                                }
                                            ?>
                                            <button class="btn btn-primary save-changes" data-id="<?php echo $user_id; ?>" data-pending="<?php echo $is_pending_user; ?>"><?php echo $save_button_text; ?></button>
                                            <button class="btn btn-secondary recolect-data">Recoletar Informações</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>