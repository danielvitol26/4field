<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';

    $utils = new Utils();
    $user_session = $utils->get_user_session_data();
    $dbo_4field = new mypdo();

    $id = $user_session['user_id'];
    $name = $user_session['user_name'];
    $type = $user_session['user_type'];
    $company = intval($user_session['user_company']);
    $status = $user_session['user_status'];
    $activation = $user_session['user_activation'];
    $is_in_toa = $user_session['user_toa'];
    $resource_id = $user_session['user_resource_id'];
    $password = $user_session['user_password'];
    $email = $user_session['user_email'];
    $phone = $user_session['user_phone'];

    $company_db = $dbo_4field->select('empresa', 'id', $company);
    $company_name = '';
    $company_id = '';
    if(count($company_db) > 0){
        $company_id = $company_db[0][0];
        $company_name = $company_db[0][1];
    }

    $adm_type_selected = '';
    $user_type_selected = '';
    $terceiro_type_selected = '';
    $tecnico_type_selected = '';
    if($type == 'adm'){
        $adm_type_selected = 'selected';
    }else if($type == 'user'){
        $user_type_selected = 'selected';
    }else if($type == 'terceiro'){
        $terceiro_type_selected = 'selected';
    }else if($type == 'tecnico'){
        $tecnico_type_selected = 'selected';
    }

    $user_disabled = '';
    $name_disabled = '';
    $company_disabled = '';
    $type_disabled = '';

    if($is_in_toa == 1){
        $user_disabled = 'disabled';
        $name_disabled = 'disabled';
        $company_disabled = 'disabled';
        $type_disabled = 'disabled';
    }else{
        $user_disabled = 'disabled';
        $company_disabled = 'disabled';
        $type_disabled = 'disabled';
    }
?>

<div class="pagetitle">
    <h1>Minha conta</h1>
    <nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Início</a></li>
        <li class="breadcrumb-item active">Minha Conta</li>
    </ol>
    </nav>
</div><!-- End Page Title -->

<section class="section profile">
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
                    <img src="assets/img/user-skin.png" alt="Profile" class="rounded-circle">
                    <h2><?php echo $name; ?></h2>
                    <h3><?php echo $type; ?></h3>
                </div>
            </div>
        </div>

        <div class="col-xl-8">
            <div class="card">
                <div class="card-body pt-3">
                    <!-- Bordered Tabs -->
                    <ul class="nav nav-tabs nav-tabs-bordered">

                        <li class="nav-item">
                            <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Dados</button>
                        </li>

                        <li class="nav-item">
                            <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Editar Dados</button>
                        </li>

                        <li class="nav-item">
                            <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Alterar Senha</button>
                        </li>

                    </ul>
                    <div class="tab-content pt-2">
                        <div class="tab-pane fade show active profile-overview" id="profile-overview">
                            <div class="row">
                                <div class="col-lg-3 col-md-4 label ">Usuário</div>
                                <div class="col-lg-9 col-md-8"><?php echo $resource_id; ?></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 col-md-4 label">Nome</div>
                                <div class="col-lg-9 col-md-8"><?php echo $name; ?></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 col-md-4 label">Empresa</div>
                                <div class="col-lg-9 col-md-8"><?php echo $company_name; ?></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 col-md-4 label">Perfil</div>
                                <div class="col-lg-9 col-md-8"><?php echo $type; ?></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 col-md-4 label">Email</div>
                                <div class="col-lg-9 col-md-8"><?php echo $email; ?></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 col-md-4 label">Telefone</div>
                                <div class="col-lg-9 col-md-8"><?php echo $phone; ?></div>
                            </div>

                        </div>

                        <div class="tab-pane fade profile-edit pt-3" id="profile-edit">
                            <!-- Profile Edit Form -->
                            <form>
                                <!--<div class="row mb-3">
                                    <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
                                    <div class="col-md-8 col-lg-9">
                                    <img src="assets/img/profile-img.jpg" alt="Profile">
                                    <div class="pt-2">
                                        <a href="#" class="btn btn-primary btn-sm" title="Upload new profile image"><i class="bi bi-upload"></i></a>
                                        <a href="#" class="btn btn-danger btn-sm" title="Remove my profile image"><i class="bi bi-trash"></i></a>
                                    </div>
                                    </div>
                                </div>-->

                                <div class="row mb-3">
                                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Usuário</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="resource_id" type="text" class="form-control" value="<?php echo $resource_id; ?>" <?php echo $user_disabled; ?>>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="company" class="col-md-4 col-lg-3 col-form-label">Nome</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="name" type="text" class="form-control" value="<?php echo $name; ?>" <?php echo $name_disabled; ?>>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="Country" class="col-md-4 col-lg-3 col-form-label">Empresa</label>
                                    <div class="col-md-8 col-lg-9">
                                        <select class="empresa" name="company" <?php echo $company_disabled; ?>>
                                            <option value="">Selecione sua empresa</option>
                                            <?php
                                                $dbo_4field = new mypdo();

                                                $array_select = [
                                                    'status' => 1,
                                                ];

                                                $companys = $dbo_4field->select_multi_rules('empresa', $array_select, 'nome asc');
                                                foreach($companys as $company){
                                                    if($company[0] == $company_id){
                                                        echo '<option value="'. $company[0] .'" selected>'. $company[1] .'</option>';
                                                    }
                                                    echo '<option value="'. $company[0] .'">'. $company[1] .'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="Address" class="col-md-4 col-lg-3 col-form-label">Perfil</label>
                                    <div class="col-md-8 col-lg-9">
                                        <select class="perfil" name="type" <?php echo $type_disabled; ?>>
                                            <option value="">Selecione seu perfil</option>
                                            <option value="adm" <?php echo $adm_type_selected; ?>>Administrador</option>
                                            <option value="user" <?php echo $user_type_selected; ?>>Funcionário da Tim</option>
                                            <option value="terceiro" <?php echo $terceiro_type_selected; ?>>Parceiro</option>
                                            <option value="tecnico" <?php echo $tecnico_type_selected; ?>>Técnico Parceiro</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="Email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="email" type="email" class="form-control" value="<?php echo $email; ?>">
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="Phone" class="col-md-4 col-lg-3 col-form-label">Telefone</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="phone" type="text" class="form-control" value="<?php echo $phone; ?>">
                                    </div>
                                </div>
                            </form><!-- End Profile Edit Form -->
                            <div class="text-center">
                                <button id="update_user_profile" data-id="<?php echo $id; ?>" class="btn btn-primary">Salvar Alterações</button>
                            </div>
                        </div>

                        <div class="tab-pane fade pt-3 change-password" id="profile-change-password">
                            <!-- Change Password Form -->
                            <form>
                                <div class="row mb-3">
                                    <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Senha atual</label>
                                    <div class="col-md-8 col-lg-9">
                                    <input name="password" type="password" class="form-control" id="currentPassword">
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">Nova senha</label>
                                    <div class="col-md-8 col-lg-9">
                                    <input name="newpassword" type="password" class="form-control" id="newPassword">
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Confirmação da nova senha</label>
                                    <div class="col-md-8 col-lg-9">
                                    <input name="renewpassword" type="password" class="form-control" id="renewPassword">
                                    </div>
                                </div>
                            </form><!-- End Change Password Form -->
                            <div class="text-center">
                                <button class="btn btn-primary" id='change_password'>Alterar Senha</button>
                            </div>
                        </div>
                    </div><!-- End Bordered Tabs -->
                </div>
            </div>
        </div>
    </div>
</section>