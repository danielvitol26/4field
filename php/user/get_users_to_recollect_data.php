<?php
    include_once '../../classes/Utils.php';
    include_once '../../classes/Pdo.php';

    if (
        isset($_POST['users_to_recollect'])
    ){
        $users_to_recollect = $_POST['users_to_recollect'];

        $utils = new utils();
        $dbo_4field = new mypdo();
        //$dbo_old = new mypdo("localhost", "u487615999_4field", "root", "");

        $users_to_upload = null;
        if($users_to_recollect == 'just_users_from_api'){
            $users_to_upload = $dbo_4field->select('usuario', 'is_in_toa', 1);
        }else if($users_to_recollect == 'all_users'){
            $users_to_upload = $dbo_4field->select('usuario');
        }
        $users_to_upload = $utils->utf8_converter($users_to_upload);
        echo json_encode(['users_to_upload' => $users_to_upload]);
    }else{
        echo json_encode(['users_to_upload' => 'data_not_received']);
    }
?>