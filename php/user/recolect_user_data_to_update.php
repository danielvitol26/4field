<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../data_collector/data_collector_user.php';

    $jsonObj = json_decode(file_get_contents('php://input'), true);
    if (isset($jsonObj['resource_id'])){
        $resource_id = trim($jsonObj['resource_id']);

        $collector = new UserDataCollector();
        $path_resource = $collector->create_path_to_resource($resource_id);
        $user_resource_data = $collector->get_data_from_api($path_resource);
        $updated_user = null;

        //Primeiro devemos tentar pegar o usuário pelo resources
        if($user_resource_data != false && isset($user_resource_data->status) && $user_resource_data->status != 404){
            $updated_user = $collector->discover_user_data($user_resource_data);
        }else{
            //Caso de errado, pegamos o usuário pelo users
            $path_user = $collector->create_path_to_user($resource_id);
            $user_data = $collector->get_data_from_api($path_user);

            if ($user_data != false && isset($user_data->status) && $user_data->status != 404){
                $updated_user = $collector->discover_user_data($user_data);
            }else{
                $collector->set_users_out_toa($resource_id);
                if(strtoupper($resource_id[0]) == 'F'){
                    $updated_user = [
                        'updated_user_type'=>'4FIELD_USER',
                        'updated_company'=>'TIM',
                        'updated_status'=>'out_of_toa',
                    ];
                }else if(strtoupper($resource_id[0]) == 'T'){
                    $updated_user = [
                        'updated_user_type'=>'4FIELD_TERCEIRO',
                        'updated_status'=>'out_of_toa',
                    ];
                }else{
                    $updated_user = [
                        'updated_status'=>'out_of_toa',
                    ];
                }
            }
        }

        if($updated_user == false){
            echo json_encode(['updated_status'=>'inactive']);
        }else{
            echo json_encode($updated_user);
        }
    }else{
        echo json_encode(['updated_status'=>'resource_id_not_sended']);
    }
?>