<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../data_collector/data_collector_user.php';
    include_once '../../classes/Utils.php';

    if(
        isset($_POST['password']) &&
        isset($_POST['newpassword']) &&
        isset($_POST['renewpassword'])
    ){
        $resource_id = '';
        $old_password = '';
        if(isset($_POST['resource_id'])){
            $resource_id = $_POST['resource_id'];
            $old_password = $_POST['password'];
        }else{
            $resource_id = $_SESSION['usuarioLogin'];
            $old_password = $_SESSION['usuarioSenha'];
        }

        $password = $_POST['password'];
        $newpassword = $_POST['newpassword'];
        $renewpassword = $_POST['renewpassword'];

        $utils = new utils();

        if($password == '' || $newpassword == '' || $renewpassword == ''){
            echo json_encode(['update_status'=>'blank_password']);
        }else if($old_password != $password){
            echo json_encode(['update_status'=>'old_password_diferent']);
        }else if($newpassword != $renewpassword){
            echo json_encode(['update_status'=>'diferent_password']);
        }else if($newpassword == $password){
            echo json_encode(['update_status'=>'same_password']);
        }else if($utils->validate_password($newpassword) == false){
            echo json_encode(['update_status'=>'invalid_password']);
        }else{
            $update_array = [
                'senha' => $newpassword,
            ];

            $dbo_4field = new mypdo();
            //$dbo_old = new mypdo("localhost", "u487615999_4field", "root", "");
            $update_user = $dbo_4field->update("usuario", "matricula='$resource_id'", $update_array);

            if($update_user == true){
                if(!isset($_POST['resource_id'])){
                    $_SESSION['usuarioSenha'] = $newpassword;
                }
                echo json_encode(['update_status'=>'success']);
            }else if($update_user == false){
                echo json_encode(['update_status'=>'error']);
            }
        }
    }else{
        echo json_encode(['update_status'=>'data_not_received']);
    }