<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../data_collector/data_collector_user.php';
    include_once '../../classes/Utils.php';

    if(
        isset($_POST['user_id']) &&
        isset($_POST['resource_id']) &&
        isset($_POST['name']) &&
        isset($_POST['company']) &&
        isset($_POST['type']) &&
        isset($_POST['email']) &&
        isset($_POST['phone'])
    ){
        $user_id = intval($_POST['user_id']);
        $resource_id = $_POST['resource_id'];
        $name = $_POST['name'];
        $company = $_POST['company'];
        $type = $_POST['type'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];

        $status = '';
        if(isset($_POST['status'])){
            $status = $_POST['status'];
        }

        $activation = '';
        if(isset($_POST['activation'])){
            if($_POST['activation'] == true){
                $activation = 1;
            }else{
                $activation = 0;
            }
        }

        $is_adm_requisition = false;
        if(isset($_POST['is_adm_requisition'])){
            $is_adm_requisition = boolval($_POST['is_adm_requisition']);
        }

        $utils = new Utils();
        $phone = $utils->remove_special_characters($phone);

        if(strlen($resource_id) < 4){
            echo json_encode(['update_status'=>'invalid_resource_id']);
        }else if(strlen($name) < 4){
            echo json_encode(['update_status'=>'invalid_name']);
        }else if($utils->validate_email($email) == false){
            echo json_encode(['update_status'=>'invalid_email']);
        }else if(strlen($phone) > 13 || strlen($phone) < 13){
            echo json_encode(['update_status'=>'invalid_phone']);
        }else if($company == ''){
            echo json_encode(['update_status'=>'invalid_company']);
        }else{
            $update_array = [
                'matricula' => trim(strtoupper($resource_id)),
                'nome' => trim(mb_strtoupper($name)),
                'id_empresa' => intval($company),
                'tipo' => $type,
                'email' => trim($email),
                'telefone' => $phone,
            ];

            if($status != ''){
                $update_array += ['status' => intval($status)];
            }

            if($activation != ''){
                $update_array += ['activation' => $activation];
            }

            $dbo_4field = new mypdo();
            $update_user = $dbo_4field->update("usuario", "id=$user_id", $update_array);

            if($update_user == true){
                if($is_adm_requisition == true){
                    echo json_encode(['update_status'=>'success']);
                }else{
                    $_SESSION['usuarioLogin'] = $resource_id;
                    $_SESSION['usuarioNome'] = $name;
                    $_SESSION['usuarioPerfil'] = $type;
                    $_SESSION['usuarioEmpresa'] = $company;
                    $_SESSION['usuarioEmail'] = $email;
                    $_SESSION['usuarioTelefone'] = $phone;
                    $_SESSION['usuarioStatus'] = $status;
                    if($activation != ''){
                        $_SESSION['usuarioActivation'] = $activation;
                    }
                    echo json_encode(['update_status'=>'success']);
                }
            }else if($update_user == false){
                echo json_encode(['update_status'=>'error']);
            }
        }
    }else{
        echo json_encode(['update_status'=>'data_not_received']);
    }