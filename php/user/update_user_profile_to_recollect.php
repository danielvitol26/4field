<?php
    session_start();
	include_once '../../classes/Pdo.php';
	include_once '../../data_collector/data_collector_user.php';
    include_once '../../classes/Utils.php';


    $jsonObj = json_decode(file_get_contents('php://input'), true);
    if (isset($jsonObj['resource_id'])){
        $resource_id = trim($jsonObj['resource_id']);

        $dbo_4field = new mypdo();
        $update_array = [];

        if(isset($jsonObj['name'])){
            $update_array['nome'] = trim($jsonObj['name']);
        }

        if(isset($jsonObj['company'])){
            $company_initials = $jsonObj['company'];
            $company = $dbo_4field->select('empresa', 'sigla', $company_initials);
            if(count($company) > 0){
                $company_id = $company[0][0];
                $update_array['id_empresa'] = intval($company_id);
            }
        }

        if(isset($jsonObj['type'])){
            $update_array['tipo'] = $jsonObj['type'];
        }

        if(isset($jsonObj['status'])){
            if($jsonObj['status'] == 'active'){
                $update_array['status'] = 1;
            }else if($jsonObj['status'] == 'inactive'){
                $update_array['status'] = 0;
            }
        }

        if(isset($jsonObj['is_in_toa'])){
            $update_array['is_in_toa'] = intval($jsonObj['is_in_toa']);
        }

        $update_user = $dbo_4field->update("usuario", "matricula='$resource_id'", $update_array);

        if($update_user == true){
            echo json_encode(['update_status'=>'success']);
        }else if($update_user == false){
            echo json_encode(['update_status'=>'error']);
        }
    }else{
        echo json_encode(['update_status'=>'data_not_received']);
    }